connection: "bigquery_connector"

include: "/ntdchi_views/*.view.lkml"
### include: "/ntdchi_dashboards/*.dashboard.lookml"

week_start_day: wednesday

access_grant: all_can_view {
  user_attribute: department
  allowed_values: ["DATA", "MANAGEMENT", "PLATFORM", "MKT", "PROGRAMMING", "COMIC", "ESPORT", "D2C", "B2B", "BD", "PRODUCT", "SEO", "CONTENT", "FIN", "CA", "OTHER", "TH"]
}

access_grant: bom_can_view {
  user_attribute: department
  allowed_values: ["DATA", "MANAGEMENT"]
}

access_grant: vietnam_can_view {
  user_attribute: department
  allowed_values: ["DATA", "MANAGEMENT", "PLATFORM", "MKT", "PROGRAMMING", "COMIC", "ESPORT", "D2C", "B2B", "BD", "PRODUCT", "SEO", "CONTENT", "FIN", "CA", "OTHER"]
}

access_grant: thailand_can_view {
  user_attribute: department
  allowed_values: ["DATA", "MANAGEMENT", "TH", "CA", "PRODUCT"]
}

access_grant: data_can_view {
  user_attribute: department
  allowed_values: ["DATA"]
}



datagroup: both_app_performance_cache {
  sql_trigger: SELECT COUNT(*) FROM `pops-bi.Self_Service.POP_Scheduled_DATAMART_Both_App_Performance_All` ;;
  max_cache_age: "24 hours"
}

datagroup: normal_cache {
  max_cache_age: "24 hours"
}



explore: estimated_revenue_tracking {
  required_access_grants: [bom_can_view]
}



explore: both_app_performance {
  required_access_grants: [vietnam_can_view]
  persist_with: both_app_performance_cache
}

explore: appsflyer_performances {
  required_access_grants: [vietnam_can_view]
}

explore: d2c_performance {
  required_access_grants: [vietnam_can_view]
}

explore: b2b_performance {
  required_access_grants: [vietnam_can_view]
}

explore: comic_content_revenue {
  required_access_grants: [vietnam_can_view]
}

explore: content_audience {
  required_access_grants: [vietnam_can_view]
}

explore: user_journey_content_consumption {
  required_access_grants: [vietnam_can_view]
  persist_with: normal_cache
}

explore: user_view_interact {
  required_access_grants: [vietnam_can_view]
}

explore: user_journey {
  required_access_grants: [vietnam_can_view]
}

explore: elsa_performance {
  required_access_grants: [vietnam_can_view]
  persist_with: normal_cache
}

explore: pops_kids_favourite_category {
  required_access_grants: [vietnam_can_view]
  persist_with: normal_cache
}

explore: content_library {
  required_access_grants: [vietnam_can_view]
}

explore: historical_chapters {
  required_access_grants: [vietnam_can_view]
}

explore: search_keywords_and_selected_items {
  required_access_grants: [vietnam_can_view]
}

explore: view_page {
  required_access_grants: [vietnam_can_view]
  persist_with: normal_cache
}

explore: app_reviews {
  required_access_grants: [vietnam_can_view]
}

explore: livestream_message {
  required_access_grants: [vietnam_can_view]
}



explore: realtime_report {
  required_access_grants: [data_can_view]
}

explore: duplicated_checking {
  required_access_grants: [data_can_view]
}

explore: session_load_checking {
  required_access_grants: [data_can_view]
}

explore: first_joined_date_checking {
  required_access_grants: [data_can_view]
}

explore: membership_checking {
  required_access_grants: [data_can_view]
}



explore: thailand_performance {
  required_access_grants: [thailand_can_view]
  sql_always_where: ${app_region} = 'TH' ;;
  persist_with: both_app_performance_cache
}

explore: thailand_appsflyer_performances {
  required_access_grants: [thailand_can_view]
}

explore: thailand_d2c_performance {
  required_access_grants: [thailand_can_view]
}

explore: thailand_b2b_performance {
  required_access_grants: [thailand_can_view]
}

explore: thailand_session_load {
  required_access_grants: [thailand_can_view]
}

explore: thailand_content_library {
  required_access_grants: [thailand_can_view]
}

explore: thailand_search_keywords_and_selected_items {
  required_access_grants: [thailand_can_view]
}

explore: thailand_app_reviews {
  required_access_grants: [thailand_can_view]
}

explore: thailand_livestream_message {
  required_access_grants: [thailand_can_view]
}
