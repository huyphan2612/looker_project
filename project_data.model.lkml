connection: "bigquery_connector"

include: "/pubsub_snapshot/*.view.lkml" # include all views in the views/ folder in this project
include: "/nhnhat_views/*.view.lkml"

include:"/views/*.view.lkml"


access_grant: all_can_view {
  user_attribute: department
  allowed_values: ["DATA", "MANAGEMENT", "PLATFORM", "MKT", "COMIC", "D2C", "B2B", "PRODUCT", "CONTENT", "TH"]
}

access_grant: vietnam_can_view {
  user_attribute: department
  allowed_values: ["DATA", "MANAGEMENT", "PLATFORM", "MKT", "COMIC", "D2C", "B2B", "PRODUCT", "CONTENT"]
}

access_grant: thailand_can_view {
  user_attribute: department
  allowed_values: ["DATA", "TH"]
}

access_grant: d2c {
  user_attribute: department
  allowed_values: ["DATA", "D2C", "PRODUCT"]
}

access_grant: mkt {
  user_attribute: department
  allowed_values: ["DATA", "MKT"]
}

access_grant: product {
  user_attribute: department
  allowed_values: ["DATA",  "PRODUCT"]
}

access_grant: exploration {
  user_attribute: department
  allowed_values: ["DATA",  "PRODUCT", "MKT"]
}

access_grant: data_can_view {
  user_attribute: department
  allowed_values: ["DATA"]
}

datagroup: cache_test  {
  max_cache_age: "24 hours"
}

# include: "/**/view.lkml"                   # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
#
# explore: order_items {
#   join: orders {
#     relationship: many_to_one
#     sql_on: ${orders.id} = ${order_items.order_id} ;;
#   }
#
#   join: users {
#     relationship: many_to_one
#     sql_on: ${users.id} = ${orders.user_id} ;;
#   }
# }

explore: esport_livestream_ccu {
  persist_with: cache_test
}

explore: rt_payment_transaction_report {
  sql_always_where: regexp_contains(upper(product_sku ), 'CLASSIN') ;;
}

explore: kids_onboarding {
  persist_with: cache_test

}

explore: video_log_video_load_wt {

}
explore: stars_transaction {

  required_access_grants: [vietnam_can_view]

  join: metatdata {
    sql_on: ${stars_transaction.product_id} = ${metatdata.object_id}  ;;
    type: left_outer
    relationship: one_to_one
  }


  join: bundle_dim {
    sql_on: ${stars_transaction.product_id} = ${bundle_dim.bundle_id}  ;;
    type: left_outer
    relationship: one_to_one
  }
}

explore: firebase_uninstall {
  required_access_grants: [exploration]
  persist_with: cache_test
}

explore: production_tracking_legal {}

explore: legal_takedown {}

explore: content_airing_plan {}

explore: comic_content_acquisition_weekly_report {}

explore:  video_content_acquisition_weekly_report {}

explore: star_statistic_unlocking_bundle {
  join: pop_dim_bundles {
    sql_on: ${star_statistic_unlocking_bundle.content_id} = ${pop_dim_bundles.bundle_id}  ;;
    type: left_outer
    relationship: one_to_one
  }


}

explore: comics_log_user_billing {
  hidden: yes
  persist_with: cache_test
}

explore: mkt_dulichtainha {
  persist_with: cache_test
}

# explore: mkt_doraemon_reward {

#   required_access_grants: [mkt]
# }

# explore: mkt_anime_reward {

#   # required_access_grants: [mkt]
#   persist_with: cache_test
# }




explore: playlist_interaction {
  required_access_grants: [exploration]
  persist_with: cache_test
}

explore: pubsub_schema {
  hidden: yes
  required_access_grants: [data_can_view]
}

explore: pubsub_RT {}

explore: view__mkt_campaign {
  hidden: yes
  required_access_grants: [mkt]
}

explore: dim_users {

  # required_access_grants: [product, thailand_can_view]
  persist_with: cache_test
}

explore: d2c_realtime{
  required_access_grants: [vietnam_can_view]

}

explore: comics_log_purchased_item{
  required_access_grants: [vietnam_can_view]
  sql_always_where: ${asset_type} = 'bundle'
      AND ${channel} <> 'QC-TEST'
      AND ${redeem_code} IS NOT NULL;;

}

explore: view_page {
  hidden: yes
  required_access_grants: [product]
  always_filter: {
    filters: {
      field: event_date
      value: "last 7 days"

    }
  }
  sql_always_where: ${device_web_info_hostname} IN ("webos.pops.vn", "tizentv.pops.vn") ;;

}

explore: dim_samsung_users {
  persist_with: cache_test
  # required_access_grants: [mkt]
  always_filter: {
    filters: {
      field: first_joined_date_date
      value: "last 7 days"

    }

  }


}

explore: session_load {
  hidden: yes
  required_access_grants: [product]
  always_filter: {
    filters: {

      field: session_date
      value: "last 7 days"
    }

  }

}


explore: TEST_pop_fact_session_load {
hidden: yes
## TO avoid querying the entire database by default, suggest setting up a filter like below, and perhaps limiting GB scanned with 'Max Billing Gigabytes' in the connection
  always_filter: {
    filters: {
      field: session_date
      value: "last 30 days"
    }
  }

  join: pop_dim_profiles {
    sql_on: ${TEST_pop_fact_session_load.profile_id} = ${pop_dim_profiles.profile_id} ;;
    type: left_outer
    relationship: many_to_one
  }

  join: pop_dim_users {
    sql_on: ${pop_dim_users.user_id} = ${pop_dim_profiles.user_id} ;;
    type: left_outer
    relationship: one_to_one
  }

  join: pop_dim_videos {
    sql_on: ${TEST_pop_fact_session_load.object_id} = ${pop_dim_videos.video_id} ;;
    sql_where: ${TEST_pop_fact_session_load.event_name} = 'video_load' ;;
    type: left_outer
    relationship: many_to_one
  }

  join: pop_dim_titles {
    sql_on: ${TEST_pop_fact_session_load.object_id} = ${pop_dim_titles.title_id} ;;
    sql_where: ${TEST_pop_fact_session_load.event_name} = 'chapter_load' ;;
    type: left_outer
    relationship: many_to_one
  }

  join: pop_scheduled_dim_videos_enhanced {
    sql_on: ${TEST_pop_fact_session_load.object_id} = ${pop_scheduled_dim_videos_enhanced.video_id} ;;
    sql_where: ${TEST_pop_fact_session_load.event_name} = 'video_load' ;;
    type: left_outer
    relationship: many_to_one
  }

}
