
view: TEST_pop_fact_session_load {
  sql_table_name: `pops-204909.pubsub_snapshot.pop_fact_session_load`
    ;;

  dimension: app_name {
    type: string
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    sql: ${TABLE}.app_region ;;
  }

  dimension: app_version {
    type: string
    sql: ${TABLE}.app_version ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension_group: datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.datetime ;;
  }

  dimension: device_id {
    type: string
    sql: ${TABLE}.device_id ;;
  }

  dimension: device_model {
    type: string
    sql: ${TABLE}.device_model ;;
  }

  dimension: device_type {
    type: string
    sql: ${TABLE}.device_type ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}.event_name ;;
  }

  dimension: headers_from {
    type: string
    sql: ${TABLE}.headers_from ;;
  }

  dimension: headers_user_agent {
    type: string
    sql: ${TABLE}.headers_user_agent ;;
  }

  dimension: ip_address {
    type: string
    sql: ${TABLE}.ip_address ;;
  }

  dimension: language {
    type: string
    sql: ${TABLE}.language ;;
  }

  dimension_group: load {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.load_date ;;
  }

  dimension_group: load_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.load_datetime ;;
  }

  dimension: load_id {
    type: string
    sql: ${TABLE}.load_id ;;
  }

  dimension: load_order {
    type: number
    sql: ${TABLE}.load_order ;;
  }

  dimension: load_status {
    type: string
    sql: ${TABLE}.load_status ;;
  }

  dimension: max_player_location {
    type: number
    sql: ${TABLE}.max_player_location ;;
  }

  dimension_group: min_ping_timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.min_ping_timestamp ;;
  }

  dimension: min_player_location {
    type: number
    sql: ${TABLE}.min_player_location ;;
  }

  dimension: object_id {
    type: string
    sql: ${TABLE}.object_id ;;
  }

  dimension: object_type {
    type: string
    sql: ${TABLE}.object_type ;;
  }

  dimension: os {
    type: string
    sql: ${TABLE}.os ;;
  }

  dimension: os_version {
    type: string
    sql: ${TABLE}.os_version ;;
  }

  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: platform_detail {
    type: string
    sql: ${TABLE}.platform_detail ;;
  }

  dimension: profile_id {
    type: string
    sql: ${TABLE}.profile_id ;;
  }

  dimension_group: session {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.session_date ;;
  }

  dimension_group: session_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.session_datetime ;;
  }

  dimension: session_group {
    type: string
    sql: ${TABLE}.session_group ;;
  }

  dimension: session_id {
    type: string
    sql: ${TABLE}.session_id ;;
  }

  dimension: session_load_id {
    type: string
    sql: ${TABLE}.session_load_id ;;
  }

  dimension: valid_pings {
    type: number
    sql: ${TABLE}.valid_pings ;;
  }

  dimension: views {
    type: number
    sql: ${TABLE}.views ;;
  }

  measure: m_views {
    type: sum
    sql:${views} ;;

  }

  dimension: watch_time_minutes {
    type: number
    sql: ${TABLE}.watch_time_minutes ;;
  }

  measure: m_watch_time_minutes {
    type: number
    sql:SUM(IF(${watch_time_seconds}>=5, ${watch_time_minutes},0)) ;;

  }

  measure: m_distinct_view{
    type: number
    sql:COUNT( DISTINCT IF(${watch_time_seconds}>=5, ${load_id}, NULL)) ;;

  }

  measure: m_distinct_user{
    type: number
    sql:COUNT(DISTINCT ${pop_dim_profiles.user_id}) ;;

  }

  measure: m_distinct_viewer{
    type: number
    sql:COUNT( DISTINCT IF(${watch_time_seconds}>=5, ${pop_dim_profiles.user_id}, NULL)) ;;

  }
  dimension: watch_time_seconds {
    type: number
    sql: ${TABLE}.watch_time_seconds ;;
  }

  dimension: web_browser_name {
    type: string
    sql: ${TABLE}.web_browser_name ;;
  }

  dimension: web_browser_version {
    type: string
    sql: ${TABLE}.web_browser_version ;;
  }

  measure: count {
    type: count
    drill_fields: [web_browser_name, app_name, event_name]
  }

  dimension: user_register_date {
    type: date
    sql:  DATE(${pop_dim_users.register_date});;

  }

  dimension: days_since_register {
    type:  number
    sql: DATE_DIFF(${session_date}, DATE(${user_register_date}), day)  ;;
  }

  dimension: user_group {
    type:  string
    sql: CASE WHEN ${days_since_register} <= 7 THEN 'New Users' ELSE 'Existing Users' END  ;;
  }

  dimension: project_name {
    type:  string
    sql: ${pop_scheduled_dim_videos_enhanced.project_name} ;;
  }


  measure: session_count_distinct {
    type:  number
    sql:  COUNT(DISTINCT ${session_id}) ;;
  }

  measure: watch_time_per_view{
    type:  number
    sql:  ${m_watch_time_minutes} / ${m_distinct_view}  ;;
  }

  measure: watch_time_per_viewer{
    type:  number
    sql:  ${m_watch_time_minutes} / ${m_distinct_viewer}  ;;
  }
}
