view: pop_dim_videos {
  sql_table_name: `pops-204909.pubsub_snapshot.pop_dim_videos`
    ;;

  dimension: age_range {
    type: string
    sql: ${TABLE}.age_range ;;
  }

  dimension: app {
    type: string
    sql: ${TABLE}.app ;;
  }

  dimension: asset_label {
    type: string
    sql: ${TABLE}.asset_label ;;
  }

  dimension: category_name {
    type: string
    sql: ${TABLE}.category_name ;;
  }

  dimension: content_group {
    type: string
    sql: ${TABLE}.content_group ;;
  }

  dimension: content_provider {
    type: string
    sql: ${TABLE}.content_provider ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: director {
    type: string
    sql: ${TABLE}.director ;;
  }

  dimension: format_name {
    type: string
    sql: ${TABLE}.format_name ;;
  }

  dimension: genre {
    type: string
    sql: ${TABLE}.genre ;;
  }

  dimension: is_deleted {
    type: yesno
    sql: ${TABLE}.is_deleted ;;
  }

  dimension: playlist_season_id {
    type: string
    sql: ${TABLE}.playlist_season_id ;;
  }

  dimension: playlist_season_title {
    type: string
    sql: ${TABLE}.playlist_season_title ;;
  }

  dimension: project_id {
    type: string
    sql: ${TABLE}.project_id ;;
  }

  dimension_group: published {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.published_date ;;
  }

  dimension_group: published_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.published_datetime ;;
  }

  dimension: region_restriction_allowed {
    type: string
    sql: ${TABLE}.region_restriction_allowed ;;
  }

  dimension_group: release {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.release_date ;;
  }

  dimension_group: release_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.release_datetime ;;
  }

  dimension: series {
    type: string
    sql: ${TABLE}.series ;;
  }

  dimension: singer {
    type: string
    sql: ${TABLE}.singer ;;
  }

  dimension: tags {
    type: string
    sql: ${TABLE}.tags ;;
  }

  dimension: upload_location {
    type: string
    sql: ${TABLE}.upload_location ;;
  }

  dimension: video_group {
    type: string
    sql: ${TABLE}.video_group ;;
  }

  dimension: video_id {
    type: string
    sql: ${TABLE}.video_id ;;
  }

  dimension: video_length_minutes {
    type: number
    sql: ${TABLE}.video_length_minutes ;;
  }

  dimension: video_length_seconds {
    type: number
    sql: ${TABLE}.video_length_seconds ;;
  }

  dimension: video_title {
    type: string
    sql: ${TABLE}.video_title ;;
  }

  dimension: yt_id {
    type: string
    sql: ${TABLE}.yt_id ;;
  }

  measure: count {
    type: count
    drill_fields: [category_name, format_name]
  }
}
