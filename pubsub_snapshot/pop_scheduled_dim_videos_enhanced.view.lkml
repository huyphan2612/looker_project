view: pop_scheduled_dim_videos_enhanced {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DIM_Videos_Enhanced`
    ;;

  dimension: category_id {
    type: number
    sql: ${TABLE}.CategoryID ;;
  }

  dimension: category_name {
    type: string
    sql: ${TABLE}.CategoryName ;;
  }

  dimension: format_id {
    type: number
    sql: ${TABLE}.format_id ;;
  }

  dimension: format_name {
    type: string
    sql: ${TABLE}.format_name ;;
  }

  dimension: g_genre_id {
    type: number
    sql: ${TABLE}.g_genre_id ;;
  }

  dimension: g_genre_title {
    type: string
    sql: ${TABLE}.g_genre_title ;;
  }

  dimension: pl_playlist_season_id {
    type: string
    sql: ${TABLE}.pl_playlist_season_id ;;
  }

  dimension: pl_playlist_season_title {
    type: string
    sql: ${TABLE}.pl_playlist_season_title ;;
  }

  dimension: project_id {
    type: string
    sql: ${TABLE}.project_id ;;
  }

  dimension: project_name {
    type: string
    sql: ${TABLE}.project_name ;;
  }

  dimension: topic_id {
    type: string
    sql: ${TABLE}.topic_id ;;
  }

  dimension: topic_title {
    type: string
    sql: ${TABLE}.topic_title ;;
  }

  dimension: v_country {
    type: string
    sql: ${TABLE}.v_country ;;
  }

  dimension: video_id {
    type: string
    sql: ${TABLE}.video_id ;;
  }

  dimension: video_title {
    type: string
    sql: ${TABLE}.video_title ;;
  }

  measure: count {
    type: count
    drill_fields: [project_name, category_name, format_name]
  }
}
