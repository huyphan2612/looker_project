view: pop_dim_titles {
  sql_table_name: `pops-204909.pubsub_snapshot.pop_dim_titles`
    ;;

  dimension: age_range {
    type: string
    sql: ${TABLE}.age_range ;;
  }

  dimension: artist {
    type: string
    sql: ${TABLE}.artist ;;
  }

  dimension: asset_label {
    type: string
    sql: ${TABLE}.asset_label ;;
  }

  dimension: author {
    type: string
    sql: ${TABLE}.author ;;
  }

  dimension: chapter_number {
    type: number
    sql: ${TABLE}.chapter_number ;;
  }

  dimension: content_group {
    type: string
    sql: ${TABLE}.content_group ;;
  }

  dimension: content_provider {
    type: string
    sql: ${TABLE}.content_provider ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: introduction {
    type: string
    sql: ${TABLE}.introduction ;;
  }

  dimension: is_deleted {
    type: yesno
    sql: ${TABLE}.is_deleted ;;
  }

  dimension: program {
    type: string
    sql: ${TABLE}.program ;;
  }

  dimension_group: published {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.published_date ;;
  }

  dimension_group: published_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.published_datetime ;;
  }

  dimension: title_id {
    type: string
    sql: ${TABLE}.title_id ;;
  }

  dimension: title_name {
    type: string
    sql: ${TABLE}.title_name ;;
  }

  dimension: total_like {
    type: number
    sql: ${TABLE}.total_like ;;
  }

  measure: count {
    type: count
    drill_fields: [title_name]
  }
}
