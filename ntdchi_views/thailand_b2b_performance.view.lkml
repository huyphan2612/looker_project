view: thailand_b2b_performance {
  derived_table: {
    sql:
      SELECT *
      FROM `pops-bi.Self_Service.POP_Scheduled_DATAMART_B2B_Performance`
      WHERE app_region = 'TH'
    ;;
  }

  parameter: timeframe_selection {
    type: unquoted
    allowed_value: {
      label: "Daily"
      value: "date"
    }
    allowed_value: {
      label: "Weekly"
      value: "week"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
    allowed_value: {
      label: "Quarterly"
      value: "quarter"
    }
    allowed_value: {
      label: "Yearly"
      value: "year"
    }
  }

  parameter: ads_dimension_selection {
    type: unquoted
    allowed_value: {
      label: "Ad Type"
      value: "ad_type"
    }
    allowed_value: {
      label: "Content"
      value: "content"
    }
    allowed_value: {
      label: "Content Provider"
      value: "content_provider"
    }
    allowed_value: {
      label: "Content Category"
      value: "content_category"
    }
    allowed_value: {
      label: "Region"
      value: "region"
    }
    allowed_value: {
      label: "Channel"
      value: "channel"
    }
    allowed_value: {
      label: "Device Vendor"
      value: "device_vendor"
    }
    allowed_value: {
      label: "Device Type"
      value: "device_type"
    }
    allowed_value: {
      label: "Device OS"
      value: "device_os"
    }
    allowed_value: {
      label: "Hour"
      value: "hour"
    }
  }

  parameter: ads_display_dimension_selection {
    type: unquoted
    allowed_value: {
      label: "Ad Format"
      value: "ad_format"
    }
    allowed_value: {
      label: "Ad Unit"
      value: "ad_unit"
    }
  }

  dimension: timeframe {
    type: date
    datatype: date
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${week}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter}
      {% else %}
        ${year}
      {% endif %};;
  }

  dimension: timeframe_order {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${year_week}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month_format}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter_format}
      {% else %}
        ${year_format}
      {% endif %};;
  }

  dimension: timeframe_format {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date_format}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${week_format}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month_format}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter_format}
      {% else %}
        ${year_format}
      {% endif %};;
  }

  dimension: ads_dimension {
    type: string
    group_label: "Ads Dimension"
    sql:
      {% if ads_dimension_selection._parameter_value == 'ad_type' %}
        ${data_type}
      {% elsif ads_dimension_selection._parameter_value == 'content' %}
        ${content_title}
      {% elsif ads_dimension_selection._parameter_value == 'content_provider' %}
        ${content_provider}
      {% elsif ads_dimension_selection._parameter_value == 'content_category' %}
        ${content_category}
      {% elsif ads_dimension_selection._parameter_value == 'region' %}
        ${region}
      {% elsif ads_dimension_selection._parameter_value == 'channel' %}
        ${channel_name}
      {% elsif ads_dimension_selection._parameter_value == 'device_vendor' %}
        ${device_vendor}
      {% elsif ads_dimension_selection._parameter_value == 'device_type' %}
        ${device_type}
      {% elsif ads_dimension_selection._parameter_value == 'device_os' %}
        ${device_os}
      {% else %}
        IF(LENGTH(CAST(${hour} AS STRING)) = 1, CONCAT("0", ${hour}, "h"), CONCAT(${hour}, "h"))
      {% endif %};;
  }

  dimension: ads_display_dimension {
    type: string
    group_label: "Ads Display Dimension"
    sql:
      {% if ads_display_dimension_selection._parameter_value == 'ad_format' %}
        ${ad_format}
      {% else %}
        ${ad_unit}
      {% endif %};;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.date) ;;
  }

  dimension: week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.week ;;
  }

  dimension: year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.year_week ;;
  }

  dimension: week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_format ;;
  }

  dimension: week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_range ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, MONTH) ;;
  }

  dimension: month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.date) ;;
  }

  dimension: quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, QUARTER) ;;
  }

  dimension: quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.date) ;;
  }

  dimension: year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, YEAR) ;;
  }

  dimension: year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.date) ;;
  }

  dimension: hour {
    type: number
    group_label: "Time"
    sql: ${TABLE}.hour ;;
  }

  dimension: data_type {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.data_type ;;
  }

  dimension: app_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_region ;;
  }

  dimension: sub_content_id {
    type: string
    group_label: "Content"
    sql: ${TABLE}.sub_content_id ;;
  }

  dimension: sub_content_title {
    type: string
    group_label: "Content"
    sql: ${TABLE}.sub_content_title ;;
  }

  dimension: content_id {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_id ;;
  }

  dimension: content_title {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_title ;;
  }

  dimension: content_category {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_category ;;
  }

  dimension: content_provider {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_provider ;;
  }

  dimension: asset_label {
    type: string
    group_label: "Content"
    sql: ${TABLE}.asset_label ;;
  }

  dimension: ad_partner_id {
    type: number
    group_label: "Ads Dimension"
    sql: ${TABLE}.ad_partner_id ;;
  }

  dimension: ad_partner {
    type: string
    group_label: "Ads Dimension"
    sql: ${TABLE}.ad_partner ;;
  }

  dimension: deal_demand_source {
    type: string
    group_label: "Ads Dimension"
    sql: ${TABLE}.deal_demand_source ;;
  }

  dimension: deal_id {
    type: string
    group_label: "Ads Dimension"
    sql: ${TABLE}.deal_id ;;
  }

  dimension: deal_name {
    type: string
    group_label: "Ads Dimension"
    sql: ${TABLE}.deal_name ;;
  }

  dimension: creative_id {
    type: string
    group_label: "Ads Dimension"
    sql: ${TABLE}.creative_id ;;
  }

  dimension: creative_name {
    type: string
    group_label: "Ads Dimension"
    sql: ${TABLE}.creative_name ;;
  }

  dimension: channel_id {
    type: number
    group_label: "Ads Dimension"
    sql: ${TABLE}.channel_id ;;
  }

  dimension: channel_name {
    type: string
    group_label: "Ads Dimension"
    sql: ${TABLE}.channel_name ;;
  }

  dimension: country {
    type: string
    group_label: "Ads Dimension"
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: region {
    type: string
    group_label: "Ads Dimension"
    sql: ${TABLE}.region ;;
  }

  dimension: device_type {
    type: string
    group_label: "Ads Dimension"
    sql: ${TABLE}.device_type ;;
  }

  dimension: device_os {
    type: string
    group_label: "Ads Dimension"
    sql: ${TABLE}.device_os ;;
  }

  dimension: device_vendor {
    type: string
    group_label: "Ads Dimension"
    sql: ${TABLE}.device_vendor ;;
  }

  dimension: platform {
    type: string
    group_label: "Ads Dimension"
    sql: ${TABLE}.platform ;;
  }

  dimension: ad_duration {
    type: number
    group_label: "Ads Dimension"
    sql: ${TABLE}.ad_duration ;;
  }

  dimension: ad_unit {
    type: string
    group_label: "Ads Display Dimension"
    sql: ${TABLE}.ad_unit ;;
  }

  dimension: ad_format {
    type: string
    group_label: "Ads Display Dimension"
    sql: ${TABLE}.format ;;
  }

  measure: ad_requests {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.ad_requests ;;
    value_format: "#,##0"
  }

  measure: ad_coverage {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.coverage ;;
    value_format: "#,##0"
  }

  measure: matched_requests {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.matched_requests ;;
    value_format: "#,##0"
  }

  measure: ad_impressions {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.ad_impressions ;;
    value_format: "#,##0"
  }

  measure: viewable_impressions {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.viewable_impressions ;;
    value_format: "#,##0"
  }

  measure: views_25pct {
    type: sum
    label: "Views 25%"
    group_label: "Measure"
    sql: ${TABLE}.views_25pct ;;
    value_format: "#,##0"
  }

  measure: views_50pct {
    type: sum
    label: "Views 50%"
    group_label: "Measure"
    sql: ${TABLE}.views_50pct ;;
    value_format: "#,##0"
  }

  measure: views_75pct {
    type: sum
    label: "Views 75%"
    group_label: "Measure"
    sql: ${TABLE}.views_75pct ;;
    value_format: "#,##0"
  }

  measure: views_100pct {
    type: sum
    label: "Views 100%"
    group_label: "Measure"
    sql: ${TABLE}.views_100pct ;;
    value_format: "#,##0"
  }

  measure: clicks {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.clicks ;;
    value_format: "#,##0"
  }

  measure: estimated_gross_revenue {
    type: sum
    group_label: "Measure"
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ${TABLE}.estimated_gross_revenue
      ELSE 0 END ;;
    value_format: "$#,##0.00"
  }

  measure: estimated_net_revenue {
    type: sum
    group_label: "Measure"
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ${TABLE}.estimated_net_revenue
      ELSE 0 END ;;
    value_format: "$#,##0.00"
  }

  measure: ads_bid_rate {
    type: number
    group_label: "Ratio"
    sql: IF(${ad_requests} = 0, 0, ${ad_coverage} / ${ad_requests}) ;;
    value_format: "#,##0.0%"
  }

  measure: ads_display_bid_rate {
    type: number
    group_label: "Ratio"
    sql: IF(${ad_requests} = 0, 0, ${matched_requests} / ${ad_requests}) ;;
    value_format: "#,##0.0%"
  }

  measure: show_rate {
    type: number
    group_label: "Ratio"
    sql: IF(${matched_requests} = 0, 0, ${ad_impressions} / ${matched_requests}) ;;
    value_format: "#,##0.0%"
  }

  measure: fill_rate {
    type: number
    group_label: "Ratio"
    sql: IF(${ad_requests} = 0, 0, ${ad_impressions} / ${ad_requests}) ;;
    value_format: "#,##0.0%"
  }

  measure: viewable_rate {
    type: number
    group_label: "Ratio"
    sql: IF(${ad_impressions} = 0, 0, ${viewable_impressions} / ${ad_impressions}) ;;
    value_format: "#,##0.0%"
  }

  measure: cvr {
    type: number
    label: "CVR"
    group_label: "Ratio"
    sql: IF(${ad_impressions} = 0, 0, ${views_100pct} / ${ad_impressions}) ;;
    value_format: "#,##0.0%"
  }

  measure: ctr {
    type: number
    label: "CTR"
    group_label: "Ratio"
    sql: IF(${ad_impressions} = 0, 0, ${clicks} / ${ad_impressions}) ;;
    value_format: "#,##0.0%"
  }

  measure: gross_ecpm {
    type: number
    label: "Gross eCPM"
    group_label: "Ratio"
    sql: IF(${ad_impressions} = 0, 0, ${estimated_gross_revenue} / ${ad_impressions} * 1000) ;;
    value_format: "$#,##0.00"
  }

  measure: net_ecpm {
    type: number
    label: "Net eCPM"
    group_label: "Ratio"
    sql: IF(${ad_impressions} = 0, 0, ${estimated_net_revenue} / ${ad_impressions} * 1000) ;;
    value_format: "$#,##0.00"
  }
}
