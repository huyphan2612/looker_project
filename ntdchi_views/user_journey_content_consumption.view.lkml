view: user_journey_content_consumption {
  derived_table: {
    sql:
      SELECT
          app_name, app_region, date, session_id, user_id, register_date, web_platform, platform_detail,
          REGEXP_REPLACE(content_name, 'DIEUCHI', ', ') AS content_name, contents,
          SPLIT(content_name, 'DIEUCHI')[OFFSET(0)] AS content_name_1,
          IF((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 > 0, SPLIT(content_name, 'DIEUCHI')[OFFSET(1)], NULL) AS content_name_2,
          IF((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 > 1, SPLIT(content_name, 'DIEUCHI')[OFFSET(2)], NULL) AS content_name_3,
          IF((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 > 2, SPLIT(content_name, 'DIEUCHI')[OFFSET(3)], NULL) AS content_name_4,
          IF((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 > 3, SPLIT(content_name, 'DIEUCHI')[OFFSET(4)], NULL) AS content_name_5,
          SPLIT(content_name, 'DIEUCHI')[OFFSET(CAST(ROUND((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 - 0, 0) AS INT64))] AS content_name_last_1,
          IF((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 > 0,
            SPLIT(content_name, 'DIEUCHI')[OFFSET(CAST(ROUND((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 - 1, 0) AS INT64))], NULL) AS content_name_last_2,
          IF((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 > 1,
            SPLIT(content_name, 'DIEUCHI')[OFFSET(CAST(ROUND((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 - 2, 0) AS INT64))], NULL) AS content_name_last_3,
          IF((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 > 2,
            SPLIT(content_name, 'DIEUCHI')[OFFSET(CAST(ROUND((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 - 3, 0) AS INT64))], NULL) AS content_name_last_4,
          IF((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 > 3,
            SPLIT(content_name, 'DIEUCHI')[OFFSET(CAST(ROUND((LENGTH(content_name) - LENGTH(REGEXP_REPLACE(content_name, 'DIEUCHI', ''))) / 7 - 4, 0) AS INT64))], NULL) AS content_name_last_5,
          viewers, views, watch_time_minutes
      FROM (
          SELECT
              app_name, app_region, date, session_id, user_id, register_date, web_platform, platform_detail,
              STRING_AGG(content_name, 'DIEUCHI') AS content_name,
              COUNT(content_name) AS contents,
              COUNT(DISTINCT viewer_id) AS viewers,
              COUNT(DISTINCT all_view5s_load_id) AS views,
              SUM(watch_time_minutes) AS watch_time_minutes
          FROM (
              SELECT
                  app_name, app_region, date, session_id, user_id, register_date, web_platform, platform_detail,
                  CONCAT(ROW_NUMBER() OVER (PARTITION BY session_id ORDER BY datetime), '. ', content_name) AS content_name,
                  viewer_id, all_view5s_load_id, watch_time_minutes
              FROM (
                  SELECT
                      app_name, app_region, datetime, session_date AS date, session_id, user_id, register_date, web_platform, platform_detail,
                      IF(event_name = "video_load", CONCAT('Project ', content_name),
                          IF(event_name  = "chapter_load", CONCAT('Comic ', content_name),
                              IF(event_name = "livestream_load", CONCAT('Livestream ', content_name), content_name))) AS content_name,
                      viewer_id, all_view5s_load_id, watch_time_minutes
                  FROM `pops-bi.Self_Service.POP_Scheduled_DATAMART_Both_App_Performance`
                  WHERE event_name <> 'session_load' AND content_name IS NOT NULL
              )
          )
          GROUP BY app_name, app_region, date, session_id, user_id, register_date, web_platform, platform_detail
      )
    ;;
    # persist_for: "24 hours"
    sql_trigger_value: SELECT COUNT(*) FROM `pops-bi.Self_Service.POP_Scheduled_DATAMART_Both_App_Performance` ;;
    partition_keys: ["date"]
    cluster_keys: ["app_region", "app_name", "platform_detail" ]
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: register_date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.register_date ;;
  }

  dimension: app_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    group_label: "Dimension"
    sql: IF(${TABLE}.app_region = 'TH', 'TH', 'VN') ;;
  }

  dimension: web_platform {
    type: string
    group_label: "Dimension"
    sql:${TABLE}.web_platform ;;
  }

  dimension: platform_detail {
    type: string
    group_label: "Dimension"
    sql:${TABLE}.platform_detail ;;
  }

  dimension: content_name {
    type: string
    group_label: "Content"
    sql:${TABLE}.content_name ;;
  }

  dimension: contents {
    type: number
    group_label: "Content"
    sql:${TABLE}.contents ;;
  }

  dimension: content_name_1 {
    type: string
    group_label: "Content"
    sql:${TABLE}.content_name_1 ;;
  }

  dimension: content_name_2 {
    type: string
    group_label: "Content"
    sql:${TABLE}.content_name_2 ;;
  }

  dimension: content_name_3 {
    type: string
    group_label: "Content"
    sql:${TABLE}.content_name_3 ;;
  }

  dimension: content_name_4 {
    type: string
    group_label: "Content"
    sql:${TABLE}.content_name_4 ;;
  }

  dimension: content_name_5 {
    type: string
    group_label: "Content"
    sql:${TABLE}.content_name_5 ;;
  }

  dimension: content_name_last_1 {
    type: string
    group_label: "Content"
    sql:${TABLE}.content_name_last_1 ;;
  }

  dimension: content_name_last_2 {
    type: string
    group_label: "Content"
    sql:${TABLE}.content_name_last_2 ;;
  }

  dimension: content_name_last_3 {
    type: string
    group_label: "Content"
    sql:${TABLE}.content_name_last_3 ;;
  }

  dimension: content_name_last_4 {
    type: string
    group_label: "Content"
    sql:${TABLE}.content_name_last_4 ;;
  }

  dimension: content_name_last_5 {
    type: string
    group_label: "Content"
    sql:${TABLE}.content_name_last_5 ;;
  }

  measure: viewers {
    type: sum
    sql:${TABLE}.viewers ;;
  }

  measure: views {
    type: sum
    sql:${TABLE}.views ;;
  }

  measure: watch_time_minutes {
    type: sum
    sql:${TABLE}.watch_time_minutes ;;
    value_format: "#,##0"
  }
}
