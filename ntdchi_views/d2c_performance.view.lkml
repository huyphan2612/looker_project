view: d2c_performance {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_D2C_Performance`
    ;;

  parameter: timeframe_selection {
    type: unquoted
    allowed_value: {
      label: "Daily"
      value: "date"
    }
    allowed_value: {
      label: "Weekly"
      value: "week"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
    allowed_value: {
      label: "Quarterly"
      value: "quarter"
    }
    allowed_value: {
      label: "Yearly"
      value: "year"
    }
  }

  dimension: timeframe {
    type: date
    datatype: date
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${week}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter}
      {% else %}
        ${year}
      {% endif %};;
  }

  dimension: timeframe_order {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${year_week}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month_format}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter_format}
      {% else %}
        ${year_format}
      {% endif %};;
  }

  dimension: timeframe_format {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date_format}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${week_format}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month_format}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter_format}
      {% else %}
        ${year_format}
      {% endif %};;
  }

  parameter: currency {
    type: unquoted
    allowed_value: {
      label: "Revenue (in USD)"
      value: "revenue_usd"
    }
    allowed_value: {
      label: "Revenue (in VND)"
      value: "revenue_vnd"
    }
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.date) ;;
  }

  dimension: week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.week ;;
  }

  dimension: year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.year_week ;;
  }

  dimension: week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_format ;;
  }

  dimension: week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_range ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, MONTH) ;;
  }

  dimension: month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.date) ;;
  }

  dimension: quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, QUARTER) ;;
  }

  dimension: quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.date) ;;
  }

  dimension: year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, YEAR) ;;
  }

  dimension: year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.date) ;;
  }

  dimension: valid_from {
    type: date
    datatype: date
    group_label: "Time"
    sql: IFNULL(${TABLE}.valid_from, ${date}) ;;
  }

  dimension: valid_to {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.valid_to ;;
  }

  dimension: register_date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.register_date ;;
  }

  dimension: data_type {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.data_type ;;
  }

  dimension: data_status {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.data_status ;;
  }

  dimension: revenue_stream {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.revenue_stream ;;
  }

  dimension: country {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.country ;;
  }

  dimension: entity_country {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.entity_country ;;
  }

  dimension: package {
    type: string
    group_label: "Dimension"
    sql: IF(${TABLE}.package IN ('Unlock Chapter', 'Unlock Video'), 'Unlock Content', ${TABLE}.package) ;;
    drill_fields: [partner, product_group]
  }

  dimension: product_group_id {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.product_group_id ;;
  }

  dimension: product_group {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.product_group_name ;;
    drill_fields: [product_name]
  }

  dimension: product_id {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.product_id ;;
  }

  dimension: product_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.product_name ;;
  }

  dimension: partner {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.partner ;;
  }

  dimension: user_type {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.user_type ;;
  }

  dimension: user_id {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.user_id ;;
  }

  dimension: user_email {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.user_email ;;
  }

  measure: users {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.user_id) ;;
    value_format: "#,##0"
  }

  dimension: star_price_vnd {
    type: number
    group_label: "Dimension"
    sql: ${TABLE}.star_price_vnd ;;
    value_format: "\"VND \"#,##0.00"
  }

  dimension: star_price_usd {
    type: number
    group_label: "Dimension"
    sql: ${TABLE}.star_price_usd ;;
    value_format: "$#,##0.00"
  }

  measure: lastest_month {
    type: date
    datatype: date
    group_label: "Measure"
    sql: MAX(${month}) ;;
  }

  measure: quantity {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.quantity ;;
    value_format: "#,##0"
  }

  measure: absolute_quantity {
    type: sum
    label: "Absolute Quantity"
    group_label: "Measure"
    sql: ABS(${TABLE}.quantity) ;;
    value_format: "#,##0"
  }

  measure: stars {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.stars ;;
    value_format: "#,##0"
  }

  measure: absolute_stars {
    type: sum
    group_label: "Measure"
    sql: ABS(${TABLE}.stars) ;;
    value_format: "#,##0"
  }

  measure: fee_usd {
    type: sum
    group_label: "Measure"
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ${TABLE}.fee_usd
      ELSE 0 END ;;
    value_format: "$#,##0.00"
  }

  measure: revenue_vnd {
    type: sum
    label: "Revenue (in VND)"
    group_label: "Measure"
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ${TABLE}.revenue_vnd
      ELSE 0 END ;;
    value_format: "\"VND \"#,##0.00"
  }

  measure: absolute_revenue_vnd {
    type: sum
    label: "Absolute Revenue (in VND)"
    group_label: "Measure"
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ABS(${TABLE}.revenue_vnd)
      ELSE 0 END ;;
    value_format: "\"VND \"#,##0.00"
  }

  measure: target_revenue_usd {
    type: sum
    label: "Target Revenue (in USD)"
    group_label: "Measure"
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ${TABLE}.target_revenue
      ELSE 0 END ;;
    value_format: "$#,##0.00"
  }

  measure: revenue_usd {
    type: sum
    label: "Revenue (in USD)"
    group_label: "Measure"
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ${TABLE}.revenue_usd
      ELSE 0 END ;;
    value_format: "$#,##0.00"
  }

  measure: absolute_revenue_usd {
    type: sum
    label: "Absolute Revenue (in USD)"
    group_label: "Measure"
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ABS(${TABLE}.revenue_usd)
      ELSE 0 END ;;
    value_format: "$#,##0.00"
  }

  measure: target_actual_revenue_usd {
    type: sum
    label: "Target Actual Revenue (in USD)"
    group_label: "Measure"
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN IF(${TABLE}.data_type = 'Target', ${TABLE}.target_revenue, ABS(${TABLE}.revenue_usd))
      ELSE 0 END ;;
    value_format: "$#,##0.00"
  }

  measure: revenue {
    type: number
    group_label: "Measure"
    sql:
      {% if currency._parameter_value == 'USD' %}
        ${revenue_usd}
      {% else %}
        ${revenue_vnd}
      {% endif %};;
    value_format: "#,##0.00"
  }
}
