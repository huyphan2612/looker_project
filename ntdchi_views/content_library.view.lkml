view: content_library {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_Content_Library`
    ;;

  dimension: app_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_name ;;
  }

  dimension: country {
    type: string
    group_label: "Dimension"
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: object {
    type: string
    group_label: "Object"
    sql: ${TABLE}.object ;;
  }

  dimension: object_id {
    type: string
    group_label: "Object"
    sql: ${TABLE}.object_id ;;
  }

  dimension: object_title {
    type: string
    group_label: "Object"
    sql: ${TABLE}.object_title ;;
  }

  dimension: object_index {
    type: number
    group_label: "Object"
    sql: ${TABLE}.object_index ;;
  }

  dimension: object_group {
    type: string
    group_label: "Object"
    sql: ${TABLE}.object_group ;;
  }

  dimension: object_type {
    type: string
    group_label: "Object"
    sql: ${TABLE}.object_type ;;
  }

  dimension: object_length {
    type: number
    group_label: "Object"
    sql: ${TABLE}.object_length ;;
  }

  dimension: object_deleted {
    type: yesno
    group_label: "Object"
    sql: ${TABLE}.object_deleted ;;
  }

  dimension: publish_date {
    type: date
    datatype: date
    group_label: "Object"
    sql: ${TABLE}.publish_date ;;
  }

  dimension: content_id {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_id ;;
  }

  dimension: content_name {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_name ;;
  }

  dimension: content_category {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_category ;;
  }

  measure: objects {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.object_id) ;;
    value_format: "#,##0"
  }

  measure: contents {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.content_id) ;;
    value_format: "#,##0"
  }

  measure: object_length_mins {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.object_length ;;
    value_format: "#,##0"
  }
}
