view: thailand_search_keywords_and_selected_items {
  derived_table: {
    sql:
      SELECT *
      FROM `pops-bi.Self_Service.POP_Scheduled_DATAMART_Search_Keywords_And_Selected_Items`
      WHERE app_region = 'TH'
    ;;
  }

  dimension: date_hour {
    type: date_hour
    datatype: datetime
    group_label: "Time"
    sql: ${TABLE}.datetime ;;
  }

  dimension: date_hour_format {
    type: date_hour_of_day
    datatype: datetime
    group_label: "Time"
    sql: ${TABLE}.datetime ;;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.date) ;;
  }

  dimension: week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.week ;;
  }

  dimension: year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.year_week ;;
  }

  dimension: week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_format ;;
  }

  dimension: week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_range ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, MONTH) ;;
  }

  dimension: month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.date) ;;
  }

  dimension: quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, QUARTER) ;;
  }

  dimension: quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.date) ;;
  }

  dimension: year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, YEAR) ;;
  }

  dimension: year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.date) ;;
  }

  dimension: app_region {
    type: string
    group_label: "Dimension"
    sql: IF(${TABLE}.app_region = 'TH', 'TH', 'VN') ;;
  }

  dimension: event_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.event_name ;;
  }

  dimension: session_id {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.session_id ;;
  }

  dimension: user_id {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.user_id ;;
  }

  dimension: method {
    type: string
    group_label: "Search"
    sql: ${TABLE}.method ;;
  }

  dimension: keyword {
    type: string
    group_label: "Search"
    sql: ${TABLE}.keyword ;;
  }

  dimension: item_type {
    type: string
    group_label: "Item"
    sql: ${TABLE}.item_type ;;
  }

  dimension: item_index {
    type: number
    group_label: "Item"
    sql: ${TABLE}.item_index ;;
  }

  dimension: item_id {
    type: string
    group_label: "Item"
    sql: ${TABLE}.item_id ;;
  }

  dimension: item_title {
    type: string
    group_label: "Item"
    sql: ${TABLE}.item_title ;;
  }

  dimension: item_project {
    type: string
    group_label: "Item"
    sql: ${TABLE}.item_project ;;
  }

  measure: count {
    type: count
  }
}
