view: duplicated_checking {
  derived_table: {
    sql:
      SELECT * FROM `pops-bi.Self_Service.POP_Scheduled_DATAMART_Duplicated_Facts`
      UNION ALL
      SELECT * FROM `pops-bi.Self_Service.POP_Scheduled_DATAMART_Duplicated_Dims`
    ;;
  }

  dimension: table {
    type: string
    sql: ${TABLE}.table ;;
  }

  dimension: is_duplicated {
    type: yesno
    sql: ${TABLE}.is_duplicated ;;
  }

  measure: count_rows {
    type: sum
    sql: ${TABLE}.count_rows ;;
  }

  measure: distinct_keys {
    type: sum
    sql: ${TABLE}.distinct_keys ;;
  }
}
