view: comic_content_revenue {
  sql_table_name: `pops-bi.Vietnam_Comic.POP_Scheduled_DATAMART_Comic_Content_Revenue`
    ;;

  parameter: timeframe_selection {
    type: unquoted
    allowed_value: {
      label: "Daily"
      value: "date"
    }
    allowed_value: {
      label: "Weekly"
      value: "week"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
    allowed_value: {
      label: "Quarterly"
      value: "quarter"
    }
    allowed_value: {
      label: "Yearly"
      value: "year"
    }
  }

  parameter: comparison_selection {
    type: unquoted
    allowed_value: {
      label: "Total"
      value: "total"
    }
    allowed_value: {
      label: "By Comic"
      value: "by_comic"
    }
    allowed_value: {
      label: "By Chapter"
      value: "by_chapter"
    }
  }

  dimension: timeframe {
    type: date
    datatype: date
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${week}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter}
      {% else %}
        ${year}
      {% endif %};;
  }

  dimension: timeframe_order {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${year_week}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month_format}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter_format}
      {% else %}
        ${year_format}
      {% endif %};;
  }

  dimension: timeframe_format {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date_format}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${week_format}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month_format}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter_format}
      {% else %}
        ${year_format}
      {% endif %};;
  }

  dimension: comparison_level {
    type: string
    sql:
      {% if comparison_selection._parameter_value == 'total' %}
        'Total'
      {% elsif comparison_selection._parameter_value == 'by_comic' %}
        ${comic_name}
      {% else %}
        ${chapter_name}
      {% endif %};;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.date) ;;
  }

  dimension: week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.week ;;
  }

  dimension: year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.year_week ;;
  }

  dimension: week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_format ;;
  }

  dimension: week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_range ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, MONTH) ;;
  }

  dimension: month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.date) ;;
  }

  dimension: quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, QUARTER) ;;
  }

  dimension: quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.date) ;;
  }

  dimension: year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, YEAR) ;;
  }

  dimension: year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.date) ;;
  }

  dimension: comic_id {
    type: string
    sql: ${TABLE}.comic_id ;;
  }

  dimension: comic_name {
    type: string
    sql: ${TABLE}.comic_name ;;
    drill_fields: [chapter_name]
  }

  dimension: chapter_id {
    type: string
    sql: ${TABLE}.chapter_id ;;
  }

  dimension: chapter_name {
    type: string
    sql: ${TABLE}.chapter_name ;;
  }

  dimension: chapter_type {
    type: string
    sql: ${TABLE}.chapter_type ;;
  }

  dimension: stars {
    type: number
    sql: ${TABLE}.total_stars ;;
  }

  dimension: content_provider {
    type: string
    sql: ${TABLE}.content_provider ;;
  }

  dimension: asset_label {
    type: string
    sql: ${TABLE}.asset_label ;;
  }

  dimension: program {
    type: string
    sql: ${TABLE}.program ;;
  }

  measure: chapters {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.chapter_id) ;;
  }

  measure: paid_chapters {
    type: number
    sql: COUNT(DISTINCT IF(${TABLE}.total_unlocks > 0, ${TABLE}.chapter_id, NULL)) ;;
  }

  measure: viewers {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.user_id) ;;
  }

  measure: paid_viewers {
    type: number
    sql: COUNT(DISTINCT IF(${TABLE}.total_unlocks > 0, ${TABLE}.user_id, NULL)) ;;
  }

  measure: free_viewers {
    type: number
    sql: ${viewers} - ${paid_viewers} ;;
  }

  measure: views {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.view_load_id) ;;
  }

  measure: paid_views {
    type: number
    sql: COUNT(DISTINCT IF(${TABLE}.chapter_type = 'paid', ${TABLE}.view_load_id, NULL)) ;;
  }

  measure: free_views {
    type: number
    sql: ${views} - ${paid_views} ;;
  }

  measure: unique_views {
    type: number
    sql: COUNT(DISTINCT IF(${TABLE}.view_load_id IS NULL, NULL, CONCAT(${TABLE}.user_id, ${TABLE}.chapter_id))) ;;
  }

  measure: total_unlocks {
    type: sum
    sql: ${TABLE}.total_unlocks ;;
  }

  measure: total_stars {
    type: sum
    sql: ${TABLE}.total_stars ;;
  }

  measure: star_price_usd {
    type: max
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ${TABLE}.star_price_usd
      ELSE 0 END ;;
    value_format: "$#,##0.000000"
  }

  measure: revenue_usd {
    type: sum
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ${TABLE}.revenue_usd
      ELSE 0 END ;;
    value_format: "$#,##0.00"
  }

  measure: star_price_vnd {
    type: max
    label: "Star Price VND"
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ${TABLE}.star_price_vnd
      ELSE 0 END ;;
    value_format: "\"VND \"#,##0.00"
  }

  measure: revenue_vnd {
    type: sum
    label: "Revenue VND"
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ${TABLE}.revenue_vnd
      ELSE 0 END ;;
    value_format: "\"VND \"#,##0.00"
  }
}
