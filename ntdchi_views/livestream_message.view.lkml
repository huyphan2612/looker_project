view: livestream_message {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_Livestream_Message`
    ;;

  dimension: date_hour {
    type: date_hour
    datatype: datetime
    group_label: "Time"
    sql: DATETIME_SUB(${TABLE}.datetime, INTERVAL 7 HOUR) ;;
  }

  dimension: date_hour_format {
    type: date_hour_of_day
    datatype: datetime
    group_label: "Time"
    sql: DATETIME_SUB(${TABLE}.datetime, INTERVAL 7 HOUR) ;;
  }

  dimension: date {
    type: date
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: app_region {
    type: string
    sql: ${TABLE}.app_region ;;
  }

  dimension: action {
    type: string
    sql: ${TABLE}.action ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension: room_id {
    type: string
    sql: ${TABLE}.room_id ;;
  }

  dimension: stream_id {
    type: string
    sql: ${TABLE}.stream_id ;;
  }

  dimension: stream_title {
    type: string
    sql: ${TABLE}.stream_title ;;
  }

  dimension: raw_message {
    type: string
    sql: ${TABLE}.raw_message ;;
  }

  dimension: display_message {
    type: string
    sql: ${TABLE}.display_message ;;
  }

  dimension: photo_url {
    type: string
    sql: ${TABLE}.object_photo ;;
  }

  measure: users {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.user_id) ;;
  }

  measure: messages {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.message_id) ;;
  }

  measure: stars {
    type: sum
    sql: ${TABLE}.stars ;;
  }
}
