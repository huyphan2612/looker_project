view: first_joined_date_checking {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_First_Joined_Date_Checking`
    ;;

  dimension: chi_date {
    type: date
    datatype: date
    sql: ${TABLE}.chi_date ;;
  }

  dimension: huy_date {
    type: date
    datatype: date
    sql: ${TABLE}.huy_date ;;
  }

  dimension: date_difference {
    type: number
    sql: ${TABLE}.date_difference ;;
  }

  dimension: is_equal {
    type: yesno
    sql: ${TABLE}.is_equal ;;
  }

  dimension: app_name {
    type: string
    sql: ${TABLE}.app_name ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  measure: users {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.user_id) ;;
  }
}
