view: session_load_checking {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_Session_Load_Checking`
    ;;

  dimension: date {
    type: date
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: data_source {
    type: string
    sql: ${TABLE}.data_source ;;
  }

  dimension: app_name {
    type: string
    sql: ${TABLE}.app_name ;;
  }

  measure: sessions {
    type: sum
    sql: ${TABLE}.sessions ;;
  }

  measure: loads {
    type: sum
    sql: ${TABLE}.loads ;;
  }

  measure: video_plays {
    type: sum
    sql: ${TABLE}.video_plays ;;
  }

  measure: profiles {
    type: sum
    sql: ${TABLE}.profiles ;;
  }

  measure: watch_time {
    type: sum
    sql: ${TABLE}.watch_time_minutes ;;
    value_format: "#,##0"
  }
}
