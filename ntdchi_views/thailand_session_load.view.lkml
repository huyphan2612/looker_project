view: thailand_session_load {
  sql_table_name: `pops-bi.Thailand.POP_Scheduled_DATAMART_Session_Load`
    ;;

  dimension: date {
    type: date
    datatype: date
    group_label: "Event Date"
    sql: ${TABLE}.date ;;
  }

  dimension: week {
    type: date
    datatype: date
    group_label: "Event Date"
    sql: ${TABLE}.week ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Event Date"
    sql: ${TABLE}.month ;;
  }

  dimension: quarter {
    type: date
    datatype: date
    group_label: "Event Date"
    sql: ${TABLE}.quarter ;;
  }

  dimension: week_period {
    type: string
    group_label: "Event Date"
    sql: ${TABLE}.week_period ;;
  }

  dimension: session_date {
    type: date
    datatype: date
    group_label: "Session Date"
    sql: ${TABLE}.session_date ;;
  }

  dimension: session_week {
    type: date
    datatype: date
    group_label: "Session Date"
    sql: ${TABLE}.session_week ;;
  }

  dimension: session_month {
    type: date
    datatype: date
    group_label: "Session Date"
    sql: ${TABLE}.session_month ;;
  }

  dimension: session_quarter {
    type: date
    datatype: date
    group_label: "Session Date"
    sql: ${TABLE}.session_quarter ;;
  }

  dimension: load_date {
    type: date
    datatype: date
    group_label: "Load Date"
    sql: ${TABLE}.load_date ;;
  }

  dimension: load_week {
    type: date
    datatype: date
    group_label: "Load Date"
    sql: ${TABLE}.load_week ;;
  }

  dimension: load_month {
    type: date
    datatype: date
    group_label: "Load Date"
    sql: ${TABLE}.load_month ;;
  }

  dimension: load_quarter {
    type: date
    datatype: date
    group_label: "Load Date"
    sql: ${TABLE}.load_quarter ;;
  }

  dimension: register_date {
    type: date
    datatype: date
    group_label: "Register Date"
    sql: ${TABLE}.register_date ;;
  }

  dimension: register_week {
    type: date
    datatype: date
    group_label: "Register Date"
    sql: ${TABLE}.register_week ;;
  }

  dimension: register_month {
    type: date
    datatype: date
    group_label: "Register Date"
    sql: ${TABLE}.register_month ;;
  }

  dimension: register_quarter {
    type: date
    datatype: date
    group_label: "Register Date"
    sql: ${TABLE}.register_quarter ;;
  }

  dimension: app_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_region ;;
  }

  dimension: event_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.event_name ;;
    drill_fields: [detail*]
  }

  dimension: session_id {
    type: string
    group_label: "IDs"
    sql: ${TABLE}.session_id ;;
  }

  dimension: load_id {
    type: string
    group_label: "IDs"
    sql: ${TABLE}.load_id ;;
  }

  dimension: object_id {
    type: string
    group_label: "IDs"
    sql: ${TABLE}.object_id ;;
  }

  dimension: object_title {
    type: string
    group_label: "Content"
    sql: ${TABLE}.object_title ;;
  }

  dimension: object_length_seconds {
    type: number
    group_label: "Content"
    sql: ${TABLE}.object_length_seconds ;;
  }

  dimension: object_length_minutes {
    type: number
    group_label: "Content"
    sql: ${TABLE}.object_length_minutes ;;
  }

  dimension: object_type {
    type: string
    group_label: "Content"
    sql: ${TABLE}.object_type ;;
  }

  dimension: content_id {
    type: string
    group_label: "IDs"
    sql: ${TABLE}.content_id ;;
  }

  dimension: content_name {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_name ;;
  }

  dimension: device_type {
    type: string
    group_label: "Device"
    sql: ${TABLE}.device_type ;;
  }

  dimension: platform {
    type: string
    group_label: "Device"
    sql: ${TABLE}.platform ;;
  }

  dimension: platform_detail {
    type: string
    group_label: "Device"
    sql: ${TABLE}.platform_detail ;;
  }

  dimension: utm_campaign {
    type: string
    group_label: "Content"
    sql: ${TABLE}.utm_campaign ;;
  }

  dimension: utm_medium {
    type: string
    group_label: "Content"
    sql: ${TABLE}.utm_medium ;;
  }

  dimension: utm_source {
    type: string
    group_label: "Content"
    sql: ${TABLE}.utm_source ;;
  }

  dimension: traffic_source_name {
    type: string
    group_label: "Content"
    sql: ${TABLE}.traffic_source_name ;;
  }

  dimension: traffic_source_medium {
    type: string
    group_label: "Content"
    sql: ${TABLE}.traffic_source_medium ;;
  }

  dimension: traffic_source_source {
    type: string
    group_label: "Content"
    sql: ${TABLE}.traffic_source_source ;;
  }

  dimension: profile_id {
    type: string
    group_label: "IDs"
    sql: ${TABLE}.profile_id ;;
  }

  dimension: user_id {
    type: string
    group_label: "IDs"
    sql: ${TABLE}.user_id ;;
  }

  dimension: viewer_id {
    type: string
    group_label: "IDs"
    sql: ${TABLE}.viewer_id ;;
  }

  dimension: max_player_location {
    type: number
    group_label: "Dimension"
    sql: ${TABLE}.max_player_location ;;
  }

  dimension: min_player_location {
    type: number
    group_label: "Dimension"
    sql: ${TABLE}.min_player_location ;;
  }

  dimension: valid_pings {
    type: number
    group_label: "Dimension"
    sql: ${TABLE}.valid_pings ;;
  }

  dimension: new_user_by_date {
    type: string
    group_label: "Dimension"
    sql: IF(${TABLE}.date = ${TABLE}.register_date, 'New', 'Existing') ;;
  }

  measure: users {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.user_id) ;;
  }

  measure: viewers {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.viewer_id) ;;
  }

  measure: views {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.load_id) ;;
  }

  measure: views_5s {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.view5s_load_id) ;;
  }

  measure: watch_time_seconds {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.watch_time_seconds ;;
  }

  measure: watch_time_minutes {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.watch_time_minutes ;;
  }

  measure: watch_time_minutes_5s {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.watch_time_minutes_5s ;;
  }

  measure: records {
    type: count
    group_label: "Measure"
    drill_fields: [content_name, event_name, app_name]
  }

  set: detail {
    fields: [
      event_name,
      object_title,
      object_type,
      content_name,
      platform,
      platform_detail
    ]
  }
}
