view: historical_chapters {
  sql_table_name: `pops-bi.Vietnam_Comic.POP_Scheduled_DIM_Historical_Chapters`
    ;;

  parameter: timeframe_selection {
    type: unquoted
    allowed_value: {
      label: "Daily"
      value: "date"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
  }

  dimension: timeframe {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% else %}
        ${month}
      {% endif %};;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.inserted_date ;;
  }

  dimension: month {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.inserted_date) ;;
  }

  dimension: published_date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.published_date ;;
  }

  dimension: published_month {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.published_date) ;;
  }

  dimension: chapter_index {
    type: number
    sql: ${TABLE}.chapter_index ;;
  }

  dimension: chapter_id {
    type: string
    sql: ${TABLE}.chapter_id ;;
  }

  dimension: chapter_name {
    type: string
    sql: ${TABLE}.chapter_name ;;
  }

  dimension: chapter_group {
    type: string
    sql: ${TABLE}.chapter_group ;;
  }

  dimension: chapter_type {
    type: string
    sql: ${TABLE}.chapter_type ;;
  }

  dimension: is_deleted {
    type: yesno
    sql: ${TABLE}.is_deleted ;;
  }

  dimension: project_production_type {
    type: string
    sql: ${TABLE}.project_production_type ;;
  }

  dimension: comic_id {
    type: string
    sql: ${TABLE}.title_id ;;
  }

  dimension: comic_name {
    type: string
    sql: ${TABLE}.title_name ;;
    drill_fields: [chapter_name]
  }

  measure: total_like {
    type: max
    sql: ${TABLE}.total_like ;;
  }

  measure: chapters {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.chapter_id) ;;
  }

  measure: paid_chapters {
    type: number
    sql: COUNT(DISTINCT IF(${TABLE}.chapter_type = 'paid', ${TABLE}.chapter_id, NULL)) ;;
  }

  measure: free_chapters {
    type: number
    sql: ${chapters} - ${paid_chapters} ;;
  }

  measure: comics {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.title_id) ;;
  }

  measure: paid_comics {
    type: number
    sql: COUNT(DISTINCT IF(${TABLE}.chapter_type = 'paid', ${TABLE}.title_id, NULL)) ;;
  }

  measure: free_comics {
    type: number
    sql: ${comics} - ${paid_comics} ;;
  }
}
