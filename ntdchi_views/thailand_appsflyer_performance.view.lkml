view: thailand_appsflyer_performances {
  derived_table: {
    sql:
      SELECT *
      FROM `pops-bi.Self_Service.POP_Scheduled_DATAMART_Appsflyer_Performance`
      WHERE app_region = 'TH'
    ;;
  }

  parameter: timeframe_selection {
    type: unquoted
    allowed_value: {
      label: "Daily"
      value: "date"
    }
    allowed_value: {
      label: "Weekly"
      value: "week"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
    allowed_value: {
      label: "Quarterly"
      value: "quarter"
    }
    allowed_value: {
      label: "Yearly"
      value: "year"
    }
  }

  dimension: timeframe {
    type: date
    datatype: date
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${week}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter}
      {% else %}
        ${year}
      {% endif %};;
  }

  dimension: timeframe_order {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${year_week}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month_format}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter_format}
      {% else %}
        ${year_format}
      {% endif %};;
  }

  dimension: timeframe_format {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date_format}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${week_format}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month_format}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter_format}
      {% else %}
        ${year_format}
      {% endif %};;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.date) ;;
  }

  dimension: week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.week ;;
  }

  dimension: year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.year_week ;;
  }

  dimension: week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_format ;;
  }

  dimension: week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_range ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, MONTH) ;;
  }

  dimension: month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.date) ;;
  }

  dimension: quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, QUARTER) ;;
  }

  dimension: quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.date) ;;
  }

  dimension: year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, YEAR) ;;
  }

  dimension: year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.date) ;;
  }

  dimension: register_date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.register_date ;;
  }

  dimension: register_date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.register_date) ;;
  }

  dimension: register_week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.register_week ;;
  }

  dimension: register_year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.register_year_week ;;
  }

  dimension: register_week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.register_week_format ;;
  }

  dimension: register_week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.register_week_range ;;
  }

  dimension: register_month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.register_date, MONTH) ;;
  }

  dimension: register_month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.register_date) ;;
  }

  dimension: register_quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.register_date, QUARTER) ;;
  }

  dimension: register_quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.register_date) ;;
  }

  dimension: register_year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.register_date, YEAR) ;;
  }

  dimension: register_year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.register_date) ;;
  }

  dimension: app_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    group_label: "Dimension"
    sql: IF(${TABLE}.app_region = 'TH', 'TH', 'VN') ;;
  }

  dimension: media_source {
    type: string
    group_label: "UTM"
    sql: ${TABLE}.media_source ;;
    drill_fields: [channel, campaign, ad_set]
  }

  dimension: channel {
    type: string
    group_label: "UTM"
    sql: ${TABLE}.channel ;;
    drill_fields: [campaign, ad_set]
  }

  dimension: campaign {
    type: string
    group_label: "UTM"
    sql: ${TABLE}.campaign ;;
    drill_fields: [ad_set]
  }

  dimension: ad_set {
    type: string
    group_label: "UTM"
    sql: ${TABLE}.ad_set ;;
  }

  dimension: platform {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.platform ;;
  }

  measure: installs {
    type: number
    group_label: "Installs"
    sql: COUNT(DISTINCT ${TABLE}.install_id) ;;
    value_format: "#,##0"
  }

  measure: installed_uninstalls {
    type: number
    group_label: "Installs"
    sql: COUNT(DISTINCT ${TABLE}.installed_uninstall_id) ;;
    value_format: "#,##0"
  }

  measure: uninstalls {
    type: number
    group_label: "Installs"
    sql: COUNT(DISTINCT ${TABLE}.uninstall_id) ;;
    value_format: "#,##0"
  }

  measure: users {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT ${TABLE}.user_id) ;;
    value_format: "#,##0"
  }

  measure: viewers {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT ${TABLE}.viewer_id) ;;
    value_format: "#,##0"
  }

  measure: view0s_viewers {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT ${TABLE}.view0s_viewer_id) ;;
    value_format: "#,##0"
  }

  measure: view5s_viewers {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT ${TABLE}.view5s_viewer_id) ;;
    value_format: "#,##0"
  }

  measure: views {
    type: sum
    group_label: "Views"
    sql: ${TABLE}.views ;;
    value_format: "#,##0"
  }

  measure: views_0s {
    type: sum
    group_label: "Views"
    sql: ${TABLE}.views_0s ;;
    value_format: "#,##0"
  }

  measure: views_5s {
    type: sum
    group_label: "Views"
    sql: ${TABLE}.views_5s ;;
    value_format: "#,##0"
  }

  measure: watch_time_minutes {
    type: sum
    group_label: "Views"
    sql: ${TABLE}.watch_time_minutes ;;
    value_format: "#,##0"
  }
}
