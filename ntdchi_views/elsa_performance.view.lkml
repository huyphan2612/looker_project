view: elsa_performance {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_Elsa_Performance`
    ;;

  parameter: timeframe_selection {
    type: unquoted
    allowed_value: {
      label: "Daily"
      value: "date"
    }
    allowed_value: {
      label: "Weekly"
      value: "week"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
    allowed_value: {
      label: "Quarterly"
      value: "quarter"
    }
    allowed_value: {
      label: "Yearly"
      value: "year"
    }
  }

  dimension: timeframe {
    type: date
    datatype: date
    group_label: "Time"
    sql:
    {% if timeframe_selection._parameter_value == 'date' %}
      ${date}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${week}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${month}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${quarter}
    {% else %}
      ${year}
    {% endif %};;
  }

  dimension: timeframe_order {
    type: string
    group_label: "Time"
    sql:
    {% if timeframe_selection._parameter_value == 'date' %}
      ${date}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${year_week}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${month_format}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${quarter_format}
    {% else %}
      ${year_format}
    {% endif %};;
  }

  dimension: timeframe_format {
    type: string
    group_label: "Time"
    sql:
    {% if timeframe_selection._parameter_value == 'date' %}
      ${date_format}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${week_format}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${month_format}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${quarter_format}
    {% else %}
      ${year_format}
    {% endif %};;
  }

  dimension: new_viewer_by_timeframe {
    type: string
    group_label: "Dimension"
    sql:
    {% if timeframe_selection._parameter_value == 'date' %}
      ${TABLE}.new_viewer_by_date
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${TABLE}.new_viewer_by_week
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${TABLE}.new_viewer_by_month
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${TABLE}.new_viewer_by_quarter
    {% else %}
      ${TABLE}.new_viewer_by_year
    {% endif %};;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.date) ;;
  }

  dimension: week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.week ;;
  }

  dimension: year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.year_week ;;
  }

  dimension: week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_format ;;
  }

  dimension: week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_range ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, MONTH) ;;
  }

  dimension: month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.date) ;;
    drill_fields: [date]
  }

  dimension: quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, QUARTER) ;;
  }

  dimension: quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.date) ;;
  }

  dimension: year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, YEAR) ;;
  }

  dimension: year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.date) ;;
  }

  dimension: register_date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.register_date ;;
  }

  dimension: app_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    group_label: "Dimension"
    sql: IF(${TABLE}.app_region = 'TH', 'TH', 'VN') ;;
  }

  dimension: platform {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.platform ;;
  }

  dimension: platform_detail {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.platform_detail ;;
  }

  dimension: video_title {
    type: string
    group_label: "Content"
    sql: ${TABLE}.video_title ;;
  }

  dimension: project_name {
    type: string
    group_label: "Content"
    sql: ${TABLE}.project_name_adj ;;
  }

  dimension: player_location {
    type: number
    group_label: "Elsa"
    sql: ${TABLE}.player_location ;;
  }

  dimension: elsa_text {
    type: string
    group_label: "Elsa"
    sql: ${TABLE}.elsa_text ;;
  }

  dimension: elsa_tries {
    type: number
    group_label: "Elsa"
    sql: ${TABLE}.elsa_tries ;;
    value_format: "#,##0"
    drill_fields: [email]
  }

  dimension: login_status {
    type: string
    group_label: "Dimension"
    sql: IFNULL(${TABLE}.login_status, 'Guest') ;;
  }

  dimension: user_type {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.user_type ;;
  }

  dimension: email {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.email ;;
  }

  measure: elsa_users {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.elsa_user_id) ;;
  }

  measure: elsa_viewers {
    type: number
    sql: COUNT(DISTINCT IF(${TABLE}.elsa_views > 0, ${TABLE}.elsa_user_id, NULL)) ;;
  }

  measure: elsa_views {
    type: sum
    sql: ${TABLE}.elsa_views ;;
  }

  measure: elsa_closers {
    type: number
    sql: COUNT(DISTINCT IF(${TABLE}.elsa_closes > 0, ${TABLE}.elsa_user_id, NULL)) ;;
  }

  measure: elsa_closes {
    type: sum
    sql: ${TABLE}.elsa_closes ;;
  }

  measure: elsa_audioer {
    type: number
    sql: COUNT(DISTINCT IF(${TABLE}.elsa_audios > 0, ${TABLE}.elsa_user_id, NULL)) ;;
  }

  measure: elsa_audios {
    type: sum
    sql: ${TABLE}.elsa_audios ;;
  }

  measure: elsa_recorders {
    type: number
    sql: COUNT(DISTINCT IF(${TABLE}.elsa_records > 0, ${TABLE}.elsa_user_id, NULL)) ;;
  }

  measure: elsa_records {
    type: sum
    sql: ${TABLE}.elsa_records ;;
  }

  measure: elsa_submiters {
    type: number
    sql: COUNT(DISTINCT IF(${TABLE}.elsa_submits > 0, ${TABLE}.elsa_user_id, NULL)) ;;
  }

  measure: elsa_submits {
    type: sum
    sql: ${TABLE}.elsa_submits ;;
  }

  measure: elsa_retriers {
    type: number
    sql: COUNT(DISTINCT IF(${TABLE}.elsa_try_agains > 0, ${TABLE}.elsa_user_id, NULL)) ;;
  }

  measure: elsa_retries {
    type: sum
    sql: ${TABLE}.elsa_try_agains ;;
  }

  measure: view_users {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.view_user_id) ;;
  }

  measure: viewers {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.viewer_id) ;;
  }

  measure: views {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.all_view5s_load_id) ;;
  }

  measure: watch_time_minutes {
    type: sum
    sql: ${TABLE}.watch_time_minutes ;;
    value_format: "#,##0"
  }
}
