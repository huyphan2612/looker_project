view: realtime_report {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_Realtime_Report`
    ;;

  dimension: hour {
    type: string
    group_label: "Dimension"
    sql: FORMAT_DATETIME("%H", ${TABLE}.datetime) ;;
  }

  dimension: period {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.last_n_day_period ;;
  }

  dimension: app_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_region ;;
  }

  dimension: event_name {
    type: string
    group_label: "Dimension"
    case: {
      when: {
        sql: ${TABLE}.event_name = "session_load" ;;
        label: "App Browsing"
      }
      when: {
        sql: ${TABLE}.event_name = "video_load" ;;
        label: "Video Watching"
      }
    }
  }

  dimension: video_title {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.video_title ;;
  }

  dimension: content_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.content_name ;;
    drill_fields: [video_title, platform]
  }

  dimension: content_topic {
    type: string
    group_label: "Dimension"
    sql: COALESCE(${TABLE}.content_topic, ${TABLE}.content_name) ;;
    drill_fields: [content_name, video_title, platform]
  }

  dimension: content_category {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.content_category ;;
    drill_fields: [content_name, video_title, platform]
  }

  dimension: new_user {
    type: string
    group_label: "Dimension"
    case: {
      when: {
        sql: ${TABLE}.new_user = "New" ;;
        label: "New"
      }
      when: {
        sql: ${TABLE}.new_user = "Returning" ;;
        label: "Returning"
      }
    }
  }

  dimension: active_days {
    type: number
    group_label: "Dimension"
    sql: ${TABLE}.active_days ;;
  }

  dimension: platform {
    type: string
    group_label: "Dimension"
    case: {
      when: {
        sql: ${TABLE}.platform = "MOBILE" ;;
        label: "MOBILE"
      }
      when: {
        sql: ${TABLE}.platform = "TV" ;;
        label: "TV"
      }
      when: {
        sql: ${TABLE}.platform = "WEB" ;;
        label: "WEB"
      }
    }
    drill_fields: [platform_detail]
  }

  dimension: web_platform {
    type: string
    group_label: "Dimension"
    case: {
      when: {
        sql: ${TABLE}.web_platform = "MOBILE" ;;
        label: "MOBILE"
      }
      when: {
        sql: ${TABLE}.web_platform = "TV" ;;
        label: "TV"
      }
      when: {
        sql: ${TABLE}.web_platform = "WEB - DESKTOP" ;;
        label: "WEB - DESKTOP"
      }
      when: {
        sql: ${TABLE}.web_platform = "WEB - MOBILE" ;;
        label: "WEB - MOBILE"
      }
    }
    drill_fields: [platform_detail]
  }

  dimension: platform_detail {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.platform_detail ;;
  }

  measure: sessions {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.session_id) ;;
    value_format: "#,##0"
  }

  measure: users {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.user_id) ;;
    value_format: "#,##0"
  }

  measure: new_users {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT (CASE WHEN ${TABLE}.new_user = 'New' THEN ${TABLE}.user_id END)) ;;
    value_format: "#,##0"
  }

  measure: viewers {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.viewer_id) ;;
    value_format: "#,##0"
  }

  measure: new_viewers {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT (CASE WHEN ${TABLE}.new_user = 'New' THEN ${TABLE}.viewer_id END)) ;;
    value_format: "#,##0"
  }

  measure: views {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.video_load_id) ;;
    value_format: "#,##0"
  }

  measure: new_views {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT (CASE WHEN ${TABLE}.new_user = 'New' THEN ${TABLE}.video_load_id END)) ;;
    value_format: "#,##0"
  }

  measure: views_per_session{
    type:  number
    group_label: "Measure"
    sql:  IF(${sessions} = 0, 0, ${views} / ${sessions})  ;;
    value_format: "#,##0.0"
  }

  measure: views_per_viewer{
    type:  number
    group_label: "Measure"
    sql:  IF(${viewers} = 0, 0, ${views} / ${viewers})  ;;
    value_format: "#,##0.0"
  }
}
