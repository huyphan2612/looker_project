view: view_page {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_View_Page`
    ;;

  parameter: timeframe_selection {
    type: unquoted
    allowed_value: {
      label: "Daily"
      value: "date"
    }
    allowed_value: {
      label: "Weekly"
      value: "week"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
  }

  dimension: timeframe {
    type: date
    datatype: date
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${week}
      {% else %}
        ${month}
      {% endif %};;
  }

  dimension: timeframe_order {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${year_week}
      {% else %}
        ${month_format}
      {% endif %};;
  }

  dimension: timeframe_format {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date_format}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${week_format}
      {% else %}
        ${month_format}
      {% endif %};;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.session_date ;;
  }

  dimension: date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.session_date) ;;
  }

  dimension: week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.session_week ;;
  }

  dimension: year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.session_year_week ;;
  }

  dimension: week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.session_week_format ;;
  }

  dimension: week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.session_week_range ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.session_date, MONTH) ;;
  }

  dimension: month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.session_date) ;;
  }

  dimension: app_name {
    type: string
    group_label: "PubSub"
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    group_label: "PubSub"
    sql: IF(${TABLE}.app_region = 'TH', 'TH', 'VN') ;;
  }

  dimension: platform {
    type: string
    group_label: "PubSub"
    sql: ${TABLE}.platform ;;
  }

  dimension: platform_detail {
    type: string
    group_label: "PubSub"
    sql: ${TABLE}.platform_detail ;;
  }

  dimension: firebase_session_id {
    type: string
    group_label: "Firebase"
    sql: ${TABLE}.firebase_session_id ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    group_label: "Firebase"
    sql: ${TABLE}.country ;;
  }

  dimension: region {
    type: string
    group_label: "Firebase"
    sql: ${TABLE}.region ;;
  }

  dimension: traffic_source_medium {
    type: string
    group_label: "Firebase"
    sql: ${TABLE}.traffic_source_medium ;;
  }

  dimension: traffic_source_name {
    type: string
    group_label: "Firebase"
    sql: ${TABLE}.traffic_source_name ;;
  }

  dimension: traffic_source_source {
    type: string
    group_label: "Firebase"
    sql: ${TABLE}.traffic_source_source ;;
  }

  dimension: page_location {
    type: string
    group_label: "Firebase"
    sql: ${TABLE}.page_location ;;
  }

  dimension: page_referrer {
    type: string
    group_label: "Firebase"
    sql: ${TABLE}.page_referrer ;;
  }

  dimension: screen_order {
    type: number
    group_label: "Firebase"
    sql:
      IF(${TABLE}.screen_identifier = 'splash', 1,
      IF(${TABLE}.screen_identifier = 'home adult', 1,;;
  }

  dimension: screen_identifier {
    type: string
    group_label: "Firebase"
    sql: ${TABLE}.screen_identifier ;;
  }

  dimension: event_order {
    type: number
    group_label: "Firebase"
    sql: ${TABLE}.event_order ;;
  }

  measure: pubsub_sessions {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.session_id) ;;
  }

  measure: firebase_sessions {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.firebase_session_id) ;;
  }

  measure: users {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.user_id) ;;
  }

  measure: media_players {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.load_user_id) ;;
  }

  measure: media_starters {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.view_user_id) ;;
  }

  measure: viewers {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.viewer_id) ;;
  }

  measure: count {
    type: count
  }
}
