view: pops_kids_favourite_category {
  derived_table: {
    sql:
      WITH
        kids_categories AS (
          SELECT date,
            content_id, content_title, content_title_length_minutes, content_title_group, content_title_delete,
            content_name_id, content_name, content_topic, kids_favourite_category_name, web_platform, platform_detail,
            viewer_id, all_view5s_load_id, watch_time_minutes
          FROM `pops-bi.Self_Service.POP_Scheduled_DATAMART_Both_App_Performance` AS both_app
          LEFT JOIN `pops-bi.Self_Service.POP_Scheduled_DIM_Videos_Kids_Favourite_Category` AS categories ON categories.video_id = both_app.content_id
          WHERE app_name = 'POPS Kids'
          )
      SELECT * FROM kids_categories
    ;;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: content_title {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_title ;;
  }

  dimension: content_title_length {
    type: number
    group_label: "Content"
    sql: ${TABLE}.content_title_length_minutes ;;
    value_format: "#,##0.0"
  }

  dimension: content_title_group {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_title_group ;;
  }

  dimension: content_title_delete {
    type: yesno
    group_label: "Content"
    sql: ${TABLE}.content_title_delete ;;
  }

  dimension: content_name {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_name ;;
    drill_fields: [content_title]
  }

  dimension: content_topic {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_topic ;;
    drill_fields: [content_name, content_title]
  }

  dimension: content_category {
    type: string
    group_label: "Content"
    sql: ${TABLE}.kids_favourite_category_name ;;
    drill_fields: [content_topic, content_name, content_title]
  }

  dimension: web_platform {
    type: string
    group_label: "Dimension"
    sql:${TABLE}.web_platform ;;
  }

  dimension: platform_detail {
    type: string
    group_label: "Dimension"
    sql:${TABLE}.platform_detail ;;
  }

  measure: contents {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.content_id) ;;
    value_format: "#,##0"
  }

  measure: average_content_length {
    label: "Avg Content Length"
    type: average
    sql: ${TABLE}.content_title_length_minutes ;;
    value_format: "#,##0.0"
  }

  measure: viewers {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.viewer_id) ;;
    value_format: "#,##0"
  }

  measure: views {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.all_view5s_load_id) ;;
    value_format: "#,##0"
  }

  measure: watch_time_minutes {
    type: sum
    sql: ${TABLE}.watch_time_minutes ;;
    value_format: "#,##0"
  }
}
