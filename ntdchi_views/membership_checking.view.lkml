view: membership_checking {
  derived_table: {
    sql:
      SELECT
          product_id, product_name, date, valid_from_date, valid_to_date, valid_from, valid_to,
          DATE_DIFF(valid_from_date, date, day) AS compare_date,
          DATE_DIFF(valid_from, valid_from_date, day) AS compare_from_date,
          DATE_DIFF(valid_to, valid_to_date, day) AS compare_to_date
      FROM (
          SELECT
              DATE(TIMESTAMP_ADD(created_date, INTERVAL 7 HOUR)) AS date,
              asset_id AS product_id, packages.product_name, valid_from_date, valid_to_date,
              DATE(TIMESTAMP_ADD(valid_from_timestamp, INTERVAL 7 HOUR)) AS valid_from,
              DATE(TIMESTAMP_ADD(valid_to_timestamp, INTERVAL 7 HOUR)) AS valid_to
          FROM `pops-204909.production.Star_Statistic_Activity_Logs` AS activity_log
          INNER JOIN `pops-204909.production.Star_Statistic_Unlocking_Bundle` AS unlock_bundle ON unlock_bundle.activity_id = activity_log.transactionId
          LEFT JOIN `pops-bi.Self_Service.POP_DIM_Packages` AS packages ON packages.product_id = activity_log.asset_id
          WHERE DATE(TIMESTAMP_ADD(created_date, INTERVAL 7 HOUR)) >= '2021-05-28' AND valid_from_timestamp IS NOT NULL AND valid_to_timestamp IS NOT NULL
      )
    ;;
  }

  dimension: product_id {
    type: string
    sql: ${TABLE}.product_id ;;
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.product_name ;;
  }

  dimension: date {
    type: date
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: valid_from_date {
    type: date
    datatype: date
    sql: ${TABLE}.valid_from_date ;;
  }

  dimension: valid_to_date {
    type: date
    datatype: date
    sql: ${TABLE}.valid_to_date ;;
  }

  dimension: valid_from {
    type: date
    datatype: date
    sql: ${TABLE}.valid_from ;;
  }

  dimension: valid_to {
    type: date
    datatype: date
    sql: ${TABLE}.valid_to ;;
  }

  measure: compare_date {
    type: sum
    sql: ${TABLE}.compare_date ;;
  }

  measure: compare_from_date {
    type: sum
    sql: ${TABLE}.compare_from_date ;;
  }

  measure: compare_to_date {
    type: sum
    sql: ${TABLE}.compare_to_date ;;
  }
}
