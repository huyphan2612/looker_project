view: user_journey {
  derived_table: {
    sql:
    WITH
      user_journey AS (
        SELECT
            app_name, app_region, datetime, session_date AS date, session_id, platform,
            IF(click_user_id IS NOT NULL, CONCAT(entity_detail, 'Click'),
              IF(viewer_id IS NOT NULL AND entity_type = 'Video', 'VideoView',
                IF(viewer_id IS NOT NULL AND entity_type = 'ComicsChapter', 'ComicsChapterView',
                  IF(viewer_id IS NOT NULL AND entity_type = 'Livestream', 'LivestreamView', entity_detail)))) AS entity,
            IF(click_user_id IS NOT NULL, click_user_id,
              IF(viewer_id IS NOT NULL, viewer_id, page_view_user_id)) AS user_id
        FROM `pops-bi.Self_Service.POP_Scheduled_DATAMART_User_View_Interact`
        WHERE session_date IS NOT NULL AND entity_type_adj IS NOT NULL
      ),

      user_journey_grpby AS (
        SELECT app_name, app_region, date, session_id, platform,
            STRING_AGG(entity, 'DIEUCHI') AS entity,
            COUNT(user_id) AS users,
        FROM (
            SELECT app_name, app_region, datetime, date, session_id, platform,
                CONCAT(ROW_NUMBER() OVER (PARTITION BY session_id ORDER BY datetime), '. ', entity) AS entity,
                user_id
            FROM user_journey
        ) GROUP BY app_name, app_region, date, session_id, platform
      ),

      final_user_journey AS (
        SELECT app_name, app_region, date, session_id, platform,
            REGEXP_REPLACE(entity, 'DIEUCHI', ', ') AS entity,
            SPLIT(entity, 'DIEUCHI')[OFFSET(0)] AS entity_1,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 0, SPLIT(entity, 'DIEUCHI')[OFFSET(1)], NULL) AS entity_2,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 1, SPLIT(entity, 'DIEUCHI')[OFFSET(2)], NULL) AS entity_3,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 2, SPLIT(entity, 'DIEUCHI')[OFFSET(3)], NULL) AS entity_4,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 3, SPLIT(entity, 'DIEUCHI')[OFFSET(4)], NULL) AS entity_5,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 4, SPLIT(entity, 'DIEUCHI')[OFFSET(5)], NULL) AS entity_6,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 5, SPLIT(entity, 'DIEUCHI')[OFFSET(6)], NULL) AS entity_7,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 6, SPLIT(entity, 'DIEUCHI')[OFFSET(7)], NULL) AS entity_8,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 7, SPLIT(entity, 'DIEUCHI')[OFFSET(8)], NULL) AS entity_9,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 8, SPLIT(entity, 'DIEUCHI')[OFFSET(9)], NULL) AS entity_10,

            SPLIT(entity, 'DIEUCHI')[OFFSET(CAST(ROUND((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 - 0, 0) AS INT64))] AS entity_last_1,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 0,
              SPLIT(entity, 'DIEUCHI')[OFFSET(CAST(ROUND((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 - 1, 0) AS INT64))], NULL) AS entity_last_2,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 1,
              SPLIT(entity, 'DIEUCHI')[OFFSET(CAST(ROUND((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 - 2, 0) AS INT64))], NULL) AS entity_last_3,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 2,
              SPLIT(entity, 'DIEUCHI')[OFFSET(CAST(ROUND((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 - 3, 0) AS INT64))], NULL) AS entity_last_4,
            IF((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 > 3,
              SPLIT(entity, 'DIEUCHI')[OFFSET(CAST(ROUND((LENGTH(entity) - LENGTH(REGEXP_REPLACE(entity, 'DIEUCHI', ''))) / 7 - 4, 0) AS INT64))], NULL) AS entity_last_5,
            users
        FROM user_journey_grpby
      )
    SELECT * FROM final_user_journey
    ;;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Dimension"
    sql: ${TABLE}.date ;;
  }

  dimension: app_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    group_label: "Dimension"
    sql: IF(${TABLE}.app_region = 'TH', 'TH', 'VN') ;;
  }

  dimension: platform {
    type: string
    group_label: "Dimension"
    sql:${TABLE}.platform ;;
  }

  dimension: entity {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity ;;
  }

  dimension: entity_1 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_1 ;;
  }

  dimension: entity_2 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_2 ;;
  }

  dimension: entity_3 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_3 ;;
  }

  dimension: entity_4 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_4 ;;
  }

  dimension: entity_5 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_5 ;;
  }

  dimension: entity_6 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_6 ;;
  }

  dimension: entity_7 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_7 ;;
  }

  dimension: entity_8 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_8 ;;
  }

  dimension: entity_9 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_9 ;;
  }

  dimension: entity_10 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_10 ;;
  }

  dimension: entity_last_1 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_last_1 ;;
  }

  dimension: entity_last_2 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_last_2 ;;
  }

  dimension: entity_last_3 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_last_3 ;;
  }

  dimension: entity_last_4 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_last_4 ;;
  }

  dimension: entity_last_5 {
    type: string
    group_label: "Entity"
    sql:${TABLE}.entity_last_5 ;;
  }

  measure: users {
    type: sum
    sql:${TABLE}.users ;;
  }
}
