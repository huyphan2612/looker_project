view: thailand_d2c_performance {
  sql_table_name: `pops-bi.Thailand.POP_Scheduled_DATAMART_D2C_Performance`
    ;;

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.date) ;;
  }

  dimension: week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.week ;;
  }

  dimension: year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.year_week ;;
  }

  dimension: week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_format ;;
  }

  dimension: week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_range ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, MONTH) ;;
  }

  dimension: month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.date) ;;
  }

  dimension: quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, QUARTER) ;;
  }

  dimension: quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.date) ;;
  }

  dimension: year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, YEAR) ;;
  }

  dimension: year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.date) ;;
  }

    dimension: register_date {
      type: date
      datatype: date
      group_label: "Time"
      sql: ${TABLE}.register_date ;;
    }

    dimension: data_status {
      type: string
      group_label: "Dimension"
      sql: ${TABLE}.data_status ;;
    }

    dimension: revenue_stream {
      type: string
      group_label: "Dimension"
      sql: ${TABLE}.revenue_stream ;;
    }

    dimension: country {
      type: string
      group_label: "Dimension"
      sql: ${TABLE}.country ;;
    }

    dimension: package {
      type: string
      group_label: "Dimension"
      sql: ${TABLE}.package ;;
      drill_fields: [partner, product_group]
    }

    dimension: product_group_id {
      type: string
      group_label: "Dimension"
      sql: ${TABLE}.product_group_id ;;
    }

    dimension: product_group {
      type: string
      group_label: "Dimension"
      sql: ${TABLE}.product_group_name ;;
      drill_fields: [product_name]
    }

    dimension: product_id {
      type: string
      group_label: "Dimension"
      sql: ${TABLE}.product_id ;;
    }


    dimension: product_name {
      type: string
      group_label: "Dimension"
      sql: ${TABLE}.product_name ;;
    }

    dimension: partner {
      type: string
      group_label: "Dimension"
      sql: ${TABLE}.partner ;;
    }

    dimension: user_type {
      type: string
      group_label: "Dimension"
      sql: ${TABLE}.user_type ;;
    }

    measure: users {
      type: number
      group_label: "Measure"
      sql: COUNT(DISTINCT ${TABLE}.user_id) ;;
      value_format: "#,##0"
    }

    dimension: star_price_usd {
      type: number
      group_label: "Dimension"
      sql: ${TABLE}.star_price_usd ;;
      value_format: "$#,##0"
    }

    measure: lastest_month {
      type: date
      datatype: date
      group_label: "Measure"
      sql: MAX(${TABLE}.month) ;;
    }

    measure: quantity {
      type: sum
      group_label: "Measure"
      sql: ${TABLE}.quantity ;;
      value_format: "#,##0"
    }

    measure: absolute_quantity {
      type: sum
      label: "Absolute Quantity"
      group_label: "Measure"
      sql: ABS(${TABLE}.quantity) ;;
      value_format: "#,##0"
    }

    measure: stars {
      type: sum
      group_label: "Measure"
      sql: ${TABLE}.stars ;;
      value_format: "#,##0"
    }

    measure: absolute_stars {
      type: sum
      group_label: "Measure"
      sql: ABS(${TABLE}.stars) ;;
      value_format: "#,##0"
    }

    measure: fee_usd {
      type: sum
      group_label: "Measure"
      sql: ${TABLE}.fee_usd ;;
      value_format: "$#,##0.00"
    }

    measure: revenue_usd {
      type: sum
      label: "Revenue (in USD)"
      group_label: "Measure"
      sql: ${TABLE}.revenue_usd ;;
      value_format: "$#,##0.00"
    }

    measure: absolute_revenue_usd {
      type: sum
      label: "Absolute Revenue (in USD)"
      group_label: "Measure"
      sql: ABS(${TABLE}.revenue_usd) ;;
      value_format: "$#,##0.00"
    }
  }
