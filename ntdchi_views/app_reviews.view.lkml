view: app_reviews {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_App_Reviews`
    ;;

  parameter: timeframe_selection {
    type: unquoted
    allowed_value: {
      label: "Daily"
      value: "date"
    }
    allowed_value: {
      label: "Weekly"
      value: "week"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
    allowed_value: {
      label: "Quarterly"
      value: "quarter"
    }
    allowed_value: {
      label: "Yearly"
      value: "year"
    }
  }

  dimension: timeframe {
    type: date
    datatype: date
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${week}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter}
      {% else %}
        ${year}
      {% endif %};;
  }

  dimension: timeframe_order {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${year_week}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month_format}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter_format}
      {% else %}
        ${year_format}
      {% endif %};;
  }

  dimension: timeframe_format {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date_format}
      {% elsif timeframe_selection._parameter_value == 'week' %}
        ${week_format}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month_format}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter_format}
      {% else %}
        ${year_format}
      {% endif %};;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.date) ;;
  }

  dimension: week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.week ;;
  }

  dimension: year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.year_week ;;
  }

  dimension: week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_format ;;
  }

  dimension: week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_range ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, MONTH) ;;
  }

  dimension: month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.date) ;;
    drill_fields: [date]
  }

  dimension: quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, QUARTER) ;;
  }

  dimension: quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.date) ;;
  }

  dimension: year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, YEAR) ;;
  }

  dimension: year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.date) ;;
  }

  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: app_name {
    type: string
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    sql: ${TABLE}.app_region ;;
  }

  dimension: app_version {
    type: string
    sql: ${TABLE}.app_version ;;
    drill_fields: [app_version_detail]
  }

  dimension: app_version_detail {
    type: string
    sql: ${TABLE}.app_version_detail ;;
  }

  dimension: author {
    type: string
    sql: ${TABLE}.author ;;
  }

  dimension: review_title {
    type: string
    sql: ${TABLE}.review_title ;;
  }

  dimension: review {
    type: string
    sql: ${TABLE}.review ;;
  }

  dimension: rating {
    type: number
    sql: ${TABLE}.rating ;;
  }

  measure: reviews {
    type: sum
    sql: ${TABLE}.reviews ;;
  }
}
