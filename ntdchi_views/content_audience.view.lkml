view: content_audience {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_Content_Audience`
    ;;

  dimension: date {

    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: register_date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.register_date ;;
  }

  dimension: app_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    group_label: "Dimension"
    sql: IF(${TABLE}.app_region = 'TH', 'TH', 'VN') ;;
  }

  dimension: web_platform {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.web_platform ;;
  }

  dimension: viewer_type {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.viewer_type ;;
  }

  dimension: event_name_1 {
    type: string
    group_label: "Content 1"
    case: {
      when: {
        sql: ${TABLE}.event_name_1 IN ("video_load", "livestream_load") ;;
        label: "Video"
      }
      when: {
        sql: ${TABLE}.event_name_1 = "chapter_load" ;;
        label: "Comic"
      }
    }
  }

  dimension: content_name_1 {
    type: string
    group_label: "Content 1"
    sql:
      IF(${TABLE}.content_name_1 IS NOT NULL,
        IF(${TABLE}.event_name_1 = "video_load", CONCAT('Project ', ${TABLE}.content_name_1),
        IF(${TABLE}.event_name_1  = "chapter_load", CONCAT('Comic ', ${TABLE}.content_name_1),
        IF(${TABLE}.event_name_1 = "livestream_load", CONCAT('Livestream ', ${TABLE}.content_name_1), NULL))), ${TABLE}.content_name_1)
    ;;
  }

  dimension: content_topic_1 {
    type: string
    group_label: "Content 1"
    sql:
      IF(${TABLE}.content_topic_1 IS NOT NULL,
        IF(${TABLE}.event_name_1 = "video_load", CONCAT('Project ', ${TABLE}.content_topic_1),
        IF(${TABLE}.event_name_1  = "chapter_load", CONCAT('Comic ', ${TABLE}.content_topic_1),
        IF(${TABLE}.event_name_1 = "livestream_load", CONCAT('Livestream ', ${TABLE}.content_topic_1), NULL))), ${TABLE}.content_topic_1)
    ;;
  }

  dimension: content_category_1 {
    type: string
    group_label: "Content 1"
    sql:
      IF(${TABLE}.content_category_1 IS NOT NULL,
        IF(${TABLE}.event_name_1 = "video_load", CONCAT('Project ', ${TABLE}.content_category_1),
        IF(${TABLE}.event_name_1  = "chapter_load", CONCAT('Comic ', ${TABLE}.content_category_1),
        IF(${TABLE}.event_name_1 = "livestream_load", CONCAT('Livestream ', ${TABLE}.content_category_1), NULL))), ${TABLE}.content_category_1)
    ;;
  }

  dimension: event_name_2 {
    type: string
    group_label: "Content 2"
    case: {
      when: {
        sql: ${TABLE}.event_name_2 IN ("video_load", "livestream_load") ;;
        label: "Video"
      }
      when: {
        sql: ${TABLE}.event_name_2 = "chapter_load" ;;
        label: "Comic"
      }
    }
  }

  dimension: content_name_2 {
    type: string
    group_label: "Content 2"
    sql:
      IF(${TABLE}.content_name_2 IS NOT NULL,
        IF(${TABLE}.event_name_2 = "video_load", CONCAT('Project ', ${TABLE}.content_name_2),
        IF(${TABLE}.event_name_2  = "chapter_load", CONCAT('Comic ', ${TABLE}.content_name_2),
        IF(${TABLE}.event_name_2 = "livestream_load", CONCAT('Livestream ', ${TABLE}.content_name_2), NULL))), ${TABLE}.content_name_2)
    ;;
  }

  dimension: content_topic_2 {
    type: string
    group_label: "Content 2"
    sql:
      IF(${TABLE}.content_topic_2 IS NOT NULL,
        IF(${TABLE}.event_name_2 = "video_load", CONCAT('Project ', ${TABLE}.content_topic_2),
        IF(${TABLE}.event_name_2  = "chapter_load", CONCAT('Comic ', ${TABLE}.content_topic_2),
        IF(${TABLE}.event_name_2 = "livestream_load", CONCAT('Livestream ', ${TABLE}.content_topic_2), NULL))), ${TABLE}.content_topic_2)
    ;;
    drill_fields: [content_name_2]
  }

  dimension: content_category_2 {
    type: string
    group_label: "Content 2"
    sql:
      IF(${TABLE}.content_category_2 IS NOT NULL,
        IF(${TABLE}.event_name_2 = "video_load", CONCAT('Project ', ${TABLE}.content_category_2),
        IF(${TABLE}.event_name_2  = "chapter_load", CONCAT('Comic ', ${TABLE}.content_category_2),
        IF(${TABLE}.event_name_2 = "livestream_load", CONCAT('Livestream ', ${TABLE}.content_category_2), NULL))), ${TABLE}.content_category_2)
    ;;
    drill_fields: [content_topic_2, content_name_2]
  }

  measure: categories {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.content_category_1) ;;
  }

  measure: viewers {
    type: number
    sql: COUNT(DISTINCT ${TABLE}.viewer_id) ;;
  }
}
