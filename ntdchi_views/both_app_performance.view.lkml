view: both_app_performance {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_Both_App_Performance_All`
    ;;

  parameter: timeframe_selection {
    type: unquoted
    allowed_value: {
      label: "Hourly"
      value: "hour"
    }
    allowed_value: {
      label: "Daily"
      value: "date"
    }
    allowed_value: {
      label: "Weekly"
      value: "week"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
    allowed_value: {
      label: "Quarterly"
      value: "quarter"
    }
    allowed_value: {
      label: "Yearly"
      value: "year"
    }
  }

  parameter: dimension_selection {
    type: unquoted
    allowed_value: {
      label: "Project Category"
      value: "content_category"
    }
    allowed_value: {
      label: "Project Age"
      value: "age_category"
    }
    allowed_value: {
      label: "Project Vertical"
      value: "content_vertical"
    }
    allowed_value: {
      label: "Project Segment"
      value: "content_segment"
    }
    allowed_value: {
      label: "Publish Hour"
      value: "publish_hour"
    }
  }

  parameter: measure_selection {
    type: unquoted
    allowed_value: {
      label: "Viewers"
      value: "viewers"
    }
    allowed_value: {
      label: "Views"
      value: "views"
    }
    allowed_value: {
      label: "Watch Time"
      value: "watch_time"
    }
  }

  parameter: ratio_selection {
    type: unquoted
    allowed_value: {
      label: "Views/ Viewer"
      value: "views_per_viewer"
    }
    allowed_value: {
      label: "Watch Time/ Viewer"
      value: "watch_time_per_viewer"
    }
    allowed_value: {
      label: "Watch Time/ View"
      value: "watch_time_per_view"
    }
    allowed_value: {
      label: "Watch Time/ Session"
      value: "watch_time_per_session"
    }
  }

  dimension: timeframe {
    type: date
    datatype: date
    group_label: "Time"
    sql:
    {% if timeframe_selection._parameter_value == 'hour' %}
      ${date_hour}
    {% elsif timeframe_selection._parameter_value == 'date' %}
      ${date}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${week}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${month}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${quarter}
    {% else %}
      ${year}
    {% endif %};;
  }

  dimension: timeframe_order {
    type: string
    group_label: "Time"
    sql:
    {% if timeframe_selection._parameter_value == 'hour' %}
      ${date_hour_format}
    {% elsif timeframe_selection._parameter_value == 'date' %}
      ${date}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${year_week}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${month_format}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${quarter_format}
    {% else %}
      ${year_format}
    {% endif %};;
  }

  dimension: timeframe_format {
    type: string
    group_label: "Time"
    sql:
    {% if timeframe_selection._parameter_value == 'hour' %}
      ${date_hour_format}
    {% elsif timeframe_selection._parameter_value == 'date' %}
      ${date_format}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${week_format}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${month_format}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${quarter_format}
    {% else %}
      ${year_format}
    {% endif %};;
  }

  dimension: register_timeframe {
    type: string
    group_label: "Time"
    sql:
    {% if timeframe_selection._parameter_value == 'datetime' %}
      ${register_date}
    {% elsif timeframe_selection._parameter_value == 'date' %}
      ${register_date}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${register_week_range}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${register_month_format}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${register_quarter_format}
    {% else %}
      ${register_year_format}
    {% endif %};;
  }

  dimension: active_timeframe {
    type: number
    group_label: "Viewer"
    sql:
    {% if timeframe_selection._parameter_value == 'datetime' %}
      ${active_days}
    {% elsif timeframe_selection._parameter_value == 'date' %}
      ${active_days}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${active_weeks}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${active_months}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${active_quarters}
    {% else %}
      ${active_years}
    {% endif %};;
  }

  dimension: new_viewer_by_timeframe {
    type: string
    group_label: "Viewer"
    sql:
    {% if timeframe_selection._parameter_value == 'datetime' %}
      ${new_viewer_by_date}
    {% elsif timeframe_selection._parameter_value == 'date' %}
      ${new_viewer_by_date}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${new_viewer_by_week}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${new_viewer_by_month}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${new_viewer_by_quarter}
    {% else %}
      ${new_viewer_by_year}
    {% endif %};;
  }

  dimension: publish_timeframe_order {
    type: date
    datatype: date
    group_label: "Time"
    sql:
    {% if timeframe_selection._parameter_value == 'datetime' %}
      ${content_title_publish_date}
    {% elsif timeframe_selection._parameter_value == 'date' %}
      ${content_title_publish_date}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${content_title_publish_year_week}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${content_title_publish_month_format}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${content_title_publish_quarter_format}
    {% else %}
      ${content_title_publish_year_format}
    {% endif %};;
  }

  dimension: publish_timeframe_format {
    type: string
    group_label: "Time"
    sql:
    {% if timeframe_selection._parameter_value == 'datetime' %}
      ${content_title_publish_date_format}
    {% elsif timeframe_selection._parameter_value == 'date' %}
      ${content_title_publish_date_format}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${content_title_publish_week_format}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${content_title_publish_month_format}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${content_title_publish_quarter_format}
    {% else %}
      ${content_title_publish_year_format}
    {% endif %};;
  }

  dimension: date_hour {
    type: date_hour
    datatype: datetime
    group_label: "Time"
    sql: DATETIME_SUB(${TABLE}.datetime, INTERVAL 7 HOUR) ;;
  }

  dimension: date_hour_format {
    type: number
    group_label: "Time"
    sql:
      IF(LENGTH(CAST(EXTRACT(HOUR FROM ${TABLE}.datetime) AS STRING)) = 1,
      CONCAT(0,EXTRACT(HOUR FROM ${TABLE}.datetime)),
      CAST(EXTRACT(HOUR FROM ${TABLE}.datetime) AS STRING)) ;;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.date) ;;
  }

  dimension: week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.week ;;
  }

  dimension: year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.year_week ;;
  }

  dimension: week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_format ;;
  }

  dimension: week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_range ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, MONTH) ;;
  }

  dimension: month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.date) ;;
    drill_fields: [date]
  }

  dimension: quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, QUARTER) ;;
  }

  dimension: quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.date) ;;
  }

  dimension: year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, YEAR) ;;
  }

  dimension: year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.date) ;;
  }

  dimension: register_date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.register_date ;;
  }

  dimension: register_date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.register_date) ;;
  }

  dimension: register_week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.register_week ;;
  }

  dimension: register_year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.register_year_week ;;
  }

  dimension: register_week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.register_week_format ;;
  }

  dimension: register_week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.register_week_range ;;
  }

  dimension: register_month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.register_date, MONTH) ;;
  }

  dimension: register_month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.register_date) ;;
  }

  dimension: register_quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.register_date, QUARTER) ;;
  }

  dimension: register_quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.register_date) ;;
  }

  dimension: register_year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.register_date, YEAR) ;;
  }

  dimension: register_year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.register_date) ;;
  }

  dimension: content_title_publish_hour {
    type: number
    group_label: "Time"
    sql:
      IF(${TABLE}.content_name = 'Nào Mình Cùng Xem',
        IF(EXTRACT(HOUR FROM ${TABLE}.content_title_publish_datetime) = 10, CONCAT(15, "h"),
        IF(EXTRACT(HOUR FROM ${TABLE}.content_title_publish_datetime) = 18, CONCAT(11, "h"),
        CONCAT(EXTRACT(HOUR FROM ${TABLE}.content_title_publish_datetime), "h"))),
      IF(LENGTH(CAST(EXTRACT(HOUR FROM ${TABLE}.content_title_publish_datetime) AS STRING)) = 1,
        CONCAT(0, EXTRACT(HOUR FROM ${TABLE}.content_title_publish_datetime), "h"),
        CONCAT(EXTRACT(HOUR FROM ${TABLE}.content_title_publish_datetime), "h"))) ;;
    drill_fields: [content_title]
  }

  dimension: content_title_publish_date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.content_title_publish_date ;;
  }

  dimension: content_title_publish_date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.content_title_publish_date) ;;
  }

  dimension: content_title_publish_week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.content_title_publish_week ;;
  }

  dimension: content_title_publish_year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.content_title_publish_year_week ;;
  }

  dimension: content_title_publish_week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.content_title_publish_week_format ;;
  }

  dimension: content_title_publish_week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.content_title_publish_week_range ;;
  }

  dimension: content_title_publish_month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.content_title_publish_date, MONTH) ;;
  }

  dimension: content_title_publish_month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.content_title_publish_date) ;;
  }

  dimension: content_title_publish_quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.content_title_publish_date, QUARTER) ;;
  }

  dimension: content_title_publish_quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.content_title_publish_date) ;;
  }

  dimension: content_title_publish_year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.content_title_publish_date, YEAR) ;;
  }

  dimension: content_title_publish_year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.content_title_publish_date) ;;
  }

  dimension: install_date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.install_date ;;
  }

  dimension: retargeting_date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.retargeting_date ;;
  }

  dimension: data_type {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.data_type ;;
  }

  dimension: app_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    group_label: "Dimension"
    sql: IF(${TABLE}.app_region = 'TH', 'TH', 'VN') ;;
  }

  dimension: event_name {
    type: string
    group_label: "Dimension"
    case: {
      when: {
        sql: ${TABLE}.event_name IN ("video_load", "livestream_load") ;;
        label: "Video"
      }
      when: {
        sql: ${TABLE}.event_name = "chapter_load" ;;
        label: "Comic"
      }
    }
  }

  dimension: content_dimension {
    type: string
    group_label: "Content"
    sql:
    {% if dimension_selection._parameter_value == 'content_category' %}
      ${content_category}
    {% elsif dimension_selection._parameter_value == 'age_category' %}
      ${age_category}
    {% elsif dimension_selection._parameter_value == 'content_vertical' %}
      ${content_vertical}
    {% elsif dimension_selection._parameter_value == 'content_segment' %}
      ${content_segment}
    {% else %}
      ${content_title_publish_hour}
    {% endif %};;
  }

  dimension: load_order {
    type: number
    group_label: "Content"
    sql: ${TABLE}.load_order ;;
  }

  dimension: content_title {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_title ;;
  }

  dimension: content_title_length {
    type: number
    group_label: "Content"
    sql: ${TABLE}.content_title_length_minutes ;;
    value_format: "#,##0.0"
  }

  dimension: content_title_order {
    type: number
    group_label: "Content"
    sql: ${TABLE}.content_title_order ;;
  }

  dimension: content_title_group {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_title_group ;;
  }

  dimension: content_title_type {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_title_type ;;
  }

  dimension: content_title_delete {
    type: yesno
    group_label: "Content"
    sql: ${TABLE}.content_title_delete ;;
  }

  dimension: new_content_by_date {
    type: string
    group_label: "Content"
    sql: ${TABLE}.new_content_by_date ;;
  }

  dimension: new_content_by_week {
    type: string
    group_label: "Content"
    sql: ${TABLE}.new_content_by_week ;;
  }

  dimension: new_content_by_month {
    type: string
    group_label: "Content"
    sql: ${TABLE}.new_content_by_month ;;
  }

  dimension: new_content_by_quarter {
    type: string
    group_label: "Content"
    sql: ${TABLE}.new_content_by_quarter ;;
  }

  dimension: new_content_by_year {
    type: string
    group_label: "Content"
    sql: ${TABLE}.new_content_by_year ;;
  }

  dimension: content_name {
    type: string
    group_label: "Content"
    sql:
    IF(${TABLE}.content_name IS NOT NULL,
    IF(${TABLE}.event_name = "video_load", CONCAT('Project ', ${TABLE}.content_name),
    IF(${TABLE}.event_name  = "chapter_load", CONCAT('Comic ', ${TABLE}.content_name),
    IF(${TABLE}.event_name = "livestream_load", CONCAT('Livestream ', ${TABLE}.content_name), NULL))), ${TABLE}.content_name)
  ;;
    drill_fields: [content_category, content_title]
  }

  dimension: content_topic {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_topic ;;
    drill_fields: [content_name, content_title]
  }

  dimension: content_category {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_category ;;
    drill_fields: [content_topic, content_name, content_title]
  }

  dimension: content_production_type {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_production_type ;;
  }

  dimension: content_original_country {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_original_country ;;
  }

  dimension: age_category {
    type: string
    group_label: "Content"
    sql: ${TABLE}.age_category ;;
    drill_fields: [content_topic, content_name, content_title]
  }

  dimension: content_vertical {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_vertical ;;
    drill_fields: [content_topic, content_name, content_title]
  }

  dimension: content_segment {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_segment ;;
    drill_fields: [content_topic, content_name, content_title]
  }

  dimension: content_provider {
    type: string
    group_label: "Content"
    sql: ${TABLE}.content_provider ;;
    drill_fields: [content_topic, content_name, content_title, content_category]
  }

  dimension: installation_channel {
    type: string
    group_label: "UTM"
    sql: ${TABLE}.installation_channel ;;
  }

  dimension: utm_medium {
    label: "UTM Medium"
    type: string
    group_label: "UTM"
    sql: ${TABLE}.utm_medium ;;
    drill_fields: [utm_source, utm_campaign]
  }

  dimension: utm_source_group {
    label: "UTM Source Group"
    type: string
    group_label: "UTM"
    sql: ${TABLE}.utm_source_group ;;
    drill_fields: [utm_source, utm_campaign]
  }

  dimension: utm_source {
    label: "UTM Source"
    type: string
    group_label: "UTM"
    sql: ${TABLE}.utm_source ;;
    drill_fields: [utm_campaign]
  }

  dimension: utm_campaign {
    label: "UTM Campaign"
    type: string
    group_label: "UTM"
    sql: ${TABLE}.utm_campaign ;;
  }

  dimension: traffic_source_source {
    type: string
    group_label: "UTM"
    sql: ${TABLE}.traffic_source_source ;;
    drill_fields: [traffic_source_medium, traffic_source_name]
  }

  dimension: traffic_source_medium {
    type: string
    group_label: "UTM"
    sql: ${TABLE}.traffic_source_medium ;;
    drill_fields: [traffic_source_name]
  }

  dimension: traffic_source_name {
    type: string
    group_label: "UTM"
    sql: ${TABLE}.traffic_source_name ;;
  }

  dimension: retargeting_source {
    type: string
    group_label: "UTM"
    sql: ${TABLE}.retargeting_source ;;
    drill_fields: [retargeting_medium, retargeting_campaign]
  }

  dimension: retargeting_medium {
    type: string
    group_label: "UTM"
    sql: ${TABLE}.retargeting_medium ;;
    drill_fields: [retargeting_campaign]
  }

  dimension: retargeting_campaign {
    type: string
    group_label: "UTM"
    sql: ${TABLE}.retargeting_campaign ;;
  }

  dimension: email {
    type: string
    group_label: "Viewer"
    sql: ${TABLE}.email ;;
  }

  dimension: verified_email {
    type: yesno
    group_label: "Viewer"
    sql: ${TABLE}.verified_email ;;
  }

  dimension: user_id {
    type: string
    group_label: "Viewer"
    sql: ${TABLE}.user_id ;;
  }

  dimension: user_type {
    type: string
    group_label: "Viewer"
    sql: ${TABLE}.user_type ;;
  }

  dimension: login_status {
    type: string
    group_label: "Viewer"
    sql: ${TABLE}.login_status ;;
  }

  dimension: paid_remove_ads_user {
    type: yesno
    group_label: "Viewer"
    sql: ${TABLE}.paid_remove_ads_user ;;
  }

  dimension: paid_unlock_chapter_user {
    type: yesno
    group_label: "Viewer"
    sql: ${TABLE}.paid_unlock_chapter_user ;;
  }

  dimension: paid_unlock_lac_troi_user {
    type: yesno
    group_label: "Viewer"
    sql: ${TABLE}.paid_unlock_lactroi_user ;;
  }

  dimension: paid_unlock_sbtc_user {
    label: "Paid Unlock SBTC User"
    type: yesno
    group_label: "Viewer"
    sql: ${TABLE}.paid_unlock_sbtc_user ;;
  }

  dimension: paid_unlock_boy_love_user {
    type: yesno
    group_label: "Viewer"
    sql: ${TABLE}.paid_unlock_boylove_user ;;
  }

  dimension: membership_user {
    type: string
    group_label: "Viewer"
    sql: ${TABLE}.membership_user ;;
  }

  dimension: new_viewer_by_date {
    type: string
    group_label: "Viewer"
    sql: ${TABLE}.new_viewer_by_date ;;
  }

  dimension: new_viewer_by_week {
    type: string
    group_label: "Viewer"
    sql: ${TABLE}.new_viewer_by_week ;;
  }

  dimension: new_viewer_by_month {
    type: string
    group_label: "Viewer"
    sql: ${TABLE}.new_viewer_by_month ;;
  }

  dimension: new_viewer_by_quarter {
    type: string
    group_label: "Viewer"
    sql: ${TABLE}.new_viewer_by_quarter ;;
  }

  dimension: new_viewer_by_year {
    type: string
    group_label: "Viewer"
    sql: ${TABLE}.new_viewer_by_year ;;
  }

  dimension: active_days {
    type: number
    group_label: "Viewer"
    sql: ${TABLE}.active_days ;;
  }

  dimension: active_weeks {
    type: number
    group_label: "Viewer"
    sql: ${TABLE}.active_weeks ;;
  }

  dimension: active_months {
    type: number
    group_label: "Viewer"
    sql: ${TABLE}.active_months ;;
  }

  dimension: active_quarters {
    type: number
    group_label: "Viewer"
    sql: ${TABLE}.active_quarters ;;
  }

  dimension: active_years {
    type: number
    group_label: "Viewer"
    sql: ${TABLE}.active_years ;;
  }

  dimension: platform {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.platform ;;
    drill_fields: [web_platform]
  }

  dimension: web_platform {
    type: string
    group_label: "Dimension"
    sql:${TABLE}.web_platform ;;
  }

  dimension: platform_detail {
    type: string
    group_label: "Dimension"
    sql:${TABLE}.platform_detail ;;
  }

  dimension: media_players_conversion {
    type: number
    group_label: "Dimension"
    sql:${TABLE}.media_players_conversion ;;
    value_format: "#,##0%"
  }

  measure: measure {
    type: number
    sql:
    {% if measure_selection._parameter_value == 'viewers' %}
      ${viewers}
    {% elsif measure_selection._parameter_value == 'views' %}
      ${views_including_paid_views}
    {% else %}
      (${watch_time_minutes} + ${chapter_watch_time_minutes})
    {% endif %};;
    value_format: "#,##0"
  }

  measure: ratio {
    type: number
    sql:
    {% if ratio_selection._parameter_value == 'views_per_viewer' %}
      ${views_per_viewer}
    {% elsif ratio_selection._parameter_value == 'watch_time_per_viewer' %}
      ${watch_time_per_viewer}
    {% elsif ratio_selection._parameter_value == 'watch_time_per_view' %}
      ${watch_time_per_view}
    {% else %}
      ${watch_time_per_session}
    {% endif %};;
    value_format: "#,##0.0"
  }

  measure: content_name_publish_date {
    type: date
    datatype: date
    group_label: "Time"
    sql: MIN(${TABLE}.content_title_publish_date) ;;
  }

  measure: days {
    type: number
    group_label: "Time"
    sql: COUNT(DISTINCT ${TABLE}.date) ;;
    value_format: "#,##0"
  }

  measure: current_days {
    type: max
    group_label: "Time"
    sql: ${TABLE}.current_days ;;
    value_format: "#,##0"
  }

  measure: total_days {
    type: max
    group_label: "Time"
    sql: ${TABLE}.total_days ;;
    value_format: "#,##0"
  }

  measure: sessions {
    type: number
    group_label: "Sessions"
    sql: COUNT(DISTINCT ${TABLE}.session_id) ;;
    value_format: "#,##0"
  }

  measure: no_profile_sessions {
    type: number
    group_label: "Sessions"
    sql: COUNT(DISTINCT IF(${TABLE}.load_id IS NOT NULL AND ${TABLE}.profile_id IS NULL, ${TABLE}.session_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: contents {
    type: number
    group_label: "Contents"
    sql: COUNT(DISTINCT ${TABLE}.content_id) ;;
    value_format: "#,##0"
  }

  measure: content_names {
    type: number
    group_label: "Contents"
    sql: COUNT(DISTINCT ${TABLE}.content_name) ;;
    value_format: "#,##0"
  }

  measure: content_categories {
    type: number
    group_label: "Contents"
    sql: COUNT(DISTINCT ${TABLE}.content_category) ;;
    value_format: "#,##0"
  }

  measure: content_average_length {
    type: average
    group_label: "Contents"
    sql: ${TABLE}.content_title_length_minutes ;;
    value_format: "#,##0.0"
  }

  measure: installs {
    type: number
    group_label: "Installs"
    sql: COUNT(DISTINCT ${TABLE}.install_id) ;;
    value_format: "#,##0"
  }

  measure: installed_uninstalls {
    type: number
    group_label: "Installs"
    sql: COUNT(DISTINCT ${TABLE}.installed_uninstall_id) ;;
    value_format: "#,##0"
  }

  measure: uninstalls {
    type: number
    group_label: "Installs"
    sql: COUNT(DISTINCT ${TABLE}.uninstall_id) ;;
    value_format: "#,##0"
  }

  measure: samsung_downloads {
    type: sum
    group_label: "Installs"
    sql: ${TABLE}.samsung_downloads ;;
    value_format: "#,##0"
  }

  measure: profiles {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT ${TABLE}.profile_id) ;;
    value_format: "#,##0"
  }

  measure: valid_profiles {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.user_id IS NULL, NULL, ${TABLE}.profile_id)) ;;
    value_format: "#,##0"
  }

  measure: users {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT ${TABLE}.user_id) ;;
    value_format: "#,##0"
  }

  measure: both_app_users {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT CONCAT(${TABLE}.app_name, ${app_region}, ${TABLE}.user_id)) ;;
    value_format: "#,##0"
  }

  measure: new_users_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: existing_users_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: samsung_users {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.installation_channel = 'Samsung Galaxy Store', ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: samsung_new_users_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.installation_channel = 'Samsung Galaxy Store' AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: samsung_returning_users_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.installation_channel = 'Samsung Galaxy Store' AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: mobile_users {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "MOBILE", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: mobile_new_users_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "MOBILE" AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: mobile_existing_users_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "MOBILE" AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: tv_users {
    type: number
    label: "TV Users"
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "TV", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: tv_new_users_by_timeframe {
    type: number
    label: "TV New Users By Timeframe"
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "TV" AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: tv_existing_users_by_timeframe {
    type: number
    label: "TV Existing Users By Timeframe"
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "TV" AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_users {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "WEB", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_new_users_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "WEB" AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_existing_users_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "WEB" AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_desktop_new_users_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = "WEB - DESKTOP" AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_desktop_existing_users_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = "WEB - DESKTOP" AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_mobile_new_users_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = "WEB - MOBILE" AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_mobile_existing_users_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = "WEB - MOBILE" AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.user_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: media_play_users {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT ${TABLE}.load_user_id) ;;
    value_format: "#,##0"
  }

  measure: media_start_users {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT ${TABLE}.view_user_id) ;;
    value_format: "#,##0"
  }

  measure: target_viewers {
    type: sum
    group_label: "Users"
    sql: ${TABLE}.viewers ;;
    value_format: "#,##0"
  }

  measure: actual_viewers {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT ${TABLE}.viewer_id) ;;
    value_format: "#,##0"
  }

  measure: viewers {
    type: number
    group_label: "Users"
    sql: ${target_viewers} + ${actual_viewers} ;;
    value_format: "#,##0"
  }

  measure: both_app_viewers {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT CONCAT(${TABLE}.app_name, ${app_region}, ${TABLE}.viewer_id)) ;;
    value_format: "#,##0"
  }

  measure: daily_viewers {
    label: "DAU"
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT CONCAT(${TABLE}.date, ${TABLE}.viewer_id)) ;;
    value_format: "#,##0"
  }

  measure: new_viewers_by_date {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.new_viewer_by_date = "New Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: first_date_new_viewers_by_month {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.first_date_new_viewer_by_month = TRUE, ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: first_date_returning_viewers_by_month {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.first_date_returning_viewer_by_month = TRUE, ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: mobile_first_date_new_viewers_by_month {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.first_date_new_viewer_by_month_by_platform = TRUE AND ${TABLE}.platform = 'MOBILE', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: mobile_first_date_returning_viewers_by_month {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.first_date_returning_viewer_by_month_by_platform = TRUE AND ${TABLE}.platform = 'MOBILE', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: tv_first_date_new_viewers_by_month {
    label: "TV First Date New Viewers By Month"
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.first_date_new_viewer_by_month_by_platform = TRUE AND ${TABLE}.platform = 'TV', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: tv_first_date_returning_viewers_by_month {
    label: "TV First Date Returning Viewers By Month"
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.first_date_returning_viewer_by_month_by_platform = TRUE AND ${TABLE}.platform = 'TV', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_first_date_new_viewers_by_month {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.first_date_new_viewer_by_month_by_platform = TRUE AND ${TABLE}.platform = 'WEB', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_first_date_returning_viewers_by_month {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.first_date_returning_viewer_by_month_by_platform = TRUE AND ${TABLE}.platform = 'WEB', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_desktop_first_date_new_viewers_by_month {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.first_date_new_viewer_by_month_by_web_platform = TRUE AND ${TABLE}.web_platform = 'WEB - DESKTOP', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_desktop_first_date_returning_viewers_by_month {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.first_date_returning_viewer_by_month_by_web_platform = TRUE AND ${TABLE}.web_platform = 'WEB - DESKTOP', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_mobile_first_date_new_viewers_by_month {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.first_date_new_viewer_by_month_by_web_platform = TRUE AND ${TABLE}.web_platform = 'WEB - MOBILE', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_mobile_first_date_returning_viewers_by_month {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.first_date_returning_viewer_by_month_by_web_platform = TRUE AND ${TABLE}.web_platform = 'WEB - MOBILE', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: incremental_daily_unique_viewers_by_month {
    type: number
    group_label: "Users"
    sql: ${first_date_new_viewers_by_month} + ${first_date_returning_viewers_by_month} ;;
    value_format: "#,##0"
  }

  measure: mobile_incremental_daily_unique_viewers_by_month {
    type: number
    group_label: "Users"
    sql: ${mobile_first_date_new_viewers_by_month} + ${mobile_first_date_returning_viewers_by_month} ;;
    value_format: "#,##0"
  }

  measure: tv_incremental_daily_unique_viewers_by_month {
    label: "TV Incremental Daily Unique Viewers By Month"
    type: number
    group_label: "Users"
    sql: ${tv_first_date_new_viewers_by_month} + ${tv_first_date_returning_viewers_by_month} ;;
    value_format: "#,##0"
  }

  measure: web_incremental_daily_unique_viewers_by_month {
    type: number
    group_label: "Users"
    sql: ${web_first_date_new_viewers_by_month} + ${web_first_date_returning_viewers_by_month} ;;
    value_format: "#,##0"
  }

  measure: web_desktop_incremental_daily_unique_viewers_by_month {
    type: number
    group_label: "Users"
    sql: ${web_desktop_first_date_new_viewers_by_month} + ${web_desktop_first_date_returning_viewers_by_month} ;;
    value_format: "#,##0"
  }

  measure: web_mobile_incremental_daily_unique_viewers_by_month {
    type: number
    group_label: "Users"
    sql: ${web_mobile_first_date_new_viewers_by_month} + ${web_mobile_first_date_returning_viewers_by_month} ;;
    value_format: "#,##0"
  }

  measure: accumulated_unique_viewers_by_month {
    type: running_total
    group_label: "Users"
    sql: ${incremental_daily_unique_viewers_by_month} ;;
    value_format: "#,##0"
  }

  measure: mobile_accumulated_unique_viewers_by_month {
    type: running_total
    group_label: "Users"
    sql: ${mobile_incremental_daily_unique_viewers_by_month} ;;
    value_format: "#,##0"
  }

  measure: tv_accumulated_unique_viewers_by_month {
    label: "TV Accumulated Unique Viewers By Month"
    type: running_total
    group_label: "Users"
    sql: ${tv_incremental_daily_unique_viewers_by_month} ;;
    value_format: "#,##0"
  }

  measure: web_accumulated_unique_viewers_by_month {
    type: running_total
    group_label: "Users"
    sql: ${web_incremental_daily_unique_viewers_by_month} ;;
    value_format: "#,##0"
  }

  measure: web_desktop_accumulated_unique_viewers_by_month {
    type: running_total
    group_label: "Users"
    sql: ${web_desktop_incremental_daily_unique_viewers_by_month} ;;
    value_format: "#,##0"
  }

  measure: web_mobile_accumulated_unique_viewers_by_month {
    type: running_total
    group_label: "Users"
    sql: ${web_mobile_incremental_daily_unique_viewers_by_month} ;;
    value_format: "#,##0"
  }

  measure: new_viewers_by_month {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.new_viewer_by_month = "New Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: new_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: existing_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: samsung_viewers {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.installation_channel = 'Samsung Galaxy Store', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: samsung_new_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.installation_channel = 'Samsung Galaxy Store' AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: samsung_returning_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.installation_channel = 'Samsung Galaxy Store' AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: mobile_viewers {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "MOBILE", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: mobile_new_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "MOBILE" AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: mobile_existing_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "MOBILE" AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: tv_viewers {
    type: number
    label: "TV Viewers"
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "TV", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: tv_new_viewers_by_timeframe {
    type: number
    label: "TV New Viewers By Timeframe"
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "TV" AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: tv_existing_viewers_by_timeframe {
    type: number
    label: "TV Existing Viewers By Timeframe"
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "TV" AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_viewers {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "WEB", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_new_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "WEB" AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_existing_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = "WEB" AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_desktop_viewers {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = "WEB - DESKTOP", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_desktop_new_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = "WEB - DESKTOP" AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_desktop_existing_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = "WEB - DESKTOP" AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_mobile_viewers {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = "WEB - MOBILE", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_mobile_new_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = "WEB - MOBILE" AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_mobile_existing_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = "WEB - MOBILE" AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: target_paid_viewers {
    type: sum
    group_label: "Users"
    sql: ${TABLE}.paid_viewers ;;
    value_format: "#,##0"
  }

  measure: actual_paid_viewers {
    type: number
    group_label: "Users"
    sql: COUNT(DISTINCT IF(${TABLE}.content_title_type = 'paid', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: paid_viewers {
    type: number
    group_label: "Users"
    sql: ${target_paid_viewers} + ${actual_paid_viewers} ;;
    value_format: "#,##0"
  }

  measure: target_video_viewers {
    type: sum
    group_label: "Users"
    sql: ${TABLE}.video_viewers ;;
    value_format: "#,##0"
  }

  measure: actual_video_viewers {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load"), ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: mobile_video_viewers {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load") AND ${TABLE}.platform = 'MOBILE', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: tv_video_viewers {
    label: "TV Video Viewers"
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load") AND ${TABLE}.platform = 'TV', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_video_viewers {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load") AND ${TABLE}.platform = 'WEB', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_desktop_video_viewers {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load") AND ${TABLE}.web_platform = 'WEB - DESKTOP', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_mobile_video_viewers {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load") AND ${TABLE}.web_platform = 'WEB - MOBILE', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: video_viewers {
    type: number
    group_label: "Users"
    sql: ${target_video_viewers} + ${actual_video_viewers} ;;
    value_format: "#,##0"
  }

  measure: video_new_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load") AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: video_existing_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load") AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: target_chapter_viewers {
    type: sum
    group_label: "Users"
    sql: ${TABLE}.chapter_viewers ;;
    value_format: "#,##0"
  }

  measure: actual_chapter_viewers {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name = 'chapter_load', ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: chapter_viewers {
    type: number
    group_label: "Users"
    sql: ${target_chapter_viewers} + ${actual_chapter_viewers} ;;
    value_format: "#,##0"
  }

  measure: chapter_new_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name = 'chapter_load' AND ${new_viewer_by_timeframe} = "New Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: chapter_existing_viewers_by_timeframe {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name = 'chapter_load' AND ${new_viewer_by_timeframe} = "Returning Viewer", ${TABLE}.viewer_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: original_media_plays {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT ${TABLE}.original_load_id) ;;
    value_format: "#,##0"
  }

  measure: media_plays {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT ${TABLE}.load_id) ;;
    value_format: "#,##0"
  }

  measure: media_starts {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT ${TABLE}.view_load_id) ;;
    value_format: "#,##0"
  }

  measure: target_views {
    type: sum
    group_label: "Views"
    sql: ${TABLE}.views ;;
    value_format: "#,##0"
  }

  measure: actual_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT ${TABLE}.view5s_load_id) ;;
    value_format: "#,##0"
  }

  measure: mobile_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = 'MOBILE', ${TABLE}.view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: tv_views {
    label: "TV Views"
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = 'TV', ${TABLE}.view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = 'WEB', ${TABLE}.view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_desktop_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = 'WEB - DESKTOP', ${TABLE}.view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_mobile_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = 'WEB - MOBILE', ${TABLE}.view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: views {
    type: number
    group_label: "Views"
    sql: ${target_views} + ${actual_views} ;;
    value_format: "#,##0"
  }

  measure: unique_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT CONCAT(${TABLE}.viewer_id, ${TABLE}.content_id)) ;;
    value_format: "#,##0"
  }

  measure: views_including_paid_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT ${TABLE}.all_view5s_load_id) ;;
    value_format: "#,##0"
  }

  measure: mobile_views_including_paid_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = 'MOBILE', ${TABLE}.all_view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: tv_views_including_paid_views {
    label: "TV Views Including Paid Views"
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = 'TV', ${TABLE}.all_view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_views_including_paid_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT IF(${TABLE}.platform = 'WEB', ${TABLE}.all_view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_desktop_views_including_paid_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = 'WEB - DESKTOP', ${TABLE}.all_view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_mobile_views_including_paid_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT IF(${TABLE}.web_platform = 'WEB - MOBILE', ${TABLE}.all_view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }


  measure: paid_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT ${TABLE}.paid_view_load_id) ;;
    value_format: "#,##0"
  }

  measure: paid_acquisition_views {
    type: number
    group_label: "Views"
    sql: COUNT(DISTINCT IF(${TABLE}.utm_source_group = 'Paid', ${TABLE}.all_view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: target_video_views {
    type: sum
    group_label: "Views"
    sql: ${TABLE}.video_views ;;
    value_format: "#,##0"
  }

  measure: actual_video_views {
    type: number
    group_label: "Views"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load"), ${TABLE}.view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: mobile_video_views {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load") AND ${TABLE}.platform = 'MOBILE', ${TABLE}.view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: tv_video_views {
    label: "TV Video Views"
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load") AND ${TABLE}.platform = 'TV', ${TABLE}.view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_video_views {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load") AND ${TABLE}.platform = 'WEB', ${TABLE}.view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_desktop_video_views {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load") AND ${TABLE}.web_platform = 'WEB - DESKTOP', ${TABLE}.view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: web_mobile_video_views {
    type: number
    group_label: "Users"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name IN ("video_load", "livestream_load") AND ${TABLE}.web_platform = 'WEB - MOBILE', ${TABLE}.view5s_load_id, NULL)) ;;
  }

  measure: video_views {
    type: number
    group_label: "Views"
    sql: ${target_video_views} + ${actual_video_views} ;;
    value_format: "#,##0"
  }

  measure: target_chapter_views {
    type: sum
    group_label: "Views"
    sql: ${TABLE}.chapter_views ;;
    value_format: "#,##0"
  }

  measure: actual_chapter_views {
    type: number
    group_label: "Views"
    sql:COUNT(DISTINCT IF(${TABLE}.event_name = 'chapter_load', ${TABLE}.view5s_load_id, NULL)) ;;
    value_format: "#,##0"
  }

  measure: chapter_views {
    type: number
    group_label: "Views"
    sql: ${target_chapter_views} + ${actual_chapter_views} ;;
    value_format: "#,##0"
  }

  measure: target_watch_time_minutes {
    type: sum
    group_label: "Watch Time"
    sql: IF(${TABLE}.data_type = 'Target', ${TABLE}.watch_time_minutes, 0) ;;
    value_format: "#,##0"
  }

  measure: actual_watch_time_minutes {
    type: sum
    group_label: "Watch Time"
    sql: IF(${TABLE}.data_type = 'Actual', ${TABLE}.watch_time_minutes, 0) ;;
    value_format: "#,##0"
  }

  measure: mobile_watch_time_minutes {
    type: sum
    group_label: "Watch Time"
    sql: IF(${TABLE}.data_type = 'Actual' AND ${TABLE}.platform = 'MOBILE', ${TABLE}.watch_time_minutes, 0) ;;
    value_format: "#,##0"
  }

  measure: tv_watch_time_minutes {
    label: "TV Watch Time Minutes"
    type: sum
    group_label: "Watch Time"
    sql: IF(${TABLE}.data_type = 'Actual' AND ${TABLE}.platform = 'TV', ${TABLE}.watch_time_minutes, 0) ;;
    value_format: "#,##0"
  }

  measure: web_watch_time_minutes {
    type: sum
    group_label: "Watch Time"
    sql: IF(${TABLE}.data_type = 'Actual' AND ${TABLE}.platform = 'WEB', ${TABLE}.watch_time_minutes, 0) ;;
    value_format: "#,##0"
  }

  measure: web_desktop_watch_time_minutes {
    type: sum
    group_label: "Watch Time"
    sql: IF(${TABLE}.data_type = 'Actual' AND ${TABLE}.web_platform = 'WEB - DESKTOP', ${TABLE}.watch_time_minutes, 0) ;;
    value_format: "#,##0"
  }

  measure: web_mobile_watch_time_minutes {
    type: sum
    group_label: "Watch Time"
    sql: IF(${TABLE}.data_type = 'Actual' AND ${TABLE}.web_platform = 'WEB - MOBILE', ${TABLE}.watch_time_minutes, 0) ;;
    value_format: "#,##0"
  }

  measure: watch_time_minutes {
    type: sum
    group_label: "Watch Time"
    sql: ${TABLE}.watch_time_minutes ;;
    value_format: "#,##0"
  }

  measure: chapter_watch_time_minutes {
    type: sum
    group_label: "Watch Time"
    sql: ${TABLE}.chapter_watch_time_minutes ;;
    value_format: "#,##0"
  }

  measure: views_per_viewer {
    label: "Views/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${actual_viewers} = 0, 0, ${views_including_paid_views} / ${actual_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: mobile_views_per_viewer {
    label: "Mobile Views/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${mobile_viewers} = 0, 0, ${mobile_views_including_paid_views} / ${mobile_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: tv_views_per_viewer {
    label: "TV Views/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${tv_viewers} = 0, 0, ${tv_views_including_paid_views} / ${tv_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: web_views_per_viewer {
    label: "Web Views/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${web_viewers} = 0, 0, ${web_views_including_paid_views} / ${web_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: web_desktop_views_per_viewer {
    label: "Web Desktop Views/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${web_desktop_viewers} = 0, 0, ${web_desktop_views_including_paid_views} / ${web_desktop_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: web_mobile_views_per_viewer {
    label: "Web Mobile Views/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${web_mobile_viewers} = 0, 0, ${web_mobile_views_including_paid_views} / ${web_mobile_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: watch_time_per_viewer {
    label: "Watch Time/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${actual_viewers} = 0, 0, (${actual_watch_time_minutes} + ${chapter_watch_time_minutes}) / ${actual_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: video_watch_time_per_viewer {
    label: "Video Watch Time/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${actual_video_viewers} = 0, 0, (${actual_watch_time_minutes}) / ${actual_video_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: mobile_video_watch_time_per_viewer {
    label: "Mobile Video Watch Time/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${mobile_video_viewers} = 0, 0, (${mobile_watch_time_minutes}) / ${mobile_video_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: tv_video_watch_time_per_viewer {
    label: "TV Video Watch Time/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${tv_video_viewers} = 0, 0, (${tv_watch_time_minutes}) / ${tv_video_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: web_video_watch_time_per_viewer {
    label: "Web Video Watch Time/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${web_video_viewers} = 0, 0, (${web_watch_time_minutes}) / ${web_video_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: web_desktop_video_watch_time_per_viewer {
    label: "Web Desktop Video Watch Time/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${web_desktop_video_viewers} = 0, 0, (${web_desktop_watch_time_minutes}) / ${web_desktop_video_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: web_mobile_video_watch_time_per_viewer {
    label: "Web Mobile Video Watch Time/ Viewer"
    type: number
    group_label: "Ratio"
    sql: IF(${web_mobile_video_viewers} = 0, 0, (${web_mobile_watch_time_minutes}) / ${web_mobile_video_viewers}) ;;
    value_format: "#,##0.0"
  }

  measure: watch_time_per_view {
    label: "Watch Time/ View"
    type: number
    group_label: "Ratio"
    sql: IF(${actual_views} = 0, 0, (${actual_watch_time_minutes} + ${chapter_watch_time_minutes}) / ${actual_views}) ;;
    value_format: "#,##0.0"
  }

  measure: video_watch_time_per_view {
    label: "Video Watch Time/ View"
    type: number
    group_label: "Ratio"
    sql: IF(${actual_video_views} = 0, 0, (${actual_watch_time_minutes}) / ${actual_video_views}) ;;
    value_format: "#,##0.0"
  }

  measure: mobile_video_watch_time_per_view {
    label: "Mobile Video Watch Time/ View"
    type: number
    group_label: "Ratio"
    sql: IF(${mobile_video_views} = 0, 0, (${mobile_watch_time_minutes}) / ${mobile_video_views}) ;;
    value_format: "#,##0.0"
  }

  measure: tv_video_watch_time_per_view {
    label: "TV Video Watch Time/ View"
    type: number
    group_label: "Ratio"
    sql: IF(${tv_video_views} = 0, 0, (${tv_watch_time_minutes}) / ${tv_video_views}) ;;
    value_format: "#,##0.0"
  }

  measure: web_video_watch_time_per_view {
    label: "Web Video Watch Time/ View"
    type: number
    group_label: "Ratio"
    sql: IF(${web_video_views} = 0, 0, (${web_watch_time_minutes}) / ${web_video_views}) ;;
    value_format: "#,##0.0"
  }

  measure: web_desktop_video_watch_time_per_view {
    label: "Web Desktop Video Watch Time/ View"
    type: number
    group_label: "Ratio"
    sql: IF(${web_desktop_video_views} = 0, 0, (${web_desktop_watch_time_minutes}) / ${web_desktop_video_views}) ;;
    value_format: "#,##0.0"
  }

  measure: web_mobile_video_watch_time_per_view {
    label: "Web Mobile Video Watch Time/ View"
    type: number
    group_label: "Ratio"
    sql: IF(${web_mobile_video_views} = 0, 0, (${web_mobile_watch_time_minutes}) / ${web_mobile_video_views}) ;;
    value_format: "#,##0.0"
  }

  measure: watch_time_per_session {
    label: "Watch Time/ Session"
    type: number
    group_label: "Ratio"
    sql: IF(${sessions} = 0, 0, (${actual_watch_time_minutes} + ${chapter_watch_time_minutes}) / ${sessions}) ;;
    value_format: "#,##0.0"
  }

  measure: vs_target_viewers {
    label: "% Target Viewers"
    type: number
    group_label: "Target"
    sql: ${actual_viewers} / ${target_viewers} ;;
    value_format: "#,##0.0%"
  }

  measure: vs_target_chapter_viewers {
    label: "% Target Chapter Viewers"
    type: number
    group_label: "Target"
    sql: ${actual_chapter_viewers} / ${target_chapter_viewers} ;;
    value_format: "#,##0.0%"
  }

  measure: vs_target_views {
    label: "% Target Views"
    type: number
    group_label: "Target"
    sql: ${actual_views} / ${target_views} ;;
    value_format: "#,##0.0%"
  }

  measure: vs_target_video_views {
    label: "% Target Video Views"
    type: number
    group_label: "Target"
    sql: ${actual_video_views} / ${target_video_views} ;;
    value_format: "#,##0.0%"
  }

  measure: vs_target_chapter_views {
    label: "% Target Chapter Views"
    type: number
    group_label: "Target"
    sql: ${actual_chapter_views} / ${target_chapter_views} ;;
    value_format: "#,##0.0%"
  }

  measure: vs_target_video_watch_time_minutes {
    label: "% Target Video Watch Time"
    type: number
    group_label: "Target"
    sql: ${actual_watch_time_minutes} / ${target_watch_time_minutes} ;;
    value_format: "#,##0.0%"
  }
}
