view: user_view_interact {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_User_View_Interact`
    ;;

  parameter: timeframe_selection {
    type: unquoted
    allowed_value: {
      label: "Daily"
      value: "date"
    }
    allowed_value: {
      label: "Weekly"
      value: "week"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
    allowed_value: {
      label: "Quarterly"
      value: "quarter"
    }
    allowed_value: {
      label: "Yearly"
      value: "year"
    }
  }

  dimension: timeframe {
    type: date
    datatype: date
    group_label: "Time"
    sql:
    {% if timeframe_selection._parameter_value == 'date' %}
      ${date}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${week}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${month}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${quarter}
    {% else %}
      ${year}
    {% endif %};;
  }

  dimension: timeframe_order {
    type: string
    group_label: "Time"
    sql:
    {% if timeframe_selection._parameter_value == 'date' %}
      ${date}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${year_week}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${month_format}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${quarter_format}
    {% else %}
      ${year_format}
    {% endif %};;
  }

  dimension: timeframe_format {
    type: string
    group_label: "Time"
    sql:
    {% if timeframe_selection._parameter_value == 'date' %}
      ${date_format}
    {% elsif timeframe_selection._parameter_value == 'week' %}
      ${week_format}
    {% elsif timeframe_selection._parameter_value == 'month' %}
      ${month_format}
    {% elsif timeframe_selection._parameter_value == 'quarter' %}
      ${quarter_format}
    {% else %}
      ${year_format}
    {% endif %};;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.date) ;;
  }

  dimension: week {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.week ;;
  }

  dimension: year_week {
    type: string
    group_label: "Time"
    sql: ${TABLE}.year_week ;;
  }

  dimension: week_format {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_format ;;
  }

  dimension: week_range {
    type: string
    group_label: "Time"
    sql: ${TABLE}.week_range ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, MONTH) ;;
  }

  dimension: month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.date) ;;
    drill_fields: [date]
  }

  dimension: quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, QUARTER) ;;
  }

  dimension: quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.date) ;;
  }

  dimension: year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, YEAR) ;;
  }

  dimension: year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.date) ;;
  }

  dimension: app_name {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    group_label: "Dimension"
    sql: IF(${TABLE}.app_region = 'TH', 'TH', 'VN') ;;
  }

  dimension: platform {
    type: string
    group_label: "Dimension"
    sql: ${TABLE}.platform ;;
  }

  dimension: l_column {
    type: number
    group_label: "Entity"
    sql: ${TABLE}.l_column ;;
  }

  dimension: l_row {
    type: number
    group_label: "Entity"
    sql: ${TABLE}.l_row ;;
  }

  dimension: category {
    type: string
    group_label: "Entity"
    sql: ${TABLE}.category ;;
  }

  dimension: entity_id {
    type: string
    group_label: "Entity"
    sql: ${TABLE}.entity_id ;;
  }

  dimension: entity_title {
    type: string
    group_label: "Entity"
    sql: ${TABLE}.entity_title ;;
  }

  dimension: entity_type {
    type: string
    group_label: "Entity"
    sql: ${TABLE}.entity_type_adj ;;
  }

  dimension: entity_detail {
    type: string
    group_label: "Entity"
    sql: ${TABLE}.entity_detail ;;
  }

  dimension: object_id {
    type: string
    group_label: "Entity"
    sql: ${TABLE}.object_id ;;
  }

  dimension: object_title {
    type: string
    group_label: "Entity"
    sql: ${TABLE}.object_title ;;
  }

  dimension: object_type {
    type: string
    group_label: "Entity"
    sql: ${TABLE}.object_type ;;
  }

  measure: page_view_users {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.page_view_user_id) ;;
    value_format: "#,##0"
  }

  measure: page_views {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.page_views ;;
    value_format: "#,##0"
  }

  measure: click_users {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.click_user_id) ;;
    value_format: "#,##0"
  }

  measure: clicks {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.clicks ;;
    value_format: "#,##0"
  }

  measure: installs {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.install_id) ;;
    value_format: "#,##0"
  }

  measure: session {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.session_id) ;;
    value_format: "#,##0"
  }

  measure: viewers {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.viewer_id) ;;
    value_format: "#,##0"
  }

  measure: views {
    type: number
    group_label: "Measure"
    sql: COUNT(DISTINCT ${TABLE}.all_view5s_load_id) ;;
    value_format: "#,##0"
  }

  measure: watch_time_minutes {
    type: sum
    group_label: "Measure"
    sql: ${TABLE}.watch_time_minutes ;;
    value_format: "#,##0"
  }
}
