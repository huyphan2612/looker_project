view: estimated_revenue_tracking {
  sql_table_name: `pops-204909.NTDChi.POPSWW_Scheduled_DATAMART_Estimated_Revenue_Tracking`
    ;;

  parameter: timeframe_selection {
    type: unquoted
    allowed_value: {
      label: "Daily"
      value: "date"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
    allowed_value: {
      label: "Quarterly"
      value: "quarter"
    }
    allowed_value: {
      label: "Yearly"
      value: "year"
    }
  }

  dimension: timeframe_format {
    type: string
    group_label: "Time"
    sql:
      {% if timeframe_selection._parameter_value == 'date' %}
        ${date}
      {% elsif timeframe_selection._parameter_value == 'month' %}
        ${month_format}
      {% elsif timeframe_selection._parameter_value == 'quarter' %}
        ${quarter_format}
      {% else %}
        ${year_format}
      {% endif %};;
  }

  dimension: date {
    type: date
    datatype: date
    group_label: "Time"
    sql: ${TABLE}.date ;;
  }

  dimension: date_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%b %d", ${TABLE}.date) ;;
  }

  dimension: month {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, MONTH) ;;
  }

  dimension: month_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-%m", ${TABLE}.date) ;;
  }

  dimension: quarter {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, QUARTER) ;;
  }

  dimension: quarter_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y-Q%Q", ${TABLE}.date) ;;
  }

  dimension: year {
    type: date
    datatype: date
    group_label: "Time"
    sql: DATE_TRUNC(${TABLE}.date, YEAR) ;;
  }

  dimension: year_format {
    type: string
    group_label: "Time"
    sql: FORMAT_DATE("%Y", ${TABLE}.date) ;;
  }

  dimension: data_status {
    type: string
    sql: ${TABLE}.data_status ;;
  }

  dimension: data_type {
    type: string
    sql: ${TABLE}.data_type ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: revenue_stream {
    type: string
    sql: ${TABLE}.revenue_stream ;;
    drill_fields: [vertical]
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.vertical ;;
  }

  measure: lastest_month {
    type: date
    datatype: date
    sql: MAX(IF(${TABLE}.data_type = 'Actual' AND ${TABLE}.revenue > 0, ${TABLE}.date, NULL)) ;;
  }

  measure: target_revenue {
    type: sum
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ABS(${TABLE}.target_revenue)
      ELSE 0 END ;;
    value_format: "$#,##0"
  }

  measure: actual_revenue {
    type: sum
    sql:
      CASE WHEN {{_user_attributes["revenue_permission"]}} = 1
      THEN ABS(${TABLE}.revenue)
      ELSE 0 END ;;
    value_format: "$#,##0"
  }

  measure: revenue {
    type: number
    sql: ${target_revenue} + ${actual_revenue} ;;
    value_format: "$#,##0"
  }

  measure: target_views {
    type: sum
    sql: ABS(${TABLE}.target_views) ;;
    value_format: "#,##0"
  }

  measure: actual_views {
    type: sum
    sql: ABS(${TABLE}.views) ;;
    value_format: "#,##0"
  }

  measure: views {
    type: number
    sql: ${target_views} + ${actual_views} ;;
    value_format: "#,##0"
  }
}
