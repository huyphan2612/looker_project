view: comic_content_acquisition_weekly_report {
  sql_table_name: `pops-204909.MTHD.Comic_Content_Acquisition_Weekly_Report`
    ;;

  dimension_group: confirmed_airing_start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.confirmed_airing_start_date ;;
  }

  dimension: contract_label {
    type: string
    sql: ${TABLE}.contract_label ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: exclusity_ {
    type: string
    sql: ${TABLE}.exclusity_ ;;
  }

  dimension: frequency_per_week {
    type: number
    sql: ${TABLE}.frequency_per_week ;;
  }

  dimension_group: inserted {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.inserted_date ;;
  }

  dimension: last_week_status {
    type: string
    sql: ${TABLE}.last_week_status ;;
  }

  dimension: license_rights {
    type: string
    sql: ${TABLE}.license_rights ;;
  }

  dimension: licensed_platforms {
    type: string
    sql: ${TABLE}.licensed_platforms ;;
  }

  dimension: locallization_cost {
    type: number
    sql: ${TABLE}.locallization_cost ;;
  }

  dimension_group: lp_expiry {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.lp_expiry_date ;;
  }

  dimension_group: lp_start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.lp_start_date ;;
  }

  dimension: mg_flat_fee {
    type: number
    sql: ${TABLE}.mg_flat_fee ;;
  }

  dimension: name_of_airing_channel_platform {
    type: string
    sql: ${TABLE}.name_of_airing_channel_platform ;;
  }

  dimension: name_of_partner_on_contract {
    type: string
    sql: ${TABLE}.name_of_partner_on_contract ;;
  }

  dimension: new_titles {
    type: string
    sql: ${TABLE}.new_titles ;;
  }

  dimension: partner {
    type: string
    sql: ${TABLE}.partner ;;
  }

  dimension: payment_period {
    type: string
    sql: ${TABLE}.payment_period ;;
  }

  dimension: payment_status {
    type: string
    sql: ${TABLE}.payment_status ;;
  }

  dimension: project_id {
    type: string
    sql: ${TABLE}.project_id ;;
  }

  dimension: project_name {
    type: string
    sql: ${TABLE}.project_name ;;
  }

  dimension: received_chapters {
    type: number
    sql: ${TABLE}.received_chapters ;;
  }

  dimension: released_chapters {
    type: number
    sql: ${TABLE}.released_chapters ;;
  }

  dimension: report {
    type: string
    sql: ${TABLE}.report ;;
  }

  dimension: revenue_share {
    type: string
    sql: ${TABLE}.revenue_share ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension_group: tentative_airing {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.tentative_airing_date ;;
  }

  dimension: term_of_contract {
    type: string
    sql: ${TABLE}.term_of_contract ;;
  }

  dimension: territory {
    type: string
    sql: ${TABLE}.territory ;;
  }

  dimension: this_week_status {
    type: string
    sql: ${TABLE}.this_week_status ;;
  }

  dimension: total_chapters {
    type: number
    sql: ${TABLE}.total_chapters ;;
  }

  dimension: unreceive_chapters {
    type: number
    sql: ${TABLE}.unreceive_chapters ;;
  }

  measure: count {
    type: count
    drill_fields: [project_name]
  }
}
