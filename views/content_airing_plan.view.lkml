view: content_airing_plan {
  sql_table_name: `pops-204909.Google_Spreadsheet.Content_Airing_Plan`
    ;;

  dimension: app_install {
    type: number
    sql: ${TABLE}.app_install ;;
  }

  dimension: app_revenue {
    type: number
    sql: ${TABLE}.app_revenue ;;
  }

  dimension: app_views {
    type: number
    sql: ${TABLE}.app_views ;;
  }

  dimension_group: confirmed_airing {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.confirmed_airing_date ;;
  }

  dimension: content_vertical {
    type: string
    sql: ${TABLE}.content_vertical ;;
  }

  dimension: contract_label {
    type: string
    sql: ${TABLE}.contract_label ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension_group: day_of_released {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.day_of_released ;;
  }

  measure: day_of_released_start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: min(${TABLE}.day_of_released);;
  }


  measure: day_of_released_end {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: max(${TABLE}.day_of_released);;
  }


  dimension: duration_per_ep_mins {
    type: number
    sql: ${TABLE}.duration_per_ep_mins ;;
  }

  dimension: episode {
    type: string
    sql: ${TABLE}.episode ;;
  }

  dimension: estimated_number_of_eps_chaps {
    type: number
    sql: ${TABLE}.estimated_number_of_eps_chaps ;;
  }

  dimension: exclusity {
    type: string
    sql: ${TABLE}.exclusity ;;
  }

  dimension: factor {
    type: number
    sql: ${TABLE}.factor ;;
  }

  dimension_group: file_delivery_expected {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.file_delivery_expected_date ;;
  }

  dimension: first_airing {
    type: string
    sql: ${TABLE}.first_airing ;;
  }

  dimension: frequency_per_week {
    type: number
    sql: ${TABLE}.frequency_per_week ;;
  }

  dimension: license_fee {
    type: number
    sql: ${TABLE}.license_fee ;;
  }

  dimension: license_rights {
    type: string
    sql: ${TABLE}.license_rights ;;
  }

  dimension: licensed_platforms {
    type: string
    sql: ${TABLE}.licensed_platforms ;;
  }

  dimension: licensed_platforms_popsapp {
    type: string
    sql: ${TABLE}.licensed_platforms_popsapp ;;
  }

  dimension: licensed_platforms_popskids {
    type: string
    sql: ${TABLE}.licensed_platforms_popskids ;;
  }

  dimension: licensed_platforms_youtube {
    type: string
    sql: ${TABLE}.licensed_platforms_youtube ;;
  }

  dimension: localization_cost {
    type: number
    sql: ${TABLE}.localization_cost ;;
  }

  dimension_group: lp_expiry {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.lp_expiry_date ;;
  }

  dimension_group: lp_start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.lp_start_date ;;
  }

  dimension: material_fee {
    type: number
    sql: ${TABLE}.material_fee ;;
  }

  dimension: mg {
    type: number
    sql: ${TABLE}.mg ;;
  }

  dimension: mg_flat_fee {
    type: number
    sql: ${TABLE}.mg_flat_fee ;;
  }

  dimension: name_of_airing_channel_platform {
    type: string
    sql: ${TABLE}.name_of_airing_channel_platform ;;
  }

  dimension: name_of_airing_channel_pops_platform {
    type: string
    sql: ${TABLE}.name_of_airing_channel_pops_platform ;;
  }

  dimension: name_of_partner_on_contract {
    type: string
    sql: ${TABLE}.name_of_partner_on_contract ;;
  }

  dimension: non_exclusive {
    type: string
    sql: ${TABLE}.non_exclusive ;;
  }

  dimension: number_of_eps {
    type: number
    sql: ${TABLE}.number_of_eps ;;
  }

  dimension: number_of_eps_received {
    type: number
    sql: ${TABLE}.number_of_eps_received ;;
  }

  dimension: number_of_eps_released {
    type: number
    sql: ${TABLE}.number_of_eps_released ;;
  }

  dimension: original_language {
    type: string
    sql: ${TABLE}.original_language ;;
  }

  dimension: ownership {
    type: string
    sql: ${TABLE}.ownership ;;
  }

  dimension: partner {
    type: string
    sql: ${TABLE}.partner ;;
  }

  dimension: payment_period {
    type: string
    sql: ${TABLE}.payment_period ;;
  }

  dimension: payment_status {
    type: string
    sql: ${TABLE}.payment_status ;;
  }

  dimension: pnl_status {
    type: string
    sql: ${TABLE}.pnl_status ;;
  }

  dimension: production_cost {
    type: number
    sql: ${TABLE}.production_cost ;;
  }

  dimension: project_code {
    type: string
    sql: ${TABLE}.project_code ;;
  }

  dimension: project_id {
    type: string
    sql: ${TABLE}.project_id ;;
  }

  dimension: project_name {
    type: string
    sql: ${TABLE}.project_name ;;
  }

  dimension: ranking {
    type: string
    sql: ${TABLE}.ranking ;;
  }

  dimension: received_chapters {
    type: number
    sql: ${TABLE}.received_chapters ;;
  }

  dimension: released_chapters {
    type: number
    sql: ${TABLE}.released_chapters ;;
  }

  dimension: report {
    type: string
    sql: ${TABLE}.report ;;
  }

  dimension: revenue_share {
    type: string
    sql: ${TABLE}.revenue_share ;;
  }

  dimension: season {
    type: number
    sql: ${TABLE}.season ;;
  }

  dimension: second_airing {
    type: string
    sql: ${TABLE}.second_airing ;;
  }

  dimension: source_type {
    type: string
    sql: ${TABLE}.source_type ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: target_audience {
    type: string
    sql: ${TABLE}.target_audience ;;
  }

  dimension: target_language {
    type: string
    sql: ${TABLE}.target_language ;;
  }

  dimension_group: tentative_airing {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.tentative_airing_date ;;
  }

  dimension: term_of_contract {
    type: string
    sql: ${TABLE}.term_of_contract ;;
  }

  dimension: territory {
    type: string
    sql: ${TABLE}.territory ;;
  }

  dimension: total_chapters {
    type: number
    sql: ${TABLE}.total_chapters ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension: type_of_content {
    type: string
    sql: ${TABLE}.type_of_content ;;
  }

  dimension: type_of_localization {
    type: string
    sql: ${TABLE}.type_of_localization ;;
  }

  dimension: unreceive_chapters {
    type: number
    sql: ${TABLE}.unreceive_chapters ;;
  }

  dimension: video_original_language {
    type: string
    sql: ${TABLE}.video_original_language ;;
  }

  dimension: video_target_language {
    type: string
    sql: ${TABLE}.video_target_language ;;
  }

  dimension: youtube_revenue {
    type: number
    sql: ${TABLE}.youtube_revenue ;;
  }

  dimension: youtube_views {
    type: number
    sql: ${TABLE}.youtube_views ;;
  }

  dimension: licensed_platform {
    type: string
    sql: if (${TABLE}.licensed_platforms_popsapp = 'Yes' and
             ${TABLE}.licensed_platforms_popskids is null and
             ${TABLE}.licensed_platforms_youtube is null, "Only POPS App",
            if (${TABLE}.licensed_platforms_popsapp = 'Yes' and
                ${TABLE}.licensed_platforms_popskids = 'Yes'and
                ${TABLE}.licensed_platforms_youtube is null, "POPS App & POPS Kids App",
              if (${TABLE}.licensed_platforms_popsapp is null and
                  ${TABLE}.licensed_platforms_popskids = 'Yes'and
                  ${TABLE}.licensed_platforms_youtube is null," Only POPS Kids App",
                if (${TABLE}.licensed_platforms_popsapp is null and
                   ${TABLE}.licensed_platforms_popskids is null and
                   ${TABLE}.licensed_platforms_youtube = 'Yes', "Only Youtube",
                  if (${TABLE}.licensed_platforms_popsapp is null and
                    ${TABLE}.licensed_platforms_popskids = 'Yes'and
                    ${TABLE}.licensed_platforms_youtube = 'Yes', "POPS Kids App & Youtube",
                    if (${TABLE}.licensed_platforms_popsapp = 'Yes' and
                        ${TABLE}.licensed_platforms_popskids is null and
                         ${TABLE}.licensed_platforms_youtube = 'Yes', "POPS Kids App & Youtube",
                      if (${TABLE}.licensed_platforms_popsapp = 'Yes' and
                          ${TABLE}.licensed_platforms_popskids = 'Yes'and
                          ${TABLE}.licensed_platforms_youtube = 'Yes', "All 3 Platforms", "NA")))))))
;;
  }

  measure: number_of_titles {
    type: count_distinct
    sql: ${TABLE}.project_name;;}


  measure: number_of_airing_eps_chaps {
    type:sum
    sql: ${TABLE}.factor  ;;
}

 measure: total_minutes {
   type:  sum
   sql:  ${TABLE}.factor * ${TABLE}.duration_per_ep_mins;;
  value_format: "#,##0"
 }

  measure: total_hours {
    type:  sum
    sql:   (${TABLE}.factor * ${TABLE}.duration_per_ep_mins)/60;;
    value_format: "#,##0"
  }
  measure: count {
    type: count
    drill_fields: [project_name]
  }
}
