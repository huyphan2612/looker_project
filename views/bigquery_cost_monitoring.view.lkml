view: bigquery_cost_monitoring {
  sql_table_name: `pops-204909.GCP_cost.View_BigQuery_Cost`
    ;;

  dimension: hh {
    type: number
    sql: ${TABLE}.hh ;;
  }

  dimension: minutes {
    type: number
    sql: ${TABLE}.minutes ;;
  }

  dimension: query {
    type: string
    sql: ${TABLE}.query ;;
  }

  measure: query_cost_in_usd {
    type: sum
    sql: ${TABLE}.queryCostInUSD ;;
  }

  dimension: report {
    type: string
    sql: ${TABLE}.report ;;
  }

  measure: tb {
    type: sum
    sql: ${TABLE}.tb ;;
  }

  dimension_group: timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.timestamp ;;
  }

  measure: total_billed_bytes {
    type: sum
    sql: ${TABLE}.totalBilledBytes ;;
  }

  measure: total_processed_bytes {
    type: sum
    sql: ${TABLE}.totalProcessedBytes ;;
  }

  dimension: user {
    type: string
    sql: ${TABLE}.user ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
