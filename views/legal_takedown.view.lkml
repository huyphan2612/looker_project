view: legal_takedown {
  sql_table_name: `pops-204909.Google_Spreadsheet.Legal_Takedown`
    ;;

  dimension: action {
    type: string
    sql: ${TABLE}.action ;;
  }

  dimension_group: assignement {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.assignement_date ;;
  }

  dimension: week_status {
    type: string
    sql: if(date_diff(current_date(), ${TABLE}.first_approach_advice_connection, week) = 0, 'This Week',
       if (date_diff(current_date(),   ${TABLE}.first_approach_advice_connection, week) = 1, 'Last Week', 'NA'));;
  }

  dimension_group: first_approach_advice_connection {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.first_approach_advice_connection ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: note {
    type: string
    sql: ${TABLE}.note ;;
  }

  dimension: pic_id {
    type: string
    sql: ${TABLE}.pic_id ;;
  }

  dimension: references {
    type: string
    sql: ${TABLE}.references ;;
  }

  dimension_group: second_approach_advice_connection {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.second_approach_advice_connection ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension_group: third_approach_advice_connection {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.third_approach_advice_connection ;;
  }

  dimension: violated_contents {
    type: string
    sql: ${TABLE}.violated_contents ;;
  }

  dimension: violated_party {
    type: string
    sql: ${TABLE}.violated_party ;;
  }

  measure: count {
    type: count
    drill_fields: [name]
  }
}
