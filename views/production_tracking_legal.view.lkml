view: production_tracking_legal {
  sql_table_name: `pops-204909.Google_Spreadsheet.Production_Tracking_Legal`
    ;;

  dimension_group: assignment {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.assignment_date ;;
  }

  dimension: content {
    type: string
    sql: ${TABLE}.content ;;
  }

  dimension_group: deadline {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.deadline ;;
  }

  dimension_group: delivery_date_1st {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.delivery_date_1st ;;
  }

  dimension_group: delivery_date_2nd {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.delivery_date_2nd ;;
  }

  dimension_group: delivery_date_3rd {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.delivery_date_3rd ;;
  }

  dimension: department {
    type: string
    sql: ${TABLE}.department ;;
  }

  dimension_group: inserted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.inserted_time ;;
  }

  dimension: language {
    type: string
    sql: ${TABLE}.language ;;
  }

  dimension: notes {
    type: string
    sql: ${TABLE}.notes ;;
  }

  dimension: partners {
    type: string
    sql: ${TABLE}.partners ;;
  }

  dimension: pic_employee_id {
    type: string
    sql: ${TABLE}.pic_employee_id ;;
  }

  dimension: pic_employee_name {
    type: string
    sql: ${TABLE}.pic_employee_name ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: status_adj {
    type: string
    sql: if  (regexp_contains(lower(${TABLE}.status), 'done'), 'Done',
           if (regexp_contains(lower(${TABLE}.status), 'reviewed'), 'Reviewed',
             if (regexp_contains(lower(${TABLE}.status), 'signed'), 'Signed',
               if (regexp_contains(lower(${TABLE}.status), 'cancelled'), 'Cancelled',
                 if (regexp_contains(lower(${TABLE}.status), 'pending'), 'Pending',
                   if (regexp_contains(lower(${TABLE}.status), 'complied'), 'Complied',
                     if (regexp_contains(lower(${TABLE}.status), 'not complied'), 'Not Complied',
                       if (regexp_contains(lower(${TABLE}.status), 'waiting for feedback'), 'Waiting for feedback','Uncategorized')
                      )))))));;
  }

  dimension: task_code {
    type: string
    sql: ${TABLE}.task_code ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension_group: delivery_date_adj {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: COALESCE(${TABLE}.delivery_date_3rd, ${TABLE}.delivery_date_2nd, ${TABLE}.delivery_date_1st, ${TABLE}.assignment_date);;
  }

  dimension: week_status {
   type: string
   sql: if(date_diff(current_date(), ${delivery_date_adj_date}, week) = 0, 'This Week',
       if (date_diff(current_date(),  ${delivery_date_adj_date}, week) = 1
       and regexp_contains(lower(${TABLE}.status), 'reviewed|not complied|pending'), 'Last Week', 'NA'));;
  }


  measure: count {
    type: count
    drill_fields: [pic_employee_name]
  }
}
