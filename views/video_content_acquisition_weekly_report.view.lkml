view: video_content_acquisition_weekly_report {
  sql_table_name: `pops-204909.MTHD.Video_Content_Acquisition_Weekly_Report`
    ;;

  dimension: app_install {
    type: number
    sql: ${TABLE}.app_install ;;
  }

  dimension: app_released_videos {
    type: number
    sql: ${TABLE}.app_released_videos ;;
  }

  dimension: app_revenue {
    type: number
    sql: ${TABLE}.app_revenue ;;
  }

  dimension: app_views {
    type: number
    sql: ${TABLE}.app_views ;;
  }

  dimension: contract_label {
    type: string
    sql: ${TABLE}.contract_label ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: duration_per_ep_mins {
    type: number
    sql: ${TABLE}.duration_per_ep_mins ;;
  }

  dimension: exclusity {
    type: string
    sql: ${TABLE}.exclusity ;;
  }

  dimension_group: file_delivery_expected {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.file_delivery_expected_date ;;
  }

  dimension: frequency_per_week {
    type: number
    sql: ${TABLE}.frequency_per_week ;;
  }

  dimension_group: inserted {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.inserted_date ;;
  }

  dimension: last_week_status {
    type: string
    sql: ${TABLE}.last_week_status ;;
  }

  dimension: license_fee {
    type: number
    sql: ${TABLE}.license_fee ;;
  }

  dimension: license_rights {
    type: string
    sql: ${TABLE}.license_rights ;;
  }

  dimension: licensed_platforms_popsapp {
    type: string
    sql: ${TABLE}.licensed_platforms_popsapp ;;
  }

  dimension: licensed_platforms_popskids {
    type: string
    sql: ${TABLE}.licensed_platforms_popskids ;;
  }

  dimension: licensed_platforms_youtube {
    type: string
    sql: ${TABLE}.licensed_platforms_youtube ;;
  }

  dimension: localization_cost {
    type: number
    sql: ${TABLE}.localization_cost ;;
  }

  dimension_group: lp_expiry {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.lp_expiry_date ;;
  }

  dimension_group: lp_start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.lp_start_date ;;
  }

  dimension: material_fee {
    type: number
    sql: ${TABLE}.material_fee ;;
  }

  dimension: mg {
    type: number
    sql: ${TABLE}.mg ;;
  }

  dimension: name_of_airing_channel_pops_platform {
    type: string
    sql: ${TABLE}.name_of_airing_channel_pops_platform ;;
  }

  dimension: name_of_partner_on_contract {
    type: string
    sql: ${TABLE}.name_of_partner_on_contract ;;
  }

  dimension: new_titles {
    type: string
    sql: ${TABLE}.new_titles ;;
  }

  dimension: non_exclusive {
    type: string
    sql: ${TABLE}.non_exclusive ;;
  }

  dimension: number_of_eps {
    type: number
    sql: ${TABLE}.number_of_eps ;;
  }

  dimension: number_of_eps_received {
    type: number
    sql: ${TABLE}.number_of_eps_received ;;
  }

  dimension: number_of_eps_released {
    type: number
    sql: ${TABLE}.number_of_eps_released ;;
  }

  dimension: partner {
    type: string
    sql: ${TABLE}.partner ;;
  }

  dimension: payment_period {
    type: string
    sql: ${TABLE}.payment_period ;;
  }

  dimension: payment_status {
    type: string
    sql: ${TABLE}.payment_status ;;
  }

  dimension: pnl_status {
    type: string
    sql: ${TABLE}.pnl_status ;;
  }

  dimension: project_id {
    type: string
    sql: ${TABLE}.project_id ;;
  }

  dimension: project_name {
    type: string
    sql: ${TABLE}.project_name ;;
  }

  dimension: ranking {
    type: string
    sql: ${TABLE}.ranking ;;
  }

  dimension: report {
    type: string
    sql: ${TABLE}.report ;;
  }

  dimension: revenue_share {
    type: string
    sql: ${TABLE}.revenue_share ;;
  }

  dimension: season {
    type: number
    sql: ${TABLE}.season ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension_group: tentative_airing {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.tentative_airing_date ;;
  }

  dimension: term_of_contract {
    type: string
    sql: ${TABLE}.term_of_contract ;;
  }

  dimension: territory {
    type: string
    sql: ${TABLE}.territory ;;
  }

  dimension: this_week_status {
    type: string
    sql: ${TABLE}.this_week_status ;;
  }

  dimension: type_of_content {
    type: string
    sql: ${TABLE}.type_of_content ;;
  }

  dimension: type_of_localization {
    type: string
    sql: ${TABLE}.type_of_localization ;;
  }

  dimension: video_original_language {
    type: string
    sql: ${TABLE}.video_original_language ;;
  }

  dimension: video_target_language {
    type: string
    sql: ${TABLE}.video_target_language ;;
  }

  dimension: youtube_revenue {
    type: number
    sql: ${TABLE}.youtube_revenue ;;
  }

  dimension: youtube_views {
    type: number
    sql: ${TABLE}.youtube_views ;;
  }

  dimension: yt_released_videos {
    type: number
    sql: ${TABLE}.yt_released_videos ;;
  }

   dimension: week_status {
    type: string
    sql: if(date_diff(current_date(), ${TABLE}.inserted_date, week) = 0, 'This Week',
      if (date_diff(current_date(),   ${TABLE}.inserted_date, week) = 1, 'Last Week', 'NA'));;
  }


  measure: number_of_contracts {
    type: count_distinct
    sql: ${TABLE}.contract_label;;}

  measure: number_of_titles {
    type: count_distinct
    sql: ${TABLE}.project_name;;}

  measure: count {
    type: count
    drill_fields: [project_name]
  }
}
