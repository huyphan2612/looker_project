view: pubsub_schema{

  derived_table: {
    sql: SELECT * FROM `pops-204909.PubSub.INFORMATION_SCHEMA.COLUMNS` ;;

  }

  dimension: table_catalog {
    type: string
    sql: ${TABLE}.table_catalog ;;
  }

  dimension: table_schema {
    type: string
    sql: ${TABLE}.table_schema ;;
  }

  dimension: table_name {
    type: string
    sql: ${TABLE}.table_name ;;
  }

  dimension: column_name {
    type: string
    sql: ${TABLE}.column_name ;;
  }

  dimension: ordinal_position  {
    type: number
    sql: ${TABLE}.ordinal_position   ;;
  }



  measure: count {
    type: count

  }
}
