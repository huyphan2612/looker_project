view: playlist_interaction {
  sql_table_name: `pops-204909.NHNhat.TEST_POP_DATAMART_select_item_in_playlist`
    ;;

  dimension: app_info_version {
    type: string
    sql: ${TABLE}.app_info_version ;;
  }

  dimension: position_in_playlist {
    type: number
    sql: CAST(${TABLE}.column_index AS INT64) ;;
  }

  dimension_group: event {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.event_date ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}.event_name ;;
  }

  dimension_group: event_timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.event_timestamp ;;
  }

  dimension: is_item_selected {
    type: number
    sql: ${TABLE}.is_item_selected ;;
  }

  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: row_index {
    type: number
    sql: ${TABLE}.row_index ;;
  }

  dimension: session_id {
    type: string
    sql: ${TABLE}.session_id ;;
  }

  dimension: source_id {
    type: string
    sql: ${TABLE}.source_id ;;
  }

  dimension: source_page {
    type: string
    sql: ${TABLE}.source_page ;;
  }

  dimension: source_title {
    type: string
    sql: ${TABLE}.source_title ;;
  }

  dimension: source_type {
    type: string
    sql: ${TABLE}.source_type ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  dimension: geo_country {
    type: string
    sql: ${TABLE}.geo_country ;;
  }

  measure: count {
    type: number
    drill_fields: [event_name]
  }

  measure: number_of_records {
    type: number
    sql:  COUNT(*) ;;
  }
}
