view: star_statistic_unlocking_bundle {
  sql_table_name: `pops-204909.production.Star_Statistic_Unlocking_Bundle`
    ;;

  dimension: activity_id {
    type: string
    sql: ${TABLE}.activity_id ;;
  }

  dimension: activity_name {
    type: string
    sql: ${TABLE}.activity_name ;;
  }

  dimension: content_id {
    type: string
    sql: ${TABLE}.content_id ;;
  }

  dimension: content_name {
    type: string
    sql: ${TABLE}.content_name ;;
  }

  dimension: content_type {
    type: string
    sql: ${TABLE}.content_type ;;
  }

  dimension_group: date_unlock {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: TIMESTAMP_ADD(${TABLE}.date_unlock, INTERVAL 7 HOUR) ;;
  }

  dimension_group: file {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.file_date ;;
  }

  dimension: location {
    type: string
    sql: ${TABLE}.location ;;
  }

  measure: no_of_stars {
    type: sum
    sql: ${TABLE}.no_of_stars ;;
  }

  dimension: session_id {
    type: string
    sql: ${TABLE}.session_id ;;
  }

  dimension: user_email {
    type: string
    sql: ${TABLE}.user_email ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  measure: transactions {
    type: count_distinct
    sql: ${TABLE}.activity_id ;;
  }

  measure: buyers {
    type: count_distinct
    sql: ${TABLE}.user_id ;;
  }

  measure: max_timestamp {
    type: string
    sql: MAX(FORMAT_TIMESTAMP("%D-%H", TIMESTAMP_ADD(${TABLE}.date_unlock, INTERVAL 7 HOUR))) ;;
  }

  measure: count {
    type: count
    drill_fields: [content_name, activity_name]
  }
}
