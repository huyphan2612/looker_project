view: pop_dim_bundles {
  sql_table_name: `pops-bi.Self_Service.POP_DIM_Bundles`
    ;;

  dimension: bundle_group {
    type: string
    sql: ${TABLE}.bundle_group ;;
  }

  dimension: bundle_id {
    type: string
    sql: ${TABLE}.bundle_id ;;
  }

  dimension: bundle_name {
    type: string
    sql: ${TABLE}.bundle_name ;;
  }

  dimension: stars {
    type: number
    sql: ${TABLE}.stars ;;
  }

  measure: count {
    type: count
    drill_fields: [bundle_name]
  }
}
