view: stars_transaction {
  sql_table_name: `pops-bi.Vietnam_Comic.POP_Scheduled_DATAMART_Stars_Transaction`
    ;;

  dimension_group: activity {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.activity_date ;;
  }

  dimension_group: activity_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.activity_datetime ;;
  }

  dimension: activity_id {
    type: string
    sql: ${TABLE}.activity_id ;;
  }

  dimension: project_name {
    type:  string
    sql: ${metatdata.project_name} ;;
  }

  dimension: bundle_name {
    type:  string
    sql: ${bundle_dim.bundle} ;;
  }

  dimension: product_group_name  {
    type: string
    sql: COALESCE(${bundle_name}, ${project_name});;

  }

  dimension: activity_name {
    type: string
    sql: ${TABLE}.activity_name ;;
  }

  dimension: app_region {
    type: string
    sql: ${TABLE}.app_region ;;
  }

  measure: estimated_transaction_amount {
    type: sum
    drill_fields: [detail*]
    value_format: "0.00"
    sql: ${TABLE}.estimated_transaction_amount ;;
  }

  set: detail {
    fields: [activity_id, activity_date,  app_region, activity_name, user_id, product_id, product_name, product_group_name, no_of_stars, estimated_transaction_amount]
  }
  measure: no_of_stars {
    type: sum
    drill_fields: [detail*]
    sql: ${TABLE}.no_of_stars
    ;;
  }

  dimension: payment_method {
    type: string
    sql: ${TABLE}.payment_method ;;
  }

  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: product_id {
    type: string
    sql: ${TABLE}.product_id ;;
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.product_name ;;
  }

  dimension_group: register {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.register_date ;;
  }

  dimension: session_id {
    type: string
    sql: ${TABLE}.session_id ;;
  }

  dimension: user_email {
    type: string
    sql: ${TABLE}.user_email ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }
}
