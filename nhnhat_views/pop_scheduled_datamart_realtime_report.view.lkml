view: pubsub_RT {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_Realtime_Report`
    ;;

  dimension: active_days {
    type: number
    sql: ${TABLE}.active_days ;;
  }

  dimension: app_name {
    type: string
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    sql: ${TABLE}.app_region ;;
  }

  dimension: content_category {
    type: string
    sql: ${TABLE}.content_category ;;
  }

  dimension: content_name {
    type: string
    sql: ${TABLE}.content_name ;;
  }

  dimension: content_topic {
    type: string
    sql: ${TABLE}.content_topic ;;
  }

  dimension_group: datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.datetime ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}.event_name ;;
  }

  dimension: new_user {
    type: string
    sql: ${TABLE}.new_user ;;
  }

  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: platform_detail {
    type: string
    sql: ${TABLE}.platform_detail ;;
  }

  dimension_group: session_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.session_datetime ;;
  }

  dimension_group: session_timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      hour,
      week,
      month,
      quarter,
      year
    ]
    sql: TIMESTAMP(${TABLE}.session_datetime) ;;
  }

  dimension: session_id {
    type: string
    sql: ${TABLE}.session_id ;;
  }

  dimension: session_load_id {
    type: string
    sql: ${TABLE}.session_load_id ;;
  }




  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  dimension: video_id {
    type: string
    sql: ${TABLE}.video_id ;;
  }

  dimension_group: video_load_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.video_load_datetime ;;
  }

  dimension: video_load_id {
    type: string
    sql: ${TABLE}.video_load_id ;;
  }

  dimension: video_title {
    type: string
    sql: ${TABLE}.video_title ;;
  }

  dimension: viewer_id {
    type: string
    sql: ${TABLE}.viewer_id ;;
  }

  dimension: web_platform {
    type: string
    sql: ${TABLE}.web_platform ;;
  }

  measure: users {
    type: number
    sql: COUNT(DISTINCT ${user_id}) ;;
  }

  measure: viewers {
    type: number
    sql: COUNT(DISTINCT IF(${event_name} <> 'session_load', ${user_id}, null)) ;;
  }

  measure: conversion_rate {
    type: number
    sql: IF (${users} = 0, 0 ,${viewers} / ${users}) ;;
  }

  measure: count {
    type: count
    drill_fields: [content_name, app_name, event_name]
  }
}
