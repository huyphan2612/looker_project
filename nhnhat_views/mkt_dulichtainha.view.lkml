view: mkt_dulichtainha {
  # # You can specify the table name if it's different from the view name:
  derived_table: {
    sql: with playlist_user as
            (select
                  distinct c.user_id, a.entity_id , True as is_from_playlist
            from `pops-204909.PubSubv1.User_View`  a
            left join `pops-204909.PubSub.Video_Log_Session_Start` b on a.session_id = b.session_id
            left join `pops-204909.MongoDB.Profile` c on b.profile_id = c.profile_id
            where event_date >= '2021-08-27' and b.session_date >= '2021-08-27'
            and (regexp_contains(l_screen, '/album/du-lich') or regexp_contains(l_screen, 'comics/playlist/du-lich-'))
            and a.entity_id <> 'smart_banner')

            , video_load as (

              select

                 z.user_id ,
                 quantity ,
                 d.album_id,
                 type,
                 album_name,
                 p.is_from_playlist,
                 min(z.date) as start_date,
                 max(z.date) as end_date,
                 count(distinct if ( watch_time_seconds >= 5, content_id, null )) as items
              from `pops-bi.Self_Service.POP_Scheduled_DATAMART_Both_App_Performance` z
              left join `MongoDB.PopsApp_Playlist_Items` a on z.content_id = a.video_id
              left join `pops-204909.MongoDB.PopsApp_Topic_Season` b on a.playlist_id = b.seasons_data_id
              left join `pops-204909.MongoDB.PopsApp_Topic` c on c.topic_id = b.topic_id
              inner join (select *  from `pops-204909.NHNhat.MKT_dulichvietnam_master`) d on d.content_name_id = c.topic_id
              left join playlist_user p on z.user_id = p.user_id and d.album_id = p.entity_id
          --     left join new_viewer d on d.user_id = z.user_id
              where date >= '2021-08-27'
              and z.app_region = 'VN' and z.app_name = 'POPS App' and z.event_name = 'video_load'
              group by z.user_id, quantity, d.album_id, type , album_name, p.is_from_playlist

              )
             ,

             chapter_load as (

              select

                 z.user_id ,
                 quantity ,
                 d.album_id,
                 type,
                 album_name,
                 p.is_from_playlist,
                 min(z.date) as start_date,
                 max(z.date) as end_date,
                 count(distinct if ( ch.comic_read_time >= 5, content_id, null )) as items
              from `pops-bi.Self_Service.POP_Scheduled_DATAMART_Both_App_Performance` z
              left join `pops-204909.PubSubv1.Chapter_Load_RT` ch on ch.chapter_load_id = z.load_id
              inner join (select *  from `pops-204909.NHNhat.MKT_dulichvietnam_master`) d on d.content_name_id = z.content_name_id
              left join playlist_user p on z.user_id = p.user_id and d.album_id = p.entity_id
          --     left join new_viewer d on d.user_id = z.user_id
              where date >= '2021-08-27' and ch.event_date >= '2021-08-27'
              and z.app_region = 'VN' and z.app_name = 'POPS App' and z.event_name = 'chapter_load'
              group by z.user_id, quantity, d.album_id, type , album_name, p.is_from_playlist)

              select * from video_load where items >= quantity
              union all
              select * from chapter_load where items >= quantity;;

      sql_trigger_value: SELECT COUNT(*) FROM `pops-bi.Self_Service.POP_Scheduled_DATAMART_Both_App_Performance` ;;
        }

  dimension: user_id {
    # description: "Unique ID for each user that has ordered"
    type: string
    sql: ${TABLE}.user_id ;;
  }

  dimension: album_id {
    # description: "Unique ID for each user that has ordered"
    type: string
    sql: ${TABLE}.album_id ;;
  }

  dimension: album_name {
    # description: "Unique ID for each user that has ordered"
    type: string
    sql: ${TABLE}.album_name ;;
  }

  dimension: type {
    # description: "Unique ID for each user that has ordered"
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension: quantity {
    # description: "Unique ID for each user that has ordered"
    type: number
    sql: ${TABLE}.quantity ;;
  }

  dimension: is_from_playlist {
    # description: "Unique ID for each user that has ordered"
    type: yesno
    sql: ${TABLE}.is_from_playlist ;;
  }


  dimension: items {
    # description: "Unique ID for each user that has ordered"
    type: number
    sql: ${TABLE}.items ;;
  }

  dimension_group: min {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start_date ;;
  }

  dimension_group: max {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.end_date ;;
}

}
