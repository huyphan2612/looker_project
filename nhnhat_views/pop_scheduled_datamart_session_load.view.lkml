view: session_load {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_Session_Load`
    ;;

  dimension: app_name {
    type: string
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    sql: ${TABLE}.app_region ;;
  }

  dimension: content_id {
    type: string
    sql: ${TABLE}.content_id ;;
  }

  dimension: content_name {
    type: string
    sql: ${TABLE}.content_name ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension_group: datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.datetime ;;
  }

  dimension: device_type {
    type: string
    sql: ${TABLE}.device_type ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}.event_name ;;
  }

  dimension_group: load {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.load_date ;;
  }

  dimension_group: load_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.load_datetime ;;
  }

  dimension: load_id {
    type: string
    sql: ${TABLE}.load_id ;;
  }

  dimension_group: load_month {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.load_month ;;
  }

  dimension_group: load_quarter {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.load_quarter ;;
  }

  dimension_group: load_week {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.load_week ;;
  }

  dimension: max_player_location {
    type: number
    sql: ${TABLE}.max_player_location ;;
  }

  dimension_group: min_ping_timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.min_ping_timestamp ;;
  }

  dimension: min_player_location {
    type: number
    sql: ${TABLE}.min_player_location ;;
  }

  dimension_group: month {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.month ;;
  }

  dimension: object_id {
    type: string
    sql: ${TABLE}.object_id ;;
  }

  dimension: object_length_minutes {
    type: number
    sql: ${TABLE}.object_length_minutes ;;
  }

  dimension: object_length_seconds {
    type: number
    sql: ${TABLE}.object_length_seconds ;;
  }

  dimension: object_title {
    type: string
    sql: ${TABLE}.object_title ;;
  }

  dimension: object_type {
    type: string
    sql: ${TABLE}.object_type ;;
  }

  dimension: os {
    type: string
    sql: ${TABLE}.os ;;
  }

  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: platform_detail {
    type: string
    sql: ${TABLE}.platform_detail ;;
  }

  dimension: profile_id {
    type: string
    sql: ${TABLE}.profile_id ;;
  }

  dimension_group: quarter {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.quarter ;;
  }

  dimension_group: register {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.register_date ;;
  }

  dimension_group: register_month {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.register_month ;;
  }

  dimension_group: register_quarter {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.register_quarter ;;
  }

  dimension_group: register_week {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.register_week ;;
  }

  dimension_group: session {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.session_date ;;
  }

  dimension_group: session_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.session_datetime ;;
  }

  dimension: session_id {
    type: string
    sql: ${TABLE}.session_id ;;
  }

  dimension_group: session_month {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.session_month ;;
  }

  dimension_group: session_quarter {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.session_quarter ;;
  }

  dimension_group: session_week {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.session_week ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  dimension: valid_pings {
    type: number
    sql: ${TABLE}.valid_pings ;;
  }

  dimension: view5s_load_id {
    type: string
    sql: ${TABLE}.view5s_load_id ;;
  }

  dimension: viewer_id {
    type: string
    sql: ${TABLE}.viewer_id ;;
  }

  measure: views {
    type: sum
    sql: ${TABLE}.views ;;
  }

  measure: views_5s {
    type: sum
    sql: ${TABLE}.views_5s ;;
  }

  measure: watch_time_minutes {
    type: sum
    sql: ${TABLE}.watch_time_minutes ;;
  }

  measure: watch_time_minutes_5s {
    type: sum
    sql: ${TABLE}.watch_time_minutes_5s ;;
  }

  measure: watch_time_seconds {
    type: sum
    sql: ${TABLE}.watch_time_seconds ;;
  }

  measure: user_count{
    type: count_distinct
    sql: ${TABLE}.user_id ;;
  }

  measure: viewer_count{
    type: count_distinct
    sql: ${TABLE}.viewer_id ;;
  }

  measure: view_count{
    type: count_distinct
    sql: ${TABLE}.load_id ;;
  }

  measure: view5s_count{
    type: count_distinct
    sql: ${TABLE}.view5s_load_id ;;
  }

  dimension_group: week {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.week ;;
  }

  dimension: week_period {
    type: string
    sql: ${TABLE}.week_period ;;
  }

  measure: count {
    type: count
    drill_fields: [content_name, event_name, app_name]
  }
}
