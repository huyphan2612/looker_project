view: view_page {
  sql_table_name: `pops-204909.NHNhat.product_firebase_view_page`
    ;;

  dimension: app_info_version {
    type: string
    sql: ${TABLE}.app_info_version ;;
  }

  dimension: app_name {
    type: string
    sql: ${TABLE}.app_name ;;
  }

  dimension: content_regional {
    type: string
    sql: ${TABLE}.content_regional ;;
  }

  dimension: device_category {
    type: string
    sql: ${TABLE}.device_category ;;
  }

  dimension: device_id {
    type: string
    sql: ${TABLE}.device_id ;;
  }

  dimension: device_mobile_brand_name {
    type: string
    sql: ${TABLE}.device_mobile_brand_name ;;
  }

  dimension: device_mobile_marketing_name {
    type: string
    sql: ${TABLE}.device_mobile_marketing_name ;;
  }

  dimension: device_mobile_os_hardware_model {
    type: string
    sql: ${TABLE}.device_mobile_os_hardware_model ;;
  }

  dimension: device_operating_system {
    type: string
    sql: ${TABLE}.device_operating_system ;;
  }

  dimension: device_type {
    type: string
    sql: ${TABLE}.device_type ;;
  }

  dimension: device_web_info_browser {
    type: string
    sql: ${TABLE}.device_web_info_browser ;;
  }

  dimension: device_web_info_browser_version {
    type: string
    sql: ${TABLE}.device_web_info_browser_version ;;
  }

  dimension: device_web_info_hostname {
    type: string
    sql: ${TABLE}.device_web_info_hostname ;;
  }

  dimension_group: event {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.event_date ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}.event_name ;;
  }

  dimension_group: event_timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.event_timestamp ;;
  }

  dimension: ga_session_id {
    type: string
    sql: ${TABLE}.ga_session_id ;;
  }

  dimension: geo_city {
    type: string
    sql: ${TABLE}.geo_city ;;
  }

  dimension: geo_continent {
    type: string
    sql: ${TABLE}.geo_continent ;;
  }

  dimension: geo_country {
    type: string
    sql: ${TABLE}.geo_country ;;
  }

  dimension: geo_metro {
    type: string
    sql: ${TABLE}.geo_metro ;;
  }

  dimension: geo_region {
    type: string
    sql: ${TABLE}.geo_region ;;
  }

  dimension: ip {
    type: string
    sql: ${TABLE}.ip ;;
  }

  dimension: next_screen {
    type: string
    sql: ${TABLE}.next_screen ;;
  }

  dimension: page_code {
    type: string
    sql: ${TABLE}.page_code ;;
  }

  dimension: page_location {
    type: string
    sql: ${TABLE}.page_location ;;
  }

  dimension: page_referrer {
    type: string
    sql: ${TABLE}.page_referrer ;;
  }

  dimension: page_title {
    type: string
    sql: ${TABLE}.page_title ;;
  }

  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: previous_screen {
    type: string
    sql: ${TABLE}.previous_screen ;;
  }

  dimension: profile_session_id {
    type: string
    sql: ${TABLE}.profile_session_id ;;
  }

  dimension: screen_identifier {
    type: string
    sql: ${TABLE}.screen_identifier ;;
  }

  dimension: traffic_source_medium {
    type: string
    sql: ${TABLE}.traffic_source_medium ;;
  }

  dimension: traffic_source_name {
    type: string
    sql: ${TABLE}.traffic_source_name ;;
  }

  dimension: traffic_source_source {
    type: string
    sql: ${TABLE}.traffic_source_source ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  dimension: user_pseudo_id {
    type: string
    sql: ${TABLE}.user_pseudo_id ;;
  }

  dimension: video_id {
    type: string
    sql: ${TABLE}.video_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: user_count{
    type: count_distinct
    sql:  ${TABLE}.user_pseudo_id ;;
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      device_web_info_hostname,
      app_name,
      event_name,
      traffic_source_name,
      device_mobile_brand_name,
      device_mobile_marketing_name
    ]
  }
}
