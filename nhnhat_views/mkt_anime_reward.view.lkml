view: mkt_anime_reward {
  derived_table: {

    sql: select
               min(date) as first_date,
               z.user_id ,
               u.email,
               coalesce(p.phone, u.phone) as phone
        from `pops-bi.Self_Service.POP_Scheduled_DATAMART_Both_App_Performance` z
        left join `pops-204909.MongoDB.User` u on z.user_id = u.user_id
        left join `pops-204909.MongoDB.UserPhone` p on z.user_id = p.user_id
        left join `MongoDB.PopsApp_Playlist_Items` a on z.content_id = a.video_id
        left join `pops-204909.MongoDB.PopsApp_Topic_Season` b on a.playlist_id = b.seasons_data_id
        left join `pops-204909.MongoDB.PopsApp_Topic` c on c.topic_id = b.topic_id
        where date between '2021-07-23' and '2021-07-31'
        and app_region = 'VN' and app_name = 'POPS App' and event_name = 'video_load'
        and c.topic_id in ('5fc5f17fca8af00034b846ae', '601cff86396ebd0035478837', '5e857135574ebb00334427a2', '5fc711faca8af00034b8477a', '5f48e63e42243d00353e7c8a')
        group by z.user_id, u.email, phone  ;;

    partition_keys: ["first_date"]
    sql_trigger_value: SELECT COUNT(*) FROM `pops-bi.Self_Service.POP_Scheduled_DATAMART_Both_App_Performance` ;;
  }


  dimension_group: date_added {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.first_date ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  dimension: phone {
    type: string
    sql: ${TABLE}.phone ;;
  }


  measure: count {
    type: count
    drill_fields: []
  }
}
