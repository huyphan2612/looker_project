view: video_log_video_load_wt {
  sql_table_name: `pops-204909.PubSub.Video_Log_Video_Load_WT`
    ;;

  dimension: content_exposure_seconds {
    type: number
    sql: ${TABLE}.content_exposure_seconds ;;
  }

  dimension: event_start_sec {
    type: number
    sql: ${TABLE}.event_start_sec ;;
  }

  dimension_group: event_timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.event_timestamp ;;
  }

  dimension_group: load {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.load_date ;;
  }

  dimension: max_player_location {
    type: number
    sql: ${TABLE}.max_player_location ;;
  }

  dimension_group: min_ping_event_timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.min_ping_event_timestamp ;;
  }

  dimension: min_player_location {
    type: number
    sql: ${TABLE}.min_player_location ;;
  }

  dimension: number_of_ads {
    type: number
    sql: ${TABLE}.number_of_ads ;;
  }

  dimension: rec_id {
    type: string
    sql: ${TABLE}.rec_id ;;
  }

  dimension: session_id {
    type: string
    sql: ${TABLE}.session_id ;;
  }

  dimension: valid_pings_count {
    type: number
    sql: ${TABLE}.valid_pings_count ;;
  }

  dimension: video_id {
    type: string
    sql: ${TABLE}.video_id ;;
  }

  dimension: video_index {
    type: number
    sql: ${TABLE}.video_index ;;
  }

  dimension: video_load_id {
    type: string
    sql: ${TABLE}.video_load_id ;;
  }

  measure: views {
    type: count_distinct
    sql: ${TABLE}.video_load_id ;;
  }

  measure: watch_time_seconds {
    type: sum
    sql: ${TABLE}.watch_time_seconds ;;
  }

  measure: watch_time_hour {
    type: sum
    sql: ${TABLE}.watch_time_seconds / 3600 ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
