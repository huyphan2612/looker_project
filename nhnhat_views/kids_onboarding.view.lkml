view: kids_onboarding {
  # # You can specify the table name if it's different from the view name:
  derived_table: {
    sql: with A as (
            select
                  distinct
                  coalesce(a.user_id , b.user_id) as user_id,
                  a.session_id ,
                  entity_id ,
                  a.event_timestamp ,
                  'x' as  object_id,
                  'view_page' as action_type


            from `pops-204909.PubSubv1.User_View`  a
            left join `pops-204909.PubSubv1.Session_Start` b on a.session_id = b.session_id
            where a.event_date  >= '2021-08-24' --and b.event_date >= '2021-08-24'
            and a.ap_name = 'POPS Kids'
            and a.platform in ('IOS', 'ANDROID')
            and entity_id in ( 'kids_onboarding', 'age_category', 'parental') )

            , B as (
            select
                  distinct
                  coalesce(a.user_id , b.user_id) as user_id,
                  a.session_id ,
                  entity_id ,
                  a.event_timestamp ,
                  'x' as  object_id,
                  'view_page' as action_type


            from `pops-204909.PubSubv1.User_View`  a
            left join `pops-204909.PubSubv1.Session_Start` b on a.session_id = b.session_id
            where a.event_date  >= '2021-08-24' --and b.event_date >= '2021-08-24'
            and a.ap_name = 'POPS Kids'
            and a.platform in ('IOS', 'ANDROID')
            and entity_id in ( 'parental')
            and a.session_id in (select session_id  from A))

            , C as (

            select
                  distinct
                  user_id ,
                  session_id ,
                  entity_id ,
                  event_timestamp ,
                  object_id ,
                  action_type ,

            from `pops-204909.PubSubv1.User_Interact`
            where event_date  >= '2021-08-24'
            and ap_name = 'POPS Kids'
            and platform in ('IOS', 'ANDROID')
            and entity_id in ('parental', 'kids_onboarding', 'age_category') )



            select
                    entity_id ,
                    object_id ,
                    action_type ,
                    min(event_timestamp ) as min_time,
                    max(event_timestamp ) as max_time,
                    count(distinct user_id ) as users,
                    count(distinct session_id ) as sessions

            from (select * from A union all select * from B union all select * from C)

            group by  entity_id ,
                    object_id ,
                    action_type  ;;
    sql_trigger_value: SELECT COUNT(*) FROM `pops-204909.PubSubv1.User_View`  ;;
  }
  #
  # # Define your dimensions and measures here, like this:
  dimension: entity_id {
    # description: "Unique ID for each user that has ordered"
    type: string
    sql: ${TABLE}.entity_id ;;
  }

  dimension: object_id {
    # description: "Unique ID for each user that has ordered"
    type: string
    sql: ${TABLE}.object_id ;;
  }

  dimension: action_type {
    # description: "Unique ID for each user that has ordered"
    type: string
    sql: ${TABLE}.action_type ;;

  }

  measure: users {
    # description: "Unique ID for each user that has ordered"
    type: sum
    sql: ${TABLE}.users ;;

  }

  dimension_group: min {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.min_time ;;
  }

  dimension_group: max {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.max_time ;;
  }


  #
  # dimension: lifetime_orders {
  #   description: "The total number of orders for each user"
  #   type: number
  #   sql: ${TABLE}.lifetime_orders ;;
  # }
  #
  # dimension_group: most_recent_purchase {
  #   description: "The date when each user last ordered"
  #   type: time
  #   timeframes: [date, week, month, year]
  #   sql: ${TABLE}.most_recent_purchase_at ;;
  # }
  #
  # measure: total_lifetime_orders {
  #   description: "Use this for counting lifetime orders across many users"
  #   type: sum
  #   sql: ${lifetime_orders} ;;
  # }
}

# view: kids_onboarding {
#   # Or, you could make this view a derived table, like this:
#   derived_table: {
#     sql: SELECT
#         user_id as user_id
#         , COUNT(*) as lifetime_orders
#         , MAX(orders.created_at) as most_recent_purchase_at
#       FROM orders
#       GROUP BY user_id
#       ;;
#   }
#
#   # Define your dimensions and measures here, like this:
#   dimension: user_id {
#     description: "Unique ID for each user that has ordered"
#     type: number
#     sql: ${TABLE}.user_id ;;
#   }
#
#   dimension: lifetime_orders {
#     description: "The total number of orders for each user"
#     type: number
#     sql: ${TABLE}.lifetime_orders ;;
#   }
#
#   dimension_group: most_recent_purchase {
#     description: "The date when each user last ordered"
#     type: time
#     timeframes: [date, week, month, year]
#     sql: ${TABLE}.most_recent_purchase_at ;;
#   }
#
#   measure: total_lifetime_orders {
#     description: "Use this for counting lifetime orders across many users"
#     type: sum
#     sql: ${lifetime_orders} ;;
#   }
# }
