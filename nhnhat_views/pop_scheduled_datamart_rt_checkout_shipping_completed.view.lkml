view: d2c_realtime {
  sql_table_name: `pops-bi.Vietnam_Comic.POP_Scheduled_DATAMART_RT_Checkout_Shipping_Completed_2021`
    ;;

  dimension: address {
    type: string
    sql: ${TABLE}.address ;;
  }

  dimension: bundle {
    type: string
    sql: ${TABLE}.bundle ;;
  }

  dimension: bundle_group_id {
    type: string
    sql: ${TABLE}.bundle_group_id ;;
  }

  dimension: bundle_group_name {
    type: string
    sql: ${TABLE}.bundle_group_name ;;
  }

  dimension: bundle_id {
    type: string
    sql: ${TABLE}.bundle_id ;;
  }

  dimension: c_shipping_email {
    type: string
    sql: ${TABLE}.c_shipping_email ;;
  }

  dimension: c_shipping_name {
    type: string
    sql: ${TABLE}.c_shipping_name ;;
  }

  dimension: c_shipping_phones {
    type: string
    sql: ${TABLE}.c_shipping_phones ;;
  }

  dimension: c_user_id {
    type: string
    sql: ${TABLE}.c_user_id ;;
  }

  measure: star {
    type: sum
    sql: ${TABLE}.star ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: platforms {
    type: string
    sql: ${TABLE}.platforms ;;
  }

  dimension: is_POPS {
    type: yesno
    sql: REGEXP_CONTAINS(UPPER(${TABLE}.registered_email), 'POPS') ;;
  }

  dimension_group: timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      hour,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.timestamp ;;
  }

  dimension: transaction_id {
    type: string
    sql: ${TABLE}.transaction_id ;;
  }

  measure: max_timestamp {
    type: string
    sql: MAX(FORMAT_TIMESTAMP("%D-%H", ${TABLE}.timestamp)) ;;
  }
  measure: transaction_numbers {
    type: count_distinct
    sql: ${TABLE}.transaction_id ;;
  }

  measure: users_count {
    type: count_distinct
    sql: ${TABLE}.c_user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [c_shipping_name, bundle_group_name]
  }
}
