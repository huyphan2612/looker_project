view: dim_samsung_users {

derived_table: {
  sql: select
      min(date) as first_joined_date,
      a.user_id ,
      c.device_id ,
      b.email


      from `pops-bi.Self_Service.POP_Scheduled_DATAMART_Both_App_Performance` a
      left join `pops-bi.production.DIM_Users` b on a.user_id = b.user_id
      left join `pops-204909.PubSub.Video_Log_Session_Start` c on a.session_id = c.session_id
      where a.date >= '2021-05-27' and c.session_date >= '2021-05-27'
      and a.app_region = 'VN'
      and a.app_name = 'POPS App'
      and regexp_contains(UPPER(a.installation_channel) , 'SAMSUNG')
      group by a.user_id ,
            b.email, c.device_id  ;;

  partition_keys: ["first_joined_date"]
  sql_trigger_value: SELECT COUNT(*) FROM  `pops-bi.Self_Service.POP_Scheduled_DATAMART_Both_App_Performance` ;;

}

# view: dim_samsung_users {
#   # Or, you could make this view a derived table, like this:
#   derived_table: {
#     sql: SELECT
#         user_id as user_id
#         , COUNT(*) as lifetime_orders
#         , MAX(orders.created_at) as most_recent_purchase_at
#       FROM orders
#       GROUP BY user_id
#       ;;
#   }
#
#   # Define your dimensions and measures here, like this:
#   dimension: user_id {
#     description: "Unique ID for each user that has ordered"
#     type: number
#     sql: ${TABLE}.user_id ;;
#   }
#
  dimension_group: first_joined_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.first_joined_date ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: device_id {
    type: string
    sql: ${TABLE}.device_id ;;
  }



  }
#
#   dimension_group: most_recent_purchase {
#     description: "The date when each user last ordered"
#     type: time
#     timeframes: [date, week, month, year]
#     sql: ${TABLE}.most_recent_purchase_at ;;
#   }
#
#   measure: total_lifetime_orders {
#     description: "Use this for counting lifetime orders across many users"
#     type: sum
#     sql: ${lifetime_orders} ;;
#   }
# }
