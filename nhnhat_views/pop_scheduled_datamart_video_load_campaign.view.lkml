view: view__mkt_campaign {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DATAMART_Video_Load_Campaign`
    ;;

  dimension: app_name {
    type: string
    sql: ${TABLE}.app_name ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: device_id {
    type: string
    sql: ${TABLE}.device_id ;;
  }

  dimension: device_type {
    type: string
    sql: ${TABLE}.device_type ;;
  }

  dimension_group: event_timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.event_timestamp ;;
  }

  dimension_group: load {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.load_date ;;
  }

  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: team {
    type: string
    sql: ${TABLE}.team ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  dimension: utm_campaign {
    type: string
    sql: ${TABLE}.utm_campaign ;;
  }

  dimension: utm_medium {
    type: string
    sql: ${TABLE}.utm_medium ;;
  }

  dimension: utm_source {
    type: string
    sql: ${TABLE}.utm_source ;;
  }

  dimension: video_load_id {
    type: string
    sql: ${TABLE}.video_load_id ;;
  }

  measure: watch_time_seconds{
    type: number
    sql: ${TABLE}.watch_time_seconds ;;
  }

  measure: watch_time_hours{
    type: number
    sql: SUM(${TABLE}.watch_time_seconds) / 3600 ;;
  }

  parameter: dynamic_param {
    type: unquoted
    allowed_value: {
      label: "Viewers"
      value: "viewer"
    }
    allowed_value: {
      label: "Viewer 5s"
      value: "viewer5s"
    }
    allowed_value: {
      label: "view5s"
      value: "view5s"
    }

    allowed_value: {
      label: "Watch Time Hours"
      value: "watchtime"
    }


  }

  measure: dynamic_metric {
    type:  number
    sql:
    {% if dynamic_param._parameter_value == 'view5s' %}
      COUNT(DISTINCT IF(${watch_time_seconds} >=5, ${video_load_id}, null))
    {% elsif dynamic_param._parameter_value == 'viewer5s' %}
      COUNT(DISTINCT IF(${watch_time_seconds} >=5, ${user_id}, null))
    {% elsif dynamic_param._parameter_value == 'watchtime' %}
      SUM(${TABLE}.watch_time_seconds) / 3600
    {% elsif dynamic_param._parameter_value == 'viewer' %}
      COUNT(DISTINCT ${user_id})
    {% endif %};;
  }

  measure: count {
    type: count
    drill_fields: [app_name]
  }
}
