view: bundle_dim {

  derived_table: {
    sql:
      select * from (
            SELECT  bundle_id , bundle_group_name , bundle_group_id , bundle ,
                    row_number() over (partition by bundle_id order by client_timestamp desc) as view_order
            FROM `pops-204909.PubSub.RT_Comics_Log_View_Bundle`)
      where view_order = 1

       ;;
  }

  dimension: bundle {
    type: string
    sql: ${TABLE}.bundle ;;
  }

  dimension: bundle_group_id {
    type: string
    sql: ${TABLE}.bundle_group_id ;;
  }

  dimension: bundle_group_name {
    type: string
    sql: ${TABLE}.bundle_group_name ;;
  }

  dimension: bundle_id {
    type: string
    sql: ${TABLE}.bundle_id ;;
  }

  measure: count {
    type: count
    drill_fields: [bundle_group_name]
  }
}
