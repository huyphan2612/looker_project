view: dim_users {
  derived_table: {

    sql:


      select
                b.session_date ,
                b.app_name ,
                b.platform ,
                b.device_type,
                b.app_region ,
                b.registration_status as  logged_in_status,
                count(b.user_id ) as users
            from  `pops-bi.production.DIM_First_Session_User_Info_v2` b
            where b.session_date >= '2021-01-01'
            group by
                  b.session_date ,
                  b.app_name ,
                  b.platform ,
                  b.registration_status,
                  b.device_type,
                  b.app_region;;


    sql_trigger_value: SELECT COUNT(*) FROM `pops-bi.production.DIM_First_Session_User_Info_v2` ;;
    partition_keys: ["session_date"]
    cluster_keys: ["app_name", "app_region", "platform", "device_type"]
  }



  dimension_group: session_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.session_date ;;
  }

  dimension: app_name {
    type: string
    sql: ${TABLE}.app_name ;;
  }

  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: logged_in_status {
    type: string
    sql: ${TABLE}.logged_in_status ;;
  }

  dimension: device_type {
    type: string
    sql: ${TABLE}.device_type ;;
  }

  dimension: app_region {
    type: string
    sql: ${TABLE}.app_region ;;
  }

  measure: users {
    type: sum
    sql: ${TABLE}.users ;;
  }



  measure: count {
    type: count
    drill_fields: [app_name, platform, logged_in_status]
  }
}
