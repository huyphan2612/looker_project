view: metatdata {
  sql_table_name: `pops-bi.Self_Service.POP_Scheduled_DIM_Videos_Enhanced_ALIAS`
    ;;

  dimension: category_id {
    type: string
    sql: ${TABLE}.category_id ;;
  }

  dimension: category_name {
    type: string
    sql: ${TABLE}.category_name ;;
  }

  dimension: content_type {
    type: string
    sql: ${TABLE}.content_type ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: format_id {
    type: string
    sql: ${TABLE}.format_id ;;
  }

  dimension: format_name {
    type: string
    sql: ${TABLE}.format_name ;;
  }

  dimension: genre_id {
    type: string
    sql: ${TABLE}.genre_id ;;
  }

  dimension: genre_title {
    type: string
    sql: ${TABLE}.genre_title ;;
  }

  dimension: object_id {
    type: string
    sql: ${TABLE}.object_id ;;
  }

  dimension: object_title {
    type: string
    sql: ${TABLE}.object_title ;;
  }

  dimension: object_type {
    type: string
    sql: ${TABLE}.object_type ;;
  }

  dimension: playlist_season_id {
    type: string
    sql: ${TABLE}.playlist_season_id ;;
  }

  dimension: playlist_season_title {
    type: string
    sql: ${TABLE}.playlist_season_title ;;
  }

  dimension: project_age_group {
    type: string
    sql: ${TABLE}.project_age_group ;;
  }

  dimension: project_id {
    type: string
    sql: ${TABLE}.project_id ;;
  }

  dimension: project_localization_type {
    type: string
    sql: ${TABLE}.project_localization_type ;;
  }

  dimension: project_name {
    type: string
    sql: ${TABLE}.project_name ;;
  }

  dimension: project_origin_country {
    type: string
    sql: ${TABLE}.project_origin_country ;;
  }

  dimension: project_production_type {
    type: string
    sql: ${TABLE}.project_production_type ;;
  }

  dimension: project_production_year {
    type: string
    sql: ${TABLE}.project_production_year ;;
  }

  dimension: topic_id {
    type: string
    sql: ${TABLE}.topic_id ;;
  }

  dimension: topic_title {
    type: string
    sql: ${TABLE}.topic_title ;;
  }

  measure: count {
    type: count
    drill_fields: [category_name, project_name, format_name]
  }
}
