view: rt_payment_transaction_report {

  derived_table: {
    sql: select
            a.* except(payment_status) ,
            coalesce(b.last_status, a.payment_status) as payment_status
          from `pops-204909.PubSubv1.RT_Payment_Transaction_Report` a
          left join
          ( select * from
              (select id, payment_status as last_status, row_number() over (partition by id order by updated_date desc) as trans_order
              from `pops-204909.production.Payment_Report_Payment_Audit`
              where file_date >= '2021-08-24' )
            where trans_order = 1
          ) b on a.payment_id = b.id
          where regexp_contains(upper(a.product_sku ), 'CLASSIN')
          and date(a.created_at) >= '2021-08-24'
          order by a.payment_id   ;;

    sql_trigger_value: SELECT COUNT(*) FROM  `pops-204909.PubSubv1.RT_Payment_Transaction_Report` ;;

  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: localized_title {
    type: string
    sql: ${TABLE}.localized_title ;;
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: no_of_star {
    type: number
    sql: ${TABLE}.no_of_star ;;
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: partner_type {
    type: string
    sql: ${TABLE}.partner_type ;;
  }

  dimension: payment_id {
    type: string
    sql: ${TABLE}.payment_id ;;
  }

  dimension: payment_status {
    type: string
    sql: ${TABLE}.payment_status ;;
  }

  dimension: product_sku {
    type: string
    sql: ${TABLE}.product_sku ;;
  }

  measure: transaction_amount {
    type: sum
    sql: ${TABLE}.transaction_amount ;;
  }

  measure: transactions {
    type: count_distinct
    sql: ${TABLE}.order_id   ;;
  }

  measure: buyers {
    type: count_distinct
    sql: ${TABLE}.user_id  ;;
  }


  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  measure: max_timestamp {
    type: string
    sql: MAX(FORMAT_TIMESTAMP("%D-%H", ${TABLE}.created_at)) ;;

    }

  dimension: is_POPS {
    type: string
    sql: CASE
              WHEN REGEXP_CONTAINS(upper(${TABLE}.email),'POPS') THEN 'POPS User'
              ELSE 'Normal User'
          END;;
  }


  measure: count {
    type: count
    drill_fields: [product_name]
  }
}
