view: firebase_uninstall {
  sql_table_name: `pops-204909.NHNhat.TEST_POP_DATAMART_Firebase_Uninstall`
    ;;

  dimension_group: joined {
    type: time
    description: "%E4Y-%m-%d"
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.joined_date ;;
  }

  dimension_group: left {
    type: time
    description: "%E4Y-%m-%d"
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.left_date ;;
  }

  dimension: platform_when_joined {
    type: string
    sql: ${TABLE}.platform_when_joined ;;
  }

  dimension: platform_when_left {
    type: string
    sql: ${TABLE}.platform_when_left ;;
  }

  dimension: pseudo_id_when_joined {
    type: string
    sql: ${TABLE}.pseudo_id_when_joined ;;
  }

  measure: user_count {
    type: count_distinct
    sql: ${TABLE}.pseudo_id_when_joined ;;
  }

  dimension: pseudo_id_when_left {
    type: string
    sql: ${TABLE}.pseudo_id_when_left ;;
  }

  dimension: uninstall_cohort {
    type: string
    sql: CASE
            WHEN DATE_DIFF(${joined_date}, ${left_date}, day) = 0 THEN 'Day-1 uninstall'
          ELSE
            'Day-N uninstall'

          END;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
