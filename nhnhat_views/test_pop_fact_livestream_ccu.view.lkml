view: esport_livestream_ccu {


  # sql_table_name: `pops-204909.NHNhat.TEST_POP_FACT_Livestream_CCU` ;;

  derived_table: {

    sql:  select *  from `pops-204909.NHNhat.TEST_POP_FACT_Livestream_CCU`  ;;
    sql_trigger_value: select count(*) from `pops-204909.NHNhat.TEST_POP_FACT_Livestream_CCU` ;;
    partition_keys: ["start_time"]
    cluster_keys: ["app_region", "app_name", "object_title", "platform_detail"]
  }

  dimension: object_title {
    type: string
    sql: ${TABLE}.object_title ;;
    bypass_suggest_restrictions: yes
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      hour,
      minute,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start_time ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  dimension: app_name {
    type: string
    sql: ${TABLE}.app_name ;;
  }

  dimension: app_region {
    type: string
    sql: ${TABLE}.app_region ;;
  }

  dimension: platform_detail {
    type: string
    sql: ${TABLE}.platform_detail ;;
  }

  measure: ccu {
    type: count_distinct
    sql: ${TABLE}.user_id ;;
  }

  measure: ccv {
    type: count_distinct
    sql: ${TABLE}.video_load_id ;;
  }

  dimension: stream_load_id {
    type: string
    sql: ${TABLE}.video_load_id ;;
  }



  measure: count {
    type: count
    drill_fields: []
  }
}
